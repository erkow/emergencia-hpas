<?php

return [
    'adminEmail' => 'admin@example.com',
    'days' => [
        'Monday'=>'Lunes',
        'Tuesday'=>'Martes',
        'Wednesday'=>'Miércoles',
        'Thursday'=>'Jueves',
        'Friday'=>'Viernes',
        'Saturday'=>'Sábado',
        'Sunday'=>'Domingo',
        ],
    'months' => [
        'January'=>'Enero',
        'February'=>'Febrero',
        'March'=>'Marzo',
        'April'=>'Abril',
        'May'=>'Mayo',
        'June'=>'Junio',
        'July'=>'Julio',
        'August'=>'Agosto',
        'September'=>'Septiembre',
        'October'=>'Octubre',
        'November'=>'Noviembre',
        'December'=>'Diciembre',
        ],    
   
];
