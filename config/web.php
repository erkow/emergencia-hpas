<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'name'=>'<b>Emergencia</b> HPAS',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language'=>'es-Mx',
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'userClassName' => 'app\models\Users', // fully qualified class name of your User model
                    // Usually you don't need to specify it explicitly, since the module will detect it automatically
                    'idField' => 'ID_USR', // id field of your User model that corresponds to Yii::$app->user->id
                    'usernameField' => 'USERNAME', // username field of your User model
                    'searchClass' => 'app\models\UsersSearch'    // fully qualified class name of your User model for searching
                ],
            ],
            'layout' => 'top-menu', // defaults to null, using the application's layout without the menu
//            'mainLayout' => '@app/views/layouts/main.php',
        // other avaliable values are 'right-menu' and 'top-menu'   
//            'menus' => [
//                'assignment' => [
//                    'label' => 'Grand Access' 
//                ],
//                'route' => null, // deshabilitar ítem
//            ],
        ],
        //ESTO SIRVE PARA HABILITAR EL MODULE KARKIT/GRID
           'gridview' =>  [
        'class' => '\kartik\grid\Module'
        // enter optional module parameters below - only if you need to  
        // use your own export download action or custom translation 
        // message source
        // 'downloadAction' => 'gridview/export/download',
        // 'i18n' => []
    ],
    'datecontrol' =>  [
        'class' => '\kartik\datecontrol\Module'
    ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '_MDCQVjDTK6X64N4YHJ2FnZTF0G2gz_L',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        //ESTO SIRVE PARA EN RUTAS NO PONER index.php?r=
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Disable r= routes
            'enablePrettyUrl' => true,
            // Disable index.php
            'showScriptName' => false,
//            'enableStrictParsing' => false,       
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ],
        //ESTO PERMIETE MANEJAR ACCESS-ROLE RBAC CON BASE DE DATOS
        'authManager' => [
            'class' => 'yii\rbac\DbManager' ,
            'defaultRoles' => ['guest'],
        ],
        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-green',
                ],
            ],
        ],
     'as beforeRequest' => [
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            [
                'actions' => ['login', 'error'],
                'allow' => false,
            ],
            [

                'allow' => true,
                'roles' => ['@'],
            ],
        ],
    ],


        
    ],
     //ESTO PERMITE ARMAR EL MENU DINAMICAMENTE CON EL WIDGET Yii2-admin
//        'as access' => [
//            'class' => 'mdm\admin\components\AccessControl',
//            'allowActions' => [
//                'site/*',
//                'admin/*',
//                'employee/*',
//                'check-in/*',
//                'some-controller/some-action',
//            // The actions listed here will be allowed to everyone including guests.
//            // So, 'admin/*' should not appear here in the production, of course.
//            // But in the earlier stages of your development, you may probably want to
//            // add a lot of actions here until you finally completed setting up rbac,
//            // otherwise you may not even take a first step.
//            ]
//        ],
    
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
