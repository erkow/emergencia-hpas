<?php

namespace app\controllers;

use Yii;
use app\models\CheckIn;
use app\models\CheckInSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ErPatient;
use app\models\Triage;
use yii\web\ForbiddenHttpException;
use app\models\GlobalFunctions;

/**
 * CheckInController implements the CRUD actions for CheckIn model.
 */
class CheckInController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CheckIn models.
     * @return mixed
     */
    public function actionIndex() {
        if (Yii::$app->user->can('/check-in/index')) {
            $searchModel = new CheckInSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Displays a single CheckIn model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        if (Yii::$app->user->can('/check-in/view')) {
            $model = $this->findModel($id);
            $erPatient = ErPatient::findOne($model->ID_ERP);
            return $this->render('view', [
                        'model' => $model,
                        'erPatient' => $erPatient,
            ]);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Creates a new CheckIn model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (Yii::$app->user->can('/check-in/create')) {
            $model = new CheckIn();
            $erPatient = new ErPatient();
            $activeUserId = \Yii::$app->user->identity->id;
            $model->ID_USR = $activeUserId;
            $model->DATE = date('Y/m/d');
            $model->TIME = date('H:i:s');

            //DEFAULT ER_PATIENT
            $erPatient->STATUS = 'ACTIVE';
            $erPatient->MODIFIED = date('Y/m/d H:i:s');
            $erPatient->CREATED = date('Y/m/d H:i:s');
            //END DEFAULT ER_PATIENT
            //VALIDACION DE ERRORES AJAX
//        if(Yii::$app->request->isAjax && $model->load($_POST) && $erPatient->load($_POST)){
//            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
////'json';
//            return \yii\widgets\ActiveForm::validate($model,$erPatient);
//        }

            if ($model->load(Yii::$app->request->post()) && $erPatient->load(Yii::$app->request->post())) {
                $idErP = $_POST['auxId']; //CHECKS IF EXISTS ID OF PATIENT
                $birthDayToChange = $erPatient->BIRTHDAY;
//                list($day, $month,$Year ) = explode("-", $birthDayToChange);
//                $newDateFormat = $Year.'-'.$month.'-'.$day;
//                $erPatient->BIRTHDAY = $newDateFormat;                

                if (!$idErP) {//SAVES PATIENT
                    $erPatient->save();
                    $idErP = $erPatient->ID_ERP;
                }

                $model->AGE = GlobalFunctions::CalculaEdad($birthDayToChange);
                $model->ID_ERP = $idErP;
                //CHECK IF ESCORT HAS DATA
                if (trim($model->ESCORT) == '') {
                    $model->ESCORT = NULL;
                }

                if ($model->save()) {
                    //SAVE IN TRIAGE AS WAITING (DEFAULT)
                    $triage = new Triage();
                    $triage->ID_CI = $model->ID_CI;
                    $triage->DATE_IN = date('Y/m/d');
                    $triage->TIME_IN = date('H:i:s');
                    $triage->SECONDS_ELAPSED = GlobalFunctions::getSecondsElapsedByDateTime($triage->DATE_IN, $triage->TIME_IN);
                    $triage->STATUS = 'WAITING';
                    if ($triage->save()) {
                        echo 1; //Success
                    } else {
                        echo 0; //Error
                    }
                } else {
                    print_r($erPatient);
                    print_r($model);
                    echo 0; //Error
                }
            } else {
                return $this->renderAjax('create', [
                            'model' => $model,
                            'erPatient' => $erPatient,
                ]);
                //CAmbié esto return $this->renderAjax('create', [   para que no muestre menú
            }
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Updates an existing CheckIn model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (Yii::$app->user->can('/check-in/update')) {
            $model = $this->findModel($id);
            $erPatient = ErPatient::findOne($model->ID_ERP);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->ID_CI]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                            'erPatient' => $erPatient,
                ]);
            }
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Deletes an existing CheckIn model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (Yii::$app->user->can('/check-in/delete')) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Finds the CheckIn model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CheckIn the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = CheckIn::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
