<?php

namespace app\controllers;

use Yii;
use app\models\Employee;
use app\models\EmployeeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\AuthItem;
use app\models\AuthAssignment;
use app\controllers\AuthAssignmentController;
use app\models\Users;
use yii\helpers\ArrayHelper;

/**
 * EmployeeController implements the CRUD actions for Employee model.
 */
class EmployeeController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Employee models.
     * @return mixed
     */
    public function actionIndex() {
        if (Yii::$app->user->can('/employee/index')) {
            $searchModel = new EmployeeSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Displays a single Employee model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        if (Yii::$app->user->can('/employee/view')) {
            return $this->render('view', [
                        'model' => $this->findModel($id),
            ]);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (Yii::$app->user->can('/employee/create')) {
            $model = new Employee();
            $model->DATE_MODIFIED = date('Y/m/d H:i:s');
            $authItems = AuthItem::find()->all();
            $model->STATUS = 'ACTIVE';


            if ($model->load(Yii::$app->request->post()) & $model->save()) {

                //GENERATE USER
                $user = new Users();
                $user->USERNAME = $model->ID;
                $user->PASSWORD = $model->ID;
                $user->ID_EMP = $model->ID_EMP;
//            $user->DATE_MODIFIED = date('Y/m/d H:i:s'); 
                $user->STATUS = 'ACTIVE';
                $user->save();


                //ADD PERMISSIONS
//           print_r($_POST);
//            die('jelo');
                $permissionList = $_POST['Employee']['perfiles'];
                if (is_array($permissionList)) {
                    foreach ($permissionList as $value) {
                        $newPermission = new AuthAssignment;
                        $newPermission->user_id = $user->ID_USR;
                        $newPermission->item_name = $value;
                        $newPermission->save();
                    }
                }


                return $this->redirect(['view', 'id' => $model->ID_EMP]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                            'authItems' => $authItems
                ]);
            }
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Updates an existing Employee model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (Yii::$app->user->can('/employee/update')) {
            $model = $this->findModel($id);
            $user = Users::find()->where(['ID_EMP' => $id])->one();
            $authItems = AuthItem::find()->all();
            $model->DATE_MODIFIED = date('Y/m/d H:i:s');


            //RECUPERAR LOS PERMISOS GUARDADOS (LOS CHECKED BOXES EXISTENTES)
            $model->perfiles = ArrayHelper::getColumn($model->getAuthAssignment()->asArray()->all(), 'item_name');
            $perfiles_guardados = $model->perfiles;

//        print_r($perfiles_guardados);



            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                if (isset($_POST['Employee']['perfiles'])) {
                    $permissionList = $_POST['Employee']['perfiles'];
                } else {
                    $permissionList = array();
                }


                //ADD PERMISSIONS (Compara cuales son nuevos)
                $ids_to_update = array_diff($permissionList, $perfiles_guardados);

                if (is_array($ids_to_update)) {
                    foreach ($ids_to_update as $value) {
                        $newPermission = new AuthAssignment;
                        $newPermission->user_id = $user->ID_USR;
                        $newPermission->item_name = $value;
                        $newPermission->save();
                    }
                }

                //REMOVE PERMISSIONS THAT not exist in the selected ids (Compara cuales hay que quitar)
                $ids_to_remove = array_diff($perfiles_guardados, $permissionList);

                if (is_array($ids_to_remove)) {
                    foreach ($ids_to_remove as $value) {
                        $newPermission = AuthAssignment::findOne($value, $user->ID_USR);
                        $newPermission->delete();
                    }
                }
//print_r($newPermission->getErrors());
//            die();
                return $this->redirect(['view', 'id' => $model->ID_EMP]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                            'authItems' => $authItems,
                ]);
            }
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Deletes an existing Employee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (Yii::$app->user->can('/employee/delete')) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Finds the Employee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
