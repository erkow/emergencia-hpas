<?php

namespace app\controllers;

use Yii;
use app\models\ErLog;
use app\models\ErLogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ErPatient;
use app\models\GlobalFunctions;
use app\models\TriageSearch;
use app\models\Triage;
use app\models\CheckIn;
use app\models\Cie10;
use app\models\Users;
use app\models\Employee;
use kartik\mpdf\Pdf;

/**
 * ErLogController implements the CRUD actions for ErLog model.
 */
class ErLogController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ErLog models.
     * @return mixed
     */
    public function actionIndex() {
        if (Yii::$app->user->can('/er-log/index')) {
            $searchModel = new ErLogSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            $dataProvider->pagination->pageParam = 'er-log-page';
            $dataProvider->sort->sortParam = 'er-log-sort';

            $triageSearchModel = new TriageSearch();
            $triageDataProvider = $triageSearchModel->searchForErLog(Yii::$app->request->queryParams);
            $triageDataProvider->pagination->pageParam = 'triage-page';
            $triageDataProvider->sort->sortParam = 'triage-sort';

            return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'triageSearchModel' => $triageSearchModel,
                        'triageDataProvider' => $triageDataProvider,
            ]);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Displays a single ErLog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        if (Yii::$app->user->can('/er-log/view')) {
            return $this->render('view', [
                        'model' => $this->findModel($id),
            ]);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Displays a single Triage model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewTriage($id) {
        if (Yii::$app->user->can('/er-log/view-triage')) {
            $model = Triage::findOne($id);
            $checkIn = CheckIn::findOne($model->ID_CI);
            $erPatient = ErPatient::findOne($checkIn->ID_ERP);

            return $this->render('view-triage', [
                        'model' => $model,
                        'checkIn' => $checkIn,
                        'erPatient' => $erPatient,
            ]);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Creates a new ErLog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (Yii::$app->user->can('/er-log/create')) {
            $model = new ErLog();
            $erPatient = new ErPatient();
            //ERLOG DEFAULT VALUES
            $model->ID_USR_ADMISION = \Yii::$app->user->identity->id;
            $model->SECONDS_ELAPSED_ATTENTION = 0;
            $model->STATUS_ATTENTION = 'ACTIVO';
            $model->DATE_CREATED = date('Y/m/d');
            $model->TIME_CREATED = date('H:i:s');
            $model->MODIFIED = date('Y/m/d H:i:s');

            //DEFAULT ER_PATIENT
            $erPatient->STATUS = 'ACTIVE';
            $erPatient->MODIFIED = date('Y/m/d H:i:s');
            $erPatient->CREATED = date('Y/m/d H:i:s');


            if ($model->load(Yii::$app->request->post()) && $erPatient->load(Yii::$app->request->post())) {

                $idErP = $_POST['auxId']; //CHECKS IF EXISTS ID OF PATIENT
                $birthDayToChange = $erPatient->BIRTHDAY;
//                list($day, $month,$Year ) = explode("-", $birthDayToChange);
//                $newDateFormat = $Year.'-'.$month.'-'.$day;
//                $erPatient->BIRTHDAY = $newDateFormat;                

                if (!$idErP) {//SAVES PATIENT
                    $erPatient->save();
                    $idErP = $erPatient->ID_ERP;
                }
//print_r($erPatient);die('err');
                $model->AGE = GlobalFunctions::CalculaEdad($birthDayToChange);
                $model->ID_ERP = $idErP;

                //SETTING UNCHECKED BOXES TO NO
                $this->setCheckBoxesValues($model);

                //VALIDATE PHONES //REMOVE __
                $auxPhone = str_replace('_', '', $model->PHONE);
                $auxEscortPhone = $model->ESCORT_PHONE;
                $model->PHONE = str_replace('_', '', $auxPhone);
                $model->ESCORT_PHONE = str_replace('_', '', $auxEscortPhone);

                //CHANGE MERIDIANT TIME FORMAT TO TIME
                $auxTime = $model->TROUBLE_TIME;
                $auxTime = date('H:i:s', strtotime($auxTime));
                if ($model->TROUBLE_TIME) {
                    $model->TROUBLE_TIME = $auxTime;
                }

                if ($model->save()) {
                    $idErL = $model->ID_ERL;
                    $modelAux = $this->findModel($idErL);
                    //            print_r($modelAux);   
                    $modelAux->CODE_008 = $idErL;
                    $modelAux->save();
                    echo 1; //Success                   
                } else {
                    print_r($erPatient);
                    print_r($model);
                    echo 0; //Error
                }

//            return $this->redirect(['view', 'id' => $model->ID_ERL]);
            } else {
                return $this->renderAjax('create', [
                            'model' => $model,
                            'erPatient' => $erPatient,
                ]);
            }
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Creates a new ErLog model based on an existing Triage.id is the ID_TRG from TRIAGE
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateErLog($id) {
        if (Yii::$app->user->can('/er-log/create-er-log')) {
            //LOAD TRIAGE DATA
            $triage = Triage::findOne($id);
            //LOAD CHECK IN DATA
            $checkIn = CheckIn::findOne($triage->ID_CI);
            //LOAD PATIENT DATA
            $erPatient = ErPatient::findOne($checkIn->ID_ERP);
            //LOAD CIE10
            $cie10 = Cie10::findOne($triage->ID_CIE10);

            //SETTING VALUES LOADED PREVIOUSLY
            $model = new ErLog();
            $model->ID_TRG = $triage->ID_TRG;
            $model->ID_USR_ADMISION = \Yii::$app->user->identity->id;
            $model->SECONDS_ELAPSED_ATTENTION = GlobalFunctions::getSecondsElapsedByDateTime($triage->DATE_IN, $triage->TIME_IN);
            
            $model->STATUS_ATTENTION = 'ACTIVO';
            $model->ID_ERP = $checkIn->ID_ERP;
            $model->AGE = $checkIn->AGE;
            $model->TROUBLE = $cie10->DESCRIPCION;
            $model->DATE_CREATED = date('Y/m/d');
            $model->TIME_CREATED = date('H:i:s');
            $model->MODIFIED = date('Y/m/d H:i:s');
            $model->ESCORT = $checkIn->ESCORT;
            

            if ($model->load(Yii::$app->request->post())) {
                //SETTING UNCHECKED BOXES TO NO
                $this->setCheckBoxesValues($model);

                //VALIDATE PHONES //REMOVE __
                $auxPhone = str_replace('_', '', $model->PHONE);
                $auxEscortPhone = $model->ESCORT_PHONE;
                $model->PHONE = str_replace('_', '', $auxPhone);
                $model->ESCORT_PHONE = str_replace('_', '', $auxEscortPhone);

                //CHANGE MERIDIANT TIME FORMAT TO TIME
                $auxTime = $model->TROUBLE_TIME;
                $auxTime = date('H:i:s', strtotime($auxTime));
                if ($model->TROUBLE_TIME) {
                    $model->TROUBLE_TIME = $auxTime;
                }

                $model->save();

                $idErL = $model->ID_ERL;

                $modelAux = $this->findModel($idErL);
//            print_r($modelAux);   
                $modelAux->CODE_008 = $idErL;
                $modelAux->save();

//            if($model->getErrors() || $modelAux->getErrors()){
//                print_r($model->getErrors());
//                print_r($modelAux->getErrors());
//                die('error');
//            }

                echo 1; //Success       
                return $this->redirect(['view', 'id' => $model->ID_ERL]);
            } else {
                return $this->render('create-er-log', [
                            'model' => $model,
                            'erPatient' => $erPatient,
                            'checkIn' => $checkIn,
                ]);
            }
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Updates an existing ErLog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (Yii::$app->user->can('/er-log/update')) {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->ID_ERL]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Deletes an existing ErLog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (Yii::$app->user->can('/er-log/delete')) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Finds the ErLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = ErLog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //METODO PARA CREAR PDF
    public function actionGeneratePdf($id) {
        if (Yii::$app->user->can('/er-log/generate-pdf')) {
            $model = $this->findModel($id);
            $erPatient = ErPatient::findOne($model->ID_ERP);
            //GETTING USERS ADMISION EMPLOYEE/NAME 
            $user = Users::findOne($model->ID_USR_ADMISION);
            if ($user->ID_EMP) {
                $employee = Employee::findOne($user->ID_EMP);
                $admisionName = $employee->LAST_NAME . ' ' . $employee->FIRST_NAME;
            } else {
                $admisionName = 'Administrador';
            }
            
            if($erPatient->GENDER == 'MALE'){
                $erPatient->GENDER ='Masculino';
            }else{
                $erPatient->GENDER ='Femenino';
            }
            $nombreArchivo = $model->CODE_008.'_'.$erPatient->LAST_NAME.'_'.$erPatient->FIRST_NAME.'.pdf';

            $dayNumericPdf = date("j"); //ejm 31
            $timePdf = date('H:i:s');
            $monthPdf = \Yii::$app->params['months'][date('F')]; //January
            $yearPdf = date('Y');
            $dayPdf = \Yii::$app->params['days'][date('l')]; //ejm lunes


            $content = <<<EOT
        <body>    
<table class="tableizer-table"  autosize="1">
<tr>
     <td style="width: 11.8cm;height:1cm;font-size:16px;" valign="bottom" >   <b> M.S.P.          H.P.A.S                 </b></td>
     <td style="width: 1.1cm;height:1cm;text-align:right;font-size:16px;"  valign="bottom"><b> 06 </b></td>     
     <td style="width: 1.1cm;height:1cm;text-align:right;font-size:16px;"  valign="bottom"><b> 01 </b></td>     
     <td style="width: 1.1cm;height:1cm;text-align:right;font-size:16px;"  valign="bottom"><b> 17 </b></td>     
     <td style="width: 3.5cm;height:1cm;text-align:right;font-size:16px;"  valign="bottom"><b> $model->CODE_008 </b></td>     
</tr>        
</table>
<table class="tableizer-table"  autosize="1">
<tr>
     <td style="width: 7.1cm;height:1.8cm;font-size:20px;"  valign="bottom"  ><b>$erPatient->LAST_NAME $erPatient->FIRST_NAME</b></td>
     <td style="width: 11.7cm;height:1.8cm;text-align:right;" valign="bottom" >$erPatient->ID</td>     
</tr>
</table>
<table class="tableizer-table"  autosize="1">                
 <tr>
     <td style="width: 15.3cm;height:1cm;font-size:12px;"   valign="bottom" >$model->ADDRESS</td>
     <td style="width: 3.5cm;height:1cm;font-size:12px;text-align:right;" valign="bottom">$model->PHONE</td>
 </tr>   
</table>                
<table class="tableizer-table"  autosize="1">
                 <tr>
     <td style="width: 2.3cm;height:1.2cm;font-size:12px;text-align:center;"   valign="bottom" >$erPatient->BIRTHDAY</td>
     <td style="width: 3.5cm;height:1.2cm;font-size:12px;text-align:center;" ></td>
     <td style="width: 5cm;height:1.2cm;font-size:12px;text-align:left;"   valign="bottom" > $erPatient->ID_NT</td>
     <td style="width: 1.4cm;height:1.2cm;font-size:12px;text-align:center;"   valign="bottom" >$model->AGE</td>
     <td style="width: 1.2cm;height:1.2cm;font-size:12px;text-align:center;"   valign="bottom" >$erPatient->GENDER</td>
     <td style="width: 2.9cm;height:1.2cm;font-size:12px;text-align:center;"   valign="bottom" >$model->MARITAL_STATUS</td>
     <td style="width: 2.5cm;height:1.2cm;font-size:12px;"   valign="bottom" ></td>
 </tr>                 
</table>    
<table class="tableizer-table"  autosize="1">                
 <tr>
     <td style="width: 3cm;height:1cm;text-align:center;"   valign="bottom" >$model->DATE_CREATED</td>
     <td style="width: 15.8cm;height:1cm;text-align:right;" ></td>
 </tr>   
</table>                 
<table class="tableizer-table"  autosize="1">                
 <tr>
     <td style="width: 6.2cm;height:1.2cm;font-size:12px;"   valign="bottom" >$model->NOTIFIY_CONTACT</td>
     <td style="width: 2.6cm;height:1.2cm;text-align:right;font-size:12px;" >$model->RELATIONSHIP_CONTACT</td>
     <td style="width: 10cm;height:1.2cm;text-align:right;font-size:12px;" >$model->ADDRESS_CONTACT       $model->ESCORT_PHONE</td>
 </tr>   
</table>    
<table class="tableizer-table"  autosize="1">                
 <tr>
     <td style="width: 1.5cm;height:0.9cm;"   valign="bottom" ></td>
     <td style="width: 3cm;height:0.9cm;text-align:right;" >$model->TRANSPORTATION_METHOD</td>
     <td style="width: 4.2cm;height:0.9cm;text-align:right;" ></td>
    <td style="width: 6.3cm;height:0.9cm;text-align:right;" >$model->ESCORT</td>
    <td style="width: 3.8cm;height:0.9cm;text-align:right;" >$model->ESCORT_ID</td>
 </tr>   
</table>  
<table class="tableizer-table"  autosize="1">                
 <tr>     
     <td style="width: 18.8cm;height:0.4cm;text-align:right;font-size:9px" valign="bottom">ADMISIONISTA RESPONSABLE:    $admisionName</td>
 </tr>   
</table>     
<table class="tableizer-table"  autosize="1">                
 <tr>     
     <td style="width: 3cm;height:0.4cm;text-align:right;" valign="bottom">$model->TIME_CREATED</td>
     <td style="width: 15.8cm;height:0.4cm;text-align:right;" ></td>
 </tr>   
</table>                     
                
<table class="tableizer-table"  autosize="1">                
 <tr>     
     <td style="width: 6cm;height:0.5cm;text-align:right;" ></td>
     <td style="width: 8.5cm;height:0.5cm;text-align:right;" valign="bottom">$model->TROUBLE</td>
     <td style="width: 4.3cm;height:0.5cm;text-align:right;" ></td>
 </tr>   
</table>   
    
<table class="tableizer-table"  autosize="1">                
 <tr>
     <td style="width: 5.6cm;height:1.2cm;text-align:center;"   valign="bottom" >$model->TROUBLE_DATE  $model->TROUBLE_TIME</td>     
     <td style="width: 4cm;height:1.2cm;text-align:left;" valign="bottom">$model->TROUBLE_PLACE</td>
    <td style="width: 7cm;height:1.2cm;text-align:left;" valign="bottom">$model->TROUBLE_ADDRESS</td>
    <td style="width: 2.2cm;height:1.2cm;text-align:right;" ></td>
 </tr>   
</table>                 
                
<table class="tableizer-table"  autosize="1">                
 <tr>     
     <td style="width: 2cm;height:2.4cm;text-align:right;" ></td>
     <td style="width: 12cm;height:2.4cm;text-align:left;" valign="bottom">$model->EMERGENCY_TYPE</td>
     <td style="width: 4.8cm;height:2.4cm;text-align:right;" valign="bottom">$model->VITAL_STATUS</td>
 </tr>   
</table>  

<table class="tableizer-table"  autosize="1">                
 <tr>     
     <td style="width: 18.7cm;height:4cm;text-align:left;font-size:9px" valign="bottom">$model->OBSERVATION_ADMISION</td>
 </tr>   
</table>   
<table class="tableizer-table"  autosize="1">                
 <tr>     
     <td style="width: 6cm;height:7.8cm;text-align:right;" valign="bottom"></td>
     <td style="width: 4cm;height:7.8cm;text-align:left;" valign="bottom">$timePdf</td>
     <td style="width: 4cm;height:7.8cm;text-align:right;" valign="bottom">$model->IESS_AFFILIATE</td>
     <td style="width: 3.8cm;height:7.8cm;text-align:right;" valign="bottom"></td>
 </tr>   
</table>                  
    
          </body>
EOT;


            // $content = $this->renderPartial('view');
            //set up the kartik\mpdf\Pdf component
            $pdf = new Pdf([
                'content' => $content, //$this->renderPartial('update')
//        'model'=>$model,
//        'erPatient'=>$erPatient,
                'mode' => Pdf::MODE_CORE,
                'format' => 'A4',
                'orientation' => 'P',
                'marginTop' => 28,
                'marginLeft' => 10,
                'marginRight' => 1,
                'marginBottom' => 0,
                'destination' => 'D',
                'filename' => $nombreArchivo,
//        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                'cssInline' => '	table.tableizer-table {
	/*border: 1px solid #CCC; */
        font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
} 
.tableizer-table td {

	/*border: 1px solid #ccc;*/
}
',
                'options' => ['title' => 'Paki Properties Reports'],
                'methods' => [
//               'setHeader'=>['Generated on: '.date("r")],
//               'setFooter'=>['|page {PAGENO}|'],
                ]
            ]);
            return $pdf->render();
        } else {
            throw new ForbiddenHttpException;
        }
    }

    public function beforeAction() {
        $model = new \app\models\LoginForm();
        if (Yii::$app->user->getIsGuest())
            Yii::$app->response->redirect(array('site/login'));
        return true;
    }

    //FUNCTION TO DEFINE TO NO IN CASE CHECKBOX IS SET TO 0
    public function setCheckBoxesValues($model) {
        if ($model->IESS_AFFILIATE == '0') {
            $model->IESS_AFFILIATE = 'NO';
        }
        if ($model->ISSFA_AFFILIATE == '0') {
            $model->ISSFA_AFFILIATE = 'NO';
        }
        if ($model->ISSPOL_AFFILIATE == '0') {
            $model->ISSPOL_AFFILIATE = 'NO';
        }
        if ($model->SOAT == '0') {
            $model->SOAT = 'NO';
        }
    }

}
