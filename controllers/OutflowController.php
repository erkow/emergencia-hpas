<?php

namespace app\controllers;

use Yii;
use app\models\Outflow;
use app\models\OutflowSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ErLogSearch;
use app\models\ErLog;
use app\models\GlobalFunctions;

/**
 * OutflowController implements the CRUD actions for Outflow model.
 */
class OutflowController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Outflow models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('/outflow/index')) {
            $searchModel = new OutflowSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            $dataProvider->pagination->pageParam = 'outflow-page';
            $dataProvider->sort->sortParam = 'outflow-sort';

            $erlogSearchModel = new ErLogSearch();
            $erlogDataProvider = $erlogSearchModel->searchForOutflow(Yii::$app->request->queryParams);
            $erlogDataProvider->pagination->pageParam = 'er-log-page';
            $erlogDataProvider->sort->sortParam = 'er-log-sort';

            return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'erlogSearchModel' => $erlogSearchModel,
                        'erlogDataProvider' => $erlogDataProvider,
            ]);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Displays a single Outflow model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
 {
        if (Yii::$app->user->can('/outflow/view')) {
            $model = $this->findModel($id);
            $erLog = ErLog::findOne($model->ID_ERL);

            return $this->render('view', [
                        'model' => $model,
                        'erLog' => $erLog,
            ]);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Displays a single ErLog model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewErLog($id) {
        if (Yii::$app->user->can('/outflow/view-er-log')) {
            $model = ErLog::findOne($id);

            return $this->render('view-er-log', [
                        'model' => $model,
            ]);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Creates a new Outflow model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('/outflow/create')) {
            $model = new Outflow();
            $model->DATE_OUTFLOW = date('Y/m/d');
            $model->TIME_OUTFLOW = date('H:i:s');

//        $model->SECONDS_ELAPSED_ATTENTION = GlobalFunctions::getSecondsElapsedByDateTime($triage->DATE_IN, $triage->TIME_IN);
            $model->SECONDS_ELAPSED_ATTENTION = 666;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->ID_OF]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        } else {
            throw new ForbiddenHttpException;
        }
    }
    
    
        /**
     * Creates a new Outflow model based on an existing ErLog. id is the ID_ERL from ERLOG
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateOutflowByErLog($id)
    {
        if (Yii::$app->user->can('/outflow/create-outflow-by-er-log')) {
            $model = new Outflow();
            //DEFAULT VALUES
            $model->ID_ERL = $id;
            $model->ID_USR_ADMISION = \Yii::$app->user->identity->id;
            $model->DATE_OUTFLOW = date('Y/m/d');
            $model->TIME_OUTFLOW = date('H:i:s');

            //ERLOG RELATED DATA
            $erLogAux = ErLog::findOne($id);

            $model->SECONDS_ELAPSED_ATTENTION = GlobalFunctions::getSecondsElapsedByDateTime($erLogAux->DATE_CREATED, $erLogAux->TIME_CREATED);

            if ($model->load(Yii::$app->request->post()) && $model->save() && $erLogAux->STATUS_ATTENTION != 'CERRADO') {
                //UPDATE TO CERRADO ER_LOG BASED ON ID_ERL            
                $erLogAux->STATUS_ATTENTION = 'CERRADO';
                $erLogAux->save();
//            print_r($erLogAux->getErrors());die();

                return $this->redirect(['view', 'id' => $model->ID_OF]);
            } else {
                if ($erLogAux->STATUS_ATTENTION == 'CERRADO') {
                    throw new \yii\web\ForbiddenHttpException('Error en su solicitud.');
                } else {
                    return $this->render('create', [
                                'model' => $model,
                                'erLog' => $erLogAux,
                    ]);
                }
            }
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Updates an existing Outflow model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
 {
        if (Yii::$app->user->can('/outflow/update')) {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->ID_OF]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Deletes an existing Outflow model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can('/outflow/delete')) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Finds the Outflow model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Outflow the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Outflow::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
