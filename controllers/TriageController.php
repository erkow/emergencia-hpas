<?php

namespace app\controllers;

use Yii;
use app\models\Triage;
use app\models\TriageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\CheckIn;
use app\models\ErPatient;
use kartik\mpdf\Pdf;
use app\models\GlobalFunctions;

/**
 * TriageController implements the CRUD actions for Triage model.
 */
class TriageController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Triage models.
     * @return mixed
     */
    public function actionIndex() {
        if (Yii::$app->user->can('/triage/index')) {
            $searchModel = new TriageSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Displays a single Triage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        if (Yii::$app->user->can('/triage/view')) {
            $model = $this->findModel($id);
            $checkIn = CheckIn::findOne($model->ID_CI);
            $erPatient = ErPatient::findOne($checkIn->ID_ERP);
            return $this->render('view', [
                        'model' => $model,
                        'checkIn' => $checkIn,
                        'erPatient' => $erPatient,
            ]);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Creates a new Triage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (Yii::$app->user->can('/triage/create')) {
            $model = new Triage();
            //DEFINING SCENARIO
            $model->scenario = 'create';
            $model->ID_USR = \Yii::$app->user->identity->id;
            $model->DATE_IN = date('Y/m/d');
            $model->TIME_IN = date('H:i:s');
            $model->SECONDS_ELAPSED = $this->getTimeElapsedInSeconds($model->DATE_IN, $model->TIME_IN);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->ID_TRG]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Updates an existing Triage model with status ATTENDED
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (Yii::$app->user->can('/triage/update')) {
            $model = $this->findModel($id);
            $model->DATE_OUT = date('Y/m/d');
            $model->TIME_OUT = date('H:i:s');
            $model->SECONDS_ELAPSED = GlobalFunctions::getSecondsElapsedByDateTime($model->DATE_IN, $model->TIME_IN);
            $model->STATUS = 'ATTENDED';
            $model->ID_USR = \Yii::$app->user->identity->id;

            //DEFINING SCENARIO, TIENE IR DESPUES DE LA ANTERIOR LINEA, SINO NO VALE
            $model->scenario = 'update';

            //LOADING RELATED INFO
            $checkIn = CheckIn::findOne($model->ID_CI);
            $erPatient = ErPatient::findOne($checkIn->ID_ERP);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {

//            $this->actionGeneratePdf($id);
                return $this->redirect(['view', 'id' => $model->ID_TRG]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                            'checkIn' => $checkIn,
                            'erPatient' => $erPatient,
                ]);
            }
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Deletes an existing Triage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (Yii::$app->user->can('/triage/delete')) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Finds the Triage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Triage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Triage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //METODO PARA CREAR PDF
    public function actionGeneratePdf($id) {
        if (Yii::$app->user->can('/triage/generate-pdf')) {
            $model = $this->findModel($id);
            $checkIn = CheckIn::findOne($model->ID_CI);
            $erPatient = ErPatient::findOne($checkIn->ID_ERP);
//        print_r($erPatient);die('end');
            
            $nombreArchivo = $erPatient->LAST_NAME.'_'.$erPatient->FIRST_NAME.'.pdf';

            $content = <<<EOT
        <body>    
<table class="demo">

	<thead>
	</thead>
	<tbody>
	<tr>
                <td width="15"><td>
                <td><img  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE8AAABGCAYAAACNO8lOAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAB2wSURBVHhe7Zx5XFTl/sdbrW51b/W7t1KWYdhVcs3boqZdzauiJpsKLhChppa577mU1a9MS23T0q6aWGpmZSioCAPDqiCgIjvDMAw7M8zKAPP5fZ4z6LWyfr3EP/jDb6/HM3PmzJln3vNdPt/zHLoNt+yG7Ra8TtgteJ2wG4PXboPd3oY2ezu3LdxRBGvVGtQrH4FRcT/MyXfCoLwNupQ70ZhyO8zpd6Al4XYY0x5AXVo31KU+DFXKGDQV/4RB/cfBfWAk3PtOx7LQ+ShZtgjnnbyhkfVCSQ831Lr7ora7DLU9ZFC7uqCSQ+XmgmJ3ZxR5OqPAXY693Z2AplrA1s7B6dnFplX6T1hrixnZZ8/izOl46fnNshuDZ+ekCE7AgwSvGFbtG4T391/A03fAM6XdDhvhGVLuR33qPahN+R+UpUxA0dlo9HvCHx6E59ZrErYv2IhLs6OQ5+pLeD1R5ixHlcwTNQRX6+T6X3gyB7xCD6dr4NX9Cl6bBM9ut0vwsjIzEX/qtGP+N8luMGw5O7Sjvb2Nj6x8XAaL9m3UJzvDoHgQxuS70ZxCeMrb0cRtc/JtaEm6C7rEe9GU/BDUCZ5Q5a3F2jcWwdMnED37RmLs8Choj5yCYkA/FHh4o8xVjgo3OVQuztBwVHFUyJygcXWCWuaMUrkTh4Dojq8FvEbCa+V8WgmL9GycX6uYI2fX2tKCjLQMxB6PFZO/aXZD8AQ66V/CAywcFQzb91GvcJPgNad0Y8jejmblHfTAu6FPpPcl0RsT/4LGxMdQoRiGktKj6DdkPHwHzEYv3zCkH0jCuajXcNbNFYXuMhQTTKmsB0O0Oyrk3aWtWtad8MS2B0ESposrAXrQ87oDugZOifDoaY6flls+Fmaz2JCekoETMV0AXrsA1s5wbRUha+Rg2Fa9Q3gy6JPuZ7gSGEPWmNQNhqR76In3wKS4iwD/gpoz3WEonoOD332BHr3GwXPgLIwcNhvVcRk42rsPLhNembsritx6oNSDw707ytweI7zHCawHw9aJQ3iiC7QubnxNjj09HiO8OsISuZih2kZ/owcyr/AxvdDaioz0s4iNPSnN/2bZDcIr5b9VfKDlVjxOlwpGXZKAx7BVEhjD1KK4m1t6XeptMDF09QxnjcID1satmBE+E+5+09Fz0BxEhK6Cbve3+IlASuRywpGh0oXDTYZSJ3qbm/NVaBoeU8XXtM4yaJgHVa5uOPQ4Pa+ac2nWc/CHbWbiMzEvMweK/Gex2ZCWQc+L6wKeZ1GvgrniTZjLNsJSsYaPF8KQ+wLDUxSM+6SCYVHcLg1z0m30vNtYLET+64YqZV9YG/ZizL9D4eMXhV79ZmHF3E0o3fAe4kUVlbujivlOeFWlsyuqZW4MVVdUEFwF856a+yoJTMV9ar6mkskQ/4+/o2bL/0K96R2oP9iEii1bUPTRR9CfzaD3tcJqtSItPQ0nYrsAvOb4x6FLcGH+ckGT4lEpj+kSH2FY3ktw3WBJJrTE2yRw0uBzEyVKvbIbKpUDCe8Axo6ZKsHz8XsZ763YgeIVG6AgvCJ6nopQ1ML7CEhDLxNVtpzwKt0JVC5jCLsx18kYsgxbwitkITnPED/v64JMbxek9XLHMV83VOz6FGgxotVsRGZqGgvGiY5vcHPshuAZzzxCL/srdMn3MUTvYjW9h8WBgx7XxCFCtJXDzDAV4WtIvoPH3o0G5kJ1yhOw6r7CaP9AePaJgG/fuVg590Oo1r4PBSVIEYtFidyVw5lwnFDOkFW59oCaYMtdeqCcoATIcuGNLBbl9FI1K26RJFsoX/jefHcPJDBvar/4jPDMsNlMSE9PwYkTMR3f4ObYjcFj1dQnCTlyB0HdyVx2F0OS4KjnGlLvhk7JYkFgzcmUJvS2RlbcJnplU+q9UCW7w9y4GTMiI5jzZsC772t4afIGNH76DY7Ty4o9CInAykSlJcByVl0hilXMf+X0vHI5vU0MNw+UuVDyuHgxpIW3ukBD8BXOhC3zQKrMFbWffwJYzQxbA8O2i8BrTroDxlRKD4ao/szf0JD4MDXeg6hLvh8ahnCtwoke+CiBPkyY/0CNsgeqEh5DfdLfUZXkwRy5DPujd8DZeyL8/rkQL45ehIafM3HY3VvyniqC0zjT2wignNDyCSXH2x1ZXu7IZqieo2elenkjw7s3svieS8yTZSIHCj3IcK9w9UCGTE54n0vwbBzp6aldA55OCGDmMb3iYZjTnoEpezTqM4ahNm0IDPkvw5g/i13EC9Cc8UFdxnAYCiPRXPgSalKGQnumL6ozQpCTGwOPfqPh1S8c/+wXirLYHBx7fhTOu9OD6EVaVlVRdUsIIcOvJ4pnh6PktVnIHTce6ePGoWj1UpS//y6KlszHef+RSGOuLKLMUTE/FjOcFR6eqNop4JnQajIjMy0VsV0CHvNYM3NYTYITWus2UlPFcoLHmJsPUqiKCneO+ioe2ktr0Wb5jvtSOdLZ0R0huDAUx41ErfoYRgZMgfdTkejlE4ZD73yLmre3I52VtFjOkBWCWBQDdx+ceuppIOccoK2APSsL7ZpqoLaJw8TRCHtJEbQrVyHDhx7IMC+SeyGBnqn5cocj5xHg2YxUnIzrAgXDJPQbJUllvA/MDV9yTwHh5KPdloA2cyrBlVEDVhJiJrVWAmyGRDYjhTzmAszarVCdGgN1zrtYvX4hZP0D4NNnBgKGvwLd0TQoe/VHMSGomPcqKEsu04tinxsK5OUCZsIyG2ArLID28PfQfLWXhSqRANXA5cvIChyHPA8KbIZxsocHtDu+4PEtaCHAdMKLjT3u+AI3yW4IntBvxsQHUXG6H0wNe7inGG3WVKhy1iA/6TUYaqmn7NWEmQlN9hu4FD8LzfWceHsp2hq/QXXSeKiVEYg79gG7jCHwfHIafLwCkLzte2SMY0h7EJ4QyUz8+cx5xwY/BVzI469mBuq1OBYehjN+vkjwdcbBp3uiOeYQ99fBuONTnHR9jL2xDGmswLWf7eJ72mCytSA5LQXHYn52fIGbZDcEzyDyHQtEefwAmOr2c08pWnQ/ozI5EOrTz0FfsoWgLsOqP4xy5USUKkeirmobj6OHmr5DVfIL0CaPQH3FHng/+TQ8BjJ8/UKxLupNZK9aB0WHhitnvsv1dMaxoYSXn88PZvdQpUX0M09Tz3kgz0eOGEqTqrfeZofB3pbt1/eUM3lecqRK8Oh5JhurLduzzCycPNUFLkk1sVvQK++HKr4vzLUH6GWlsOkOEcpgaOPl0FesZVueBWPjVyhPeR6ligFoqHqfOS8HdvNRaFL+jerE/tAVrsWu3Zvg2nMsfAa8hFFDo3Ax+iQOOcmo94QQdkG2jxN+HMacd/ESQ9DKEK3GT6EhiPPxxBkvF3zT2weN+/gDNjbD9J+9iGPVvegp4IlqS53HULeZbUhPy8TxE3Ed3+Dm2I15nlLA+wsqTvWGtVZ43mW06PehKmUAGlNkaCxfwn3nmQ93oiJ5CKEOQHMFCwty0WY8yi5jFOWNN8pPj4LdmoZnBgfDd9DL8H1iKo59mYhToyciy8MNJdR3Ob4yHH32GeBSEQUmPY85z5qbhcJ1a3F+/hw0RvPzq2sAtQo5kS8hm9AKCF7pIUf1TqHzmCMtZqSxw4iJ6QIFw5zIaqt4AOp4P7TU7eae8ywKX9Gj+lHLuUCvWs19eTDVf4VKxRBUJ/SFSc3QQjbajYehVQ5nW9cD2tN+sKq3Y/0bb8F7YBi8+0UifNxymGPOIL53L5RRz2UTxIlnn+NHMGxNVrRpa9FazgJRyzDVNnKrB1h91ds3I62fH0oZ7sVucgmedgc9jxqvxWZGKnvbmBNdoGAYFPdQ4z3E/MYmv3432nEBLc37UM2+teGMDIaydcx5+bDU/YeieDgqzvSBTrWB78wi+cOoSX1Wuihan+SEkjMv4lTcXvQc+CLcB7wMN3d/WOJzkcv2rYh564KXDD8OGUDn5nsN1bAmxuFgSACaP/4IiD4A85YPcTlsCk6592CL5gW1k5wiWYYUT/44O7azurHadqWrKs3iel3SA6gUYVu/k3tyYGv+CrWpA1Cb4AqDahX3ZTAffgY1Pa9CMRB6zZvcR61n3oPqtCfRmHQ/+11W7ISBKM37DMP+HQh3hq6873RsXbQV5UvfRC4rbS7btZjhg1gw0pnXNEDMEXzXxxcKFosET3eku3sil5W5xK07StmaqV3kLDTOhOcMregwzK2UKoSXns4OowuErV4pLq3fg9pED7Rq11IkH0Fr01uoTveGVvE4DBWRPGo/rA1rUKUcAI2iF4yqV6n7voHd8B5q0v3YpdzPqn03vU+OxuINWMhOQdYnDPJ+URg+ZBYqPzkApZcnPc8TsU/242+RAhSWAAe/RTwrbQEBFbHHLaGcUbEd0/B5mWt3qNihFMudHDrvM6YUcxtsLVZksD2L6wo6zyYt8NyFusT7GX7UWclPo17hTf32AEUr+9kkd9SlDWDL5ol65V/5/AHoUr1QkzSIva4fGhiyeuV90g/QlOyMmqwInD6+Fy5eo9B7YCR8vKfg4u7jOOj3BHvanshl/it84V8o8J+AwiGDcYkeJ66klHk6UdKIXtYdlc5OqGBbV0qIxZQ6Sg9vtmeMChvbM3YYmalKxHUFnWdOpEim1tNJV0segi7hb1IYNxGGkd1Hc+J93P+gdP1OL2SNODb5XkL8K5qUD/LxPWhOupe5sxtbvYcZxiOg1yai5xOj4ds/At6+4fh207f4ccJESg5PFLNoXHZ1xgU3dxQQVpnMlaCEFhRrHM7SVWcN94u+VsNiIa64pPF9WnFVxaJje2ZEBkVyXFcoGBZ+ebPIe+JaHsNPXKszJndj29aN3Ye4AHo7Q/IuKbzFEqS4ktxMeSNdUEgRV2NEeyeOJVyCrkzqzb44AYOfD4XXAHreEy/jg8XbkTp/EQUzxTIBiVUzcdVYeJdYfhRbAUtcKK0iuGLn7gxbsU8sHrlL1wZrd37qqLYtJseV5K6Q8wyJj1CqPMTxN3rPw9zeD2PSfbAm3IuWxP9egjeJ9YukO6SVMwFQDHFl2SpeI2QjX2tm5a5SeLBan8D4Sa9A3n8GevaNwpuz30X+qvVQyhielB9qwtPQ+8Syo7RyRjga6YqzWEFzZUtGr2RPnEfhnNVTjpPebqjatQN2ajyxAJSWSpF8vAuIZFV8H6hO90Hl6QGsuNzGs1CcfgymxAcI6I6roKQLCEmOYVbcBQvBtSTehlZxiV54ZModkhfWKlxZrX9GwNR5cOszDU/0n4O35ryH/LVvQeniSc+TMzwFPLF6JtYzXKWLn1pnwuMo8PTC0ce64whbs2inR7BH/ig+YfW9KERySwvTXhsyUrMYtl1g0dtuYBXT7wF0+ygf2D/qN8NWOhk6xT8YqvQmhqfkZRK8uzju5GOGNeEJgCK0m6WQZj5M7IaaRHd63kkMHzMD7n3D0bvvbLy/YAvSFyxGKitpOXOctG5Lz9O4OLxNxXynFtKEhePI3x5iX8u+VazLxp9Ee/xxtJw+Dru6CO1t9DxqvQy2Z7HHu8DSow11FMZU92K0U+2zw7Bo1qNBIWMREXmQXidBIjgWC2lI67diEdwBTk+v0yvuoAc+jPq0ITDWJsG3zwT49JsJL58ZOPBBNI6M9cc5Jn4Ve9wKWXd6HPOdCysr4allj0Pl8iiKPWXY2+Nxtmici6VV0nWgNHGsXejRAiNb4makM+fFdoXVM5JzLCq3i8VlI12xAmb1++xX5YRyrwTGoHTIGeFdDnjduL1TKhyNEjgWHUqdxtM90HBhHr4//BWcPQNYbWejp28QSvYew/e9fSlL6HkSPHGXAAcLgvC8SrEATo8sYi+7x8kZqGuS5tUuFrw7Fr3b0QYLWtlk2Aiviyx6i0naBThxewPDQiyAmys3o07hLt0x0Nxxp0Azh6jIQtLoWaEN3K9T3oP6lL9Q4z0IwxnKFLZumoKPEBTK3rbvNPTsGYYl01ajdOMWKAmuQO5F7eYhLfqUUgCXiSFdsnJHMeFd9pJjZ/d/AE16grNzXvxRbZwSHZBPOexMe61ITzuLE10hbMUvKu5BamdQtNtNhFgLo/pzqBMHoTrRFXUJzqg/I7aufC5jf+sKbRL3JbqgOkGGSlbXag5tfC+UKyYiNm4rvAcMgWffIMidnoftQhWOjQ+AwtsbGV5+yPT0Q5qXD5S+cqT5uCDN2xMpXr2Q3tsHit5e+FDeA2io5zzobbY2CZ64TUXcSSNuXHFckmJv2xUuSbVzdnYBT9wbIm72aTfB2piA2sI3UF/4OhoKOC4v5liC+oKlqCtczLEQjfkcl7gvfzka+VpD/hq06X9AdPQXWLV+G1Zu+AIHDsRBm56Nk2tWIWvlcuQsXYPcJatwfvkynFu9COdXvoqc5YuQvXwNspct5zFL8NPCV2A3MX20i/vLxL0qjnu3hANK8FrbUFJSiry8C2LPTbMbhCfdgyQ9EpNFm5h0NSeaw3GWI/NXQ+zL4jFs7lv5uFU8PsdxgWepktYYWqzt1LP0aW7tOp3jrqe6OkDLbU0tUKuld/Ez6ir4XMP9zHFVehYKviZubLSKH9GONs6l1c48xzNbxTzFXVPiXsI2RgvHzbQby3m3TLJb8Dpht+B1wjoFz2owwGAUue9aa4NB7GcqFGY0sBr/kRkN+KMj7GyvmFn/vP0/57uZduPwDOlYP7gH/ML3Q3PNt7MVfIyJni4Y83E+rA0nsCR4JeKZ269vDTixJBgrf++A9gac/eYA0ur+bKL/f853k61T8FY+2QPu/V/G/qv0bCj4JAi9XVzxr48Ir82IvLMXYRIvmw0wt1lhsVhgoayg9KK1wZh3FhelA9pYbY0w8TWjkdVXOLQpF9uDA7DpXJNU28WtvGaTiceYYDRbHTqOfmaw8L18X7Ot9Zrz8YxWHmd0DIvt1xHSeescvGHPI3K6P2bt1zhCq7UEn00fhxlhQzFawGuIxdwxC+h5OpxZPhFTl23AstdmY9rEAMzamQMzPSV27hgsoKfYVEexNmwMhg99Fs++EI4PlVpc+vpVDH707+gfvBGna1ugOrEJUeOGY9jw5+H/0kb8WGRDU/wSBE5fgkXTgrHiaB5iOs4HSz4OrQrB8888hUHPjsHsbQrUXPmNb5J1Et54bNu7AhNmR6OKE2st3YEZIRuwb93Iq/Aihs12wHvFDwNXx6LSZEFzzlZMGPsOskyEFzEMs+OrcenTQIxachyqpnrkH1yFVz9Kg56et3WCP96j57XXx2DxxCjsyqpCfZ0ayo8mYeTrJ1B6Ogo9/ebjUKGeHn7lfPXQHIjE0GkfI7PGCH3pMSwdMRpvnr252bDT8HbnxWPNhNmIrmpB2ZcRCH4vBenvXgfenMEIO0zRS7Nrv8WkEauRYbzyZRvQGL8ao4YFYv47O/BdfAaKavlr2IrwOeF9eMkKQ8YGPOUyEGODghEcHIwg/8EYErEfWbFReHbStxC3louc5zhfORSLnkPg3oqOYqNDwsKhCNpbLlrem2adh1fWhMQ1E/DK12n4MjIE/5vViJzrwhuK6THsP2l27SHCW3UNPIaZuQ7qklwkHN6BDS+PxHOzDqHS7IC3RcBLX4vhY97FOZ0ZZrMZBu1FnLusQ338LDw34wSxCbtyPhUUi4cg4CtVBzw9nw9D0J6yrgavFYbENRjrHwD/oHdwjl/s9+DN+F14FVBu8EfAe0p2XU2ojF+H0f7v4VxTEXZO9Mc7mQ1oqz2KOc/4Y2N8JZr1KsSueRGhH+WiSsAL/zW8elQdiMLQqZ/gXJ0BupKfsOyFsV0rbNf7h+I/hIfmBKx4tjdGbMxkESC8DyYiWJIqsZjnv9ABb9EYzLkK7zAiX9zggDfPHwvpedZLX+P1cUPw9NPPYMgLU/DG92Ww2etxcvHzeNL/DcTVWFD803pMHfEM/vn0YIydvQ1Jde0sGAswdu418DrOB+tlHGbBGDroSalgvPJxEkQmuJl24/BoQiRfmY9FCGPxB0HCbNcRyZQq4m+FHGankBbXPWhXRS2beUmmOKSK6POF2SlfDJQmktCg1DFLr1PSWMSVHWGUKv898S9EskOqiOO7mlS5ZbfgdcZuweuE3YLXCftjeEz81+bi37dW9qUmSX+ZubVIjenvmJ3F5E/2+bZfVAJhNhaa/36O6UoPfMXsVhjF8mOHtVznio6dhU18vNh2tvj+ATxKjg/DsThOrM/+sVkvfoPVEUEIDA3D5OCpeH1rwu/0kXZUHZqNyF0ljvWFPzJzDj4MX4xrP96c8wH+7TcE/uyNJ04YjZFjI7A5obEDQjsa4t/Ha2u/R6ko5Kbz+CB8CU5eO317FQ7NjsSuwlIcXLoY0eWdk8y/hddqhckkftkGZLwbhFek2dvRInmWBWb2pr/8yCacXjwWUdGFaBbvazqL7TPCsT1P/AV4G+UH91k4uBXiourgVEzaSXjt9MCrXkO50SFzxO1gJuFZDRl4N+iVX8F7F2OCd6GQHmem9Ck/HIXhEYfZV7dLHqnJy0aRvhUmcTLCeydoDmI04lhxFaYFdsI7OHUSdpbY0K7VdnwPcUXG4c0mcYy0yyLNwSLt+/2f+VfwLLi4fwWmBgRiSlgEpo0YgijOvr02AR+9GoopU8MwJWIl9uUKMFesGamrR+GFpfuQUlRDgCY0VWrQLP7CWvMj1kdMRtj0UAQGzsRnF00d8ApRvG8mZh1QS17TeGopwjdlU7tdxP4VUxEQOAVhEdMwYkjUH8DT4fLeaRg+8wdUle/DzFkHoG5hQDaewtLwTciuO4+3Rz+HsFdeQmjQOEycuQUJdZUOeEWlODBrJvap2mAr+wEbIidh0uQQTJq1CQn1Vmh+XI+IyWGYHhqIwJmf4aJYmr6O/RJeQxxeH78A35UaYNZfwK6QQQiP0+LillCEbklDdWMjSo8uRfCCn1F3Ta5pq87GT5+vxZwpYzFydDBefec75JnsMF76AXtiClCnq0Xm5mBMY29ZcQXenlCERV+BNw9Bb2dDHfc6xi/4DqUGM/QXdiFkUPhv4A12G4rxQcEImjgeE6Yuxt4sE1pVexAaFg2142SYF/S2BO+t4cOwMbGGkVSNxI0vYtrONER3wIsOC8UelQ4pawIx70AxDOYm5EZ/gN0ZVbj0wx7EFNRBV5uJzcHTsOd3wvsX8FpL9yI0ZCuKpWONSFk+AbPiSnFqwQD0m/QqFi5ciAXzQjB+3hGUX/Hm1npcjE9BqdFKN2f41Z7H7ojhCP+mAoaL0diwYilWrVmDpSFPYvzu0l/C+7oD3vE5CHw7Exf3hiJka7EjnIwpWD5h1h94nghvRwppk+B93QHvOOYEOuBtnBCBg46OEA0/zMSEt37G3mvhlVficGQA3r7Q0e1Y6tFgN+Ni9AasWLoKa9YsRciTjv79evZLz2s6g6UB83GwmJ6ny8MXUwbS86pwYfN0RH6ejVp6XmX6N/j8SIH0/7SQzFaKPTNDsPakmr8ev5SxAj8v8sfMQ5eRsD4E874tRRM9L21zAAI/zb8Kr+TrGQjZlgO9RYesjydgxJvZ0JxZioD5B1FMz9PlfYEpA3/reWOCOzzsGmur+BozQrYhR2+BLutjTBjxpsPzhg3HhjNael4NPS8Qkf/JxIFfeF4TlKsC8eo3Ds/L/nIF3v/+J6wLmYdvS5voeWnYzBT26eU/Aw8tKDi4EqHjxyNochTmTgmWqm1b9WlsnjsFIaGTETJ9KXPetRKAfer5aKye8SLGvUhAAUF4ac0B5JjaUBOzDqGh4Zg7fyXWLw/FqOnbcGKfo9o2X96JmayaYVGvYdnroYjYwpzXUoCDK0MxfnwQJkfNxZTg31bbkAjmtl/Bg+Uyds6ciICwKLy27HWERmyR4H04PQARM/ljBU7E5AU7kKHXOKrttTmv5CjWRTC3BQdh0uwPkFCrRcw6pqnwuZi/cj2Wh47C9O3nYOj4qGvtV/BobL4d1ZbN+VWdd6Xacj+r7W/rj0PnSVVSHGft+KXaxbkssFisPJeFDT7D46rOs9FLeaxY0xCvdVRb0cw7zmP9HZ13fQ1pE+sfDGOLxQZLx8la6MEt0rlYNblf2G913m+rbbuYg5iXlXOwGMTfzlzXfgvvlv1puwWvE3YLXifsFrxO2C14nbBb8Dpht+DdsAH/B1Rf8qQPT4VOAAAAAElFTkSuQmCC"/></td>
		<td align='center'><br><barcode code="$erPatient->ID" type="C39" size="0.5" height="2.0" /><br><br>$erPatient->ID</td>
                <td width="15"><td>
		<td align='center'>
                <b>Hospital Pablo Arturo Suárez</b><br>
                    <b>Apellidos/Nombres</b><br>
    <b>$erPatient->LAST_NAME $erPatient->FIRST_NAME</b><br>
        <b>Cédula: $erPatient->ID</b><br>
            <b>Ingreso: $model->DATE_IN $model->TIME_IN</b><br> 
   
                </td>
	</tr>
	</tbody>
</table>
          </body>
EOT;
//            <b>Prioridad: $model->ID_PRT</b><br>     

//    <div>  <barcode code="$erPatient->ID" type="c93" size="0.5" height="2.0" /></div>        
            // $content = $this->renderPartial('view');
            //set up the kartik\mpdf\Pdf component
            $pdf = new Pdf([
                'content' => $content, //$this->renderPartial('update')
//        'model'=>$model,
//        'erPatient'=>$erPatient,
                'mode' => Pdf::MODE_CORE,
                'format' => [19.8, 290],
                'orientation' => 'L',
                'marginTop' => 0,
                'marginLeft' => 1,
                'marginRight' => 1,
                'marginBottom' => 1,
                'destination' => 'D',
                'filename' => $nombreArchivo,
//        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                'cssInline' => 'body {font-size:12px}',
                'options' => ['title' => 'Paki Properties Reports'],
                'methods' => [
//               'setHeader'=>['Generated on: '.date("r")],
//               'setFooter'=>['|page {PAGENO}|'],
                ]
            ]);
            return $pdf->render();
        } else {
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Marks as absent patiend with no ID an existing Triage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionAbsent($id) {
        if (Yii::$app->user->can('/triage/absent')) {
            $model = $this->findModel($id);
            $model->scenario = 'create';
            $model->ID_PRT = NULL;
            $model->STATUS = 'ABSENT';
            $model->DATE_OUT = date('Y/m/d');
            $model->TIME_OUT = date('H:i:s');
            $model->ID_USR = \Yii::$app->user->identity->id;
            $model->save();

            return $this->redirect(['index']);
        } else {
            throw new ForbiddenHttpException;
        }
    }

}
