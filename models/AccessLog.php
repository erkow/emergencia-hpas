<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "access_log".
 *
 * @property integer $ID_AL
 * @property integer $ID_USR
 * @property string $DATE_LOG
 * @property string $TIME_LOG
 * @property string $DATE_LOG_OUT
 * @property string $TIME_LOG_OUT
 *
 * @property Users $iDUSR
 */
class AccessLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'access_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_USR', 'DATE_LOG', 'TIME_LOG'], 'required'],
            [['ID_USR'], 'integer'],
            [['DATE_LOG', 'TIME_LOG', 'DATE_LOG_OUT', 'TIME_LOG_OUT'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_AL' => 'Id  Al',
            'ID_USR' => 'Usuario',
            'DATE_LOG' => 'Fecha de Ingreso',
            'TIME_LOG' => 'Hora de Ingreso',
            'DATE_LOG_OUT' => 'Fecha de Salida',
            'TIME_LOG_OUT' => 'Hora de Salida',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDUSR()
    {
        return $this->hasOne(Users::className(), ['ID_USR' => 'ID_USR']);
    }
}
