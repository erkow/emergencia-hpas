<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AccessLog;

/**
 * AccessLogSearch represents the model behind the search form about `app\models\AccessLog`.
 */
class AccessLogSearch extends AccessLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_AL', 'ID_USR'], 'integer'],
            [['DATE_LOG', 'TIME_LOG', 'DATE_LOG_OUT', 'TIME_LOG_OUT'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AccessLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID_AL' => $this->ID_AL,
            'ID_USR' => $this->ID_USR,
            'DATE_LOG' => $this->DATE_LOG,
            'TIME_LOG' => $this->TIME_LOG,
            'DATE_LOG_OUT' => $this->DATE_LOG_OUT,
            'TIME_LOG_OUT' => $this->TIME_LOG_OUT,
        ]);

        return $dataProvider;
    }
}
