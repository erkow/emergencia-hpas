<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "check_in".
 *
 * @property integer $ID_CI
 * @property integer $ID_ERP
 * @property integer $ID_USR
 * @property integer $ID_TRB
 * @property string $DATE
 * @property string $TIME
 * @property string $ESCORT
 * @property integer $AGE
 * @property string $TROUBLE
 * @property string $MODIFIED
 *
 * @property Users $iDUSR
 * @property ErPatient $iDERP
 * @property Troubles $iDTRB
 * @property Triage[] $triages
 */
class CheckIn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'check_in';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'ID_USR', 'DATE', 'TIME', 'TROUBLE','AGE'], 'required'],
            [['ID_CI', 'ID_ERP', 'ID_USR', 'ID_TRB','AGE'], 'integer'],
            [['DATE', 'TIME', 'MODIFIED','ESCORT'], 'safe'],
            [['ESCORT'], 'string', 'max' => 50],
            [['TROUBLE'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_CI' => 'Id  Ci',
            'ID_ERP' => 'Paciente',
            'ID_USR' => 'Id  Usr',
            'ID_TRB' => 'Id  Trb',
            'AGE' => 'Edad al Registrar',
            'DATE' => 'Fecha Ingreso',
            'TIME' => 'Hora Ingreso',
            'ESCORT' => 'Acompañante (Nombre)',
            'TROUBLE' => 'Motivo/Problema',
            'MODIFIED' => 'Última Modificación',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDUSR()
    {
        return $this->hasOne(Users::className(), ['ID_USR' => 'ID_USR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDERP()
    {
        return $this->hasOne(ErPatient::className(), ['ID_ERP' => 'ID_ERP']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDTRB()
    {
        return $this->hasOne(Troubles::className(), ['ID_TRB' => 'ID_TRB']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTriages()
    {
        return $this->hasOne(Triage::className(), ['ID_CI' => 'ID_CI']);
    }
}
