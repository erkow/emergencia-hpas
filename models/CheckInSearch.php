<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CheckIn;

/**
 * CheckInSearch represents the model behind the search form about `app\models\CheckIn`.
 */
class CheckInSearch extends CheckIn
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_CI', 'ID_USR', 'ID_TRB'], 'integer'],
            [['DATE', 'TIME', 'ESCORT', 'TROUBLE','ID_ERP', 'MODIFIED'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
         
        $query = CheckIn::find()->with('triages')->where(['triage.STATUS'=>'WAITING']);
//        $query = CheckIn::find()->with('triages')->where(['triage.ID_CI'=>'6']);
//        CheckIn::find()->joinWith('triages',true,'RIGHT JOIN');
        //find()->with('triages')->where(['triage.STATUS'=>'W'])
//        print_r($query);die();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith('iDERP');
        $query->joinWith('triages');

        $query->andFilterWhere([
            'ID_CI' => $this->ID_CI,            
            'ID_USR' => $this->ID_USR,
            'ID_TRB' => $this->ID_TRB,
            'DATE' => $this->DATE,
            'TIME' => $this->TIME,
            'MODIFIED' => $this->MODIFIED,            
        ]);
//  ->leftJoin('order', '`order`.`customer_id` = `customer`.`id`')
        $query->andFilterWhere(['like', 'ESCORT', $this->ESCORT])
            ->andFilterWhere(['like', 'TROUBLE', $this->TROUBLE])                                   
            ->andFilterWhere([
                'or',
                ['like', 'er_patient.LAST_NAME', $this->ID_ERP],
                ['like', 'er_patient.FIRST_NAME', $this->ID_ERP],
            ])
                  
        ->addOrderBy('DATE DESC, TIME DESC');

        return $dataProvider;
    }
}
