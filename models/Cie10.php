<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cie10".
 *
 * @property string $ID_CIE10
 * @property string $DESCRIPCION
 * @property string $GRUPO
 */
class Cie10 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cie10';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_CIE10'], 'required'],
            [['ID_CIE10'], 'string', 'max' => 10],
            [['DESCRIPCION'], 'string', 'max' => 400],
            [['GRUPO'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_CIE10' => 'Id  Cie10',
            'DESCRIPCION' => 'Descripcion',
            'GRUPO' => 'Grupo',
        ];
    }
}
