<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "destination".
 *
 * @property integer $ID_DST
 * @property string $DESTINATION
 * @property string $STATUS
 *
 * @property QuickCare[] $quickCares
 */
class Destination extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'destination';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {//Aqui quite ID_DST (el ID) de REQUIRED, Y solo a ese le puse primaryKey lo cual permite mandar en null, aunq esto no es necesario porque ya al mandar null autoincrementa
        return [
            [['DESTINATION', 'STATUS'], 'required'],
            [['ID_DST'], 'integer'],
            [['STATUS'], 'string'],
            [['DESTINATION'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_DST' => 'Id',
            'DESTINATION' => 'Destino',
            'STATUS' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuickCares()
    {
        return $this->hasMany(QuickCare::className(), ['ID_DST' => 'ID_DST']);
    }
}
