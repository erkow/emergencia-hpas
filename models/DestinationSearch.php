<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Destination;

/**
 * DestinationSearch represents the model behind the search form about `app\models\Destination`.
 */
class DestinationSearch extends Destination
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_DST'], 'integer'],
            [['DESTINATION', 'STATUS'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Destination::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID_DST' => $this->ID_DST,
        ]);

        $query->andFilterWhere(['like', 'DESTINATION', $this->DESTINATION])
            ->andFilterWhere(['like', 'STATUS', $this->STATUS]);

        return $dataProvider;
    }
}
