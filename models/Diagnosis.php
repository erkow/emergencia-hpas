<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "diagnosis".
 *
 * @property integer $ID_DG
 * @property string $DIAGNOSIS
 * @property string $STATUS
 *
 * @property QuickCare[] $quickCares
 */
class Diagnosis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'diagnosis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_DG', 'DIAGNOSIS'], 'required'],
            [['ID_DG'], 'integer'],
            [['STATUS'], 'string'],
            [['DIAGNOSIS'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_DG' => 'Id  Dg',
            'DIAGNOSIS' => 'Diagnosis',
            'STATUS' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuickCares()
    {
        return $this->hasMany(QuickCare::className(), ['ID_DG' => 'ID_DG']);
    }
}
