<?php

namespace app\models;

use Yii;
use app\models\AuthAssignment;

/**
 * This is the model class for table "employee".
 *
 * @property integer $ID_EMP
 * @property integer $ID 
 * @property string $FIRST_NAME
 * @property string $LAST_NAME
 * @property string $BIRTHDAY 
 * @property string $DATE_MODIFIED
 * @property string $STATUS
 *
 * @property Users[] $users
 */
class Employee extends \yii\db\ActiveRecord
{
    public $perfiles;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FIRST_NAME', 'LAST_NAME','ID','STATUS'], 'required'],
            [['ID_EMP','ID'], 'integer'],            
            [['BIRTHDAY', 'DATE_MODIFIED', 'perfiles'], 'safe'],
            [['STATUS'], 'string'],
            [['FIRST_NAME', 'LAST_NAME'], 'string', 'max' => 45],
            [['ID'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_EMP' => 'Id  Emp',
            'ID'=>'Cédula',
            'FIRST_NAME' => 'Nombres',
            'LAST_NAME' => 'Apellidos',
            'BIRTHDAY' => 'Fecha de Nacimiento',            
            'DATE_MODIFIED' => 'Fecha de Modificación',
            'STATUS' => 'Estado',
            'EmpleadoNombreCompleto' => 'Empleado',
        ];
    }
    
    /*
    Returns Employee Full Name
    */
    public function getEmpleadoNombreCompleto() {
        return $this->LAST_NAME . ' ' . $this->FIRST_NAME;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['ID_EMP' => 'ID_EMP']);
    }
    
    
        //Relation Many-Many N:M mediante bridge table    
    public function getAuthAssignment() {
        return $this->hasOne(AuthAssignment::className(), ['user_id' => 'ID_USR'])
                        ->viaTable(Users::tableName(), ['ID_EMP' => 'ID_EMP']);
    }
      

}
