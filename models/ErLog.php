<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "er_log".
 *
 * @property integer $ID_ERL
 * @property integer $ID_ERP
 * @property string $CODE_008
 * @property integer $ID_TRG
 * @property integer $ID_USR_ADMISION
 * @property string $MARITAL_STATUS
 * @property integer $AGE
 * @property string $IESS_AFFILIATE
 * @property string $ISSFA_AFFILIATE
 * @property string $ISSPOL_AFFILIATE
 * @property string $SOAT
 * @property string $ADDRESS
 * @property string $PHONE
 * @property string $NOTIFIY_CONTACT
 * @property string $RELATIONSHIP_CONTACT
 * @property string $ADDRESS_CONTACT
 * @property string $ESCORT
 * @property string $ESCORT_ID
 * @property string $ESCORT_ADDRESS
 * @property string $ESCORT_PHONE
 * @property string $TRANSPORTATION_METHOD
 * @property string $TROUBLE
 * @property string $TROUBLE_DATE
 * @property string $TROUBLE_TIME
 * @property string $TROUBLE_PLACE
 * @property string $TROUBLE_ADDRESS
 * @property string $EMERGENCY_TYPE
 * @property string $VITAL_STATUS
 * @property string $REFERENCE
 * @property string $OBSERVATION_ADMISION
 * @property integer $SECONDS_ELAPSED_ATTENTION
 * @property string $DATE_CREATED
 * @property string $TIME_CREATED
 * @property string $MODIFIED
 * @property string $STATUS_ATTENTION
 *
 * @property ErPatient $iDERP
 * @property Triage $iDTRG
 * @property Users $iDUSRADMISION
 * @property Outflow[] $outflows
 * @property QuickCare[] $quickCares
 */
class ErLog extends \yii\db\ActiveRecord
{        
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'er_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_USR_ADMISION', 'ADDRESS', 'SECONDS_ELAPSED_ATTENTION', 'STATUS_ATTENTION','PHONE','NOTIFIY_CONTACT','RELATIONSHIP_CONTACT','TROUBLE','TRANSPORTATION_METHOD','TROUBLE_DATE','TROUBLE_TIME','TROUBLE_PLACE','TROUBLE_ADDRESS','EMERGENCY_TYPE','VITAL_STATUS','REFERENCE','MARITAL_STATUS','DATE_CREATED', 'TIME_CREATED'], 'required'],
            [['ID_ERP', 'ID_TRG', 'ID_USR_ADMISION', 'AGE', 'SECONDS_ELAPSED_ATTENTION','ESCORT_ID'], 'integer'],
            [['PHONE','ESCORT_PHONE'], 'string'],            
            [['MARITAL_STATUS', 'IESS_AFFILIATE', 'ISSFA_AFFILIATE', 'ISSPOL_AFFILIATE', 'SOAT', 'VITAL_STATUS', 'REFERENCE'], 'string'],
            [['TROUBLE_DATE', 'TROUBLE_TIME','AGE','CODE_008','MODIFIED','DATE_CREATED', 'TIME_CREATED'], 'safe'],
            [['EMERGENCY_TYPE'], 'string', 'max' => 30],
            [['ADDRESS', 'ADDRESS_CONTACT', 'ESCORT_ADDRESS', 'TROUBLE_ADDRESS'], 'string', 'max' => 150],
            [['NOTIFIY_CONTACT', 'RELATIONSHIP_CONTACT', 'OBSERVATION_ADMISION'], 'string', 'max' => 100],
            [['ESCORT'], 'string', 'max' => 50],            
            [['TRANSPORTATION_METHOD'], 'string', 'max' => 40],
            [['TROUBLE'], 'string', 'max' => 200],
            [['TROUBLE_PLACE'], 'string', 'max' => 60],
            [['STATUS_ATTENTION'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_ERL' => 'Id  Erl',
            'ID_ERP' => 'Paciente',
            'CODE_008' => 'Código 008',
            'ID_TRG' => 'Id  Triage',
            'ID_USR_ADMISION' => 'Admisionista',
            'MARITAL_STATUS' => 'Estado Civil',
            'AGE' => 'Edad',
            'IESS_AFFILIATE' => 'Afiliado al IESS',
            'ISSFA_AFFILIATE' => 'Afiliado al Issfa',
            'ISSPOL_AFFILIATE' => 'Afilliado al Isspol',
            'SOAT' => 'Soat',
            'ADDRESS' => 'Dirección Residencial',
            'PHONE' => 'Teléfono Paciente',
            'NOTIFIY_CONTACT' => 'Avisar a',
            'RELATIONSHIP_CONTACT' => 'Parentesco',
            'ADDRESS_CONTACT' => 'Dirección',
            'ESCORT' => 'Acompañante',
            'ESCORT_ID' => 'Cédula',
            'ESCORT_ADDRESS' => 'Dirección',
            'ESCORT_PHONE' => 'Teléfono',
            'TRANSPORTATION_METHOD' => 'Transporte de Llegada',
            'TROUBLE' => 'Causa o Motivo de Atención',
            'TROUBLE_DATE' => 'Fecha Problema',
            'TROUBLE_TIME' => 'Hora Problema',
            'TROUBLE_PLACE' => 'Lugar Ocurrencia',
            'TROUBLE_ADDRESS' => 'Dirección Ocurrencia',
            'EMERGENCY_TYPE' => 'Tipo de Emergencia',
            'VITAL_STATUS' => 'Estado de Ingreso',
            'REFERENCE' => 'Referencia',
            'OBSERVATION_ADMISION' => 'Nota/Observaciones',
            'SECONDS_ELAPSED_ATTENTION' => 'Tiempo Transcurrido desde Triage',
            'DATE_CREATED' => 'Fecha Creación',
            'TIME_CREATED' => 'Hora Creación',
            'MODIFIED' => 'Última modificación',            
            'STATUS_ATTENTION' => 'Estado',              
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDERP()
    {
        return $this->hasOne(ErPatient::className(), ['ID_ERP' => 'ID_ERP']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDTRG()
    {
        return $this->hasOne(Triage::className(), ['ID_TRG' => 'ID_TRG']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDUSRADMISION()
    {
        return $this->hasOne(Users::className(), ['ID_USR' => 'ID_USR_ADMISION']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutflows()
    {
        return $this->hasMany(Outflow::className(), ['ID_ERL' => 'ID_ERL']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuickCares()
    {
        return $this->hasMany(QuickCare::className(), ['ID_ERL' => 'ID_ERL']);
    }
    
    
    //Relation Many-Many N:M mediante bridge table    
    public function getEmployee(){
        return $this->hasOne(Employee::className(), ['ID_EMP'=>'ID_EMP'])
            ->viaTable(Users::tableName(),['ID_USR'=>'ID_USR_ADMISION']);
    }   
    
}
