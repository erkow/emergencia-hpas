<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ErLog;

/**
 * ErLogSearch represents the model behind the search form about `app\models\ErLog`.
 */
class ErLogSearch extends ErLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_ERL', 'ID_ERP', 'ID_TRG', 'AGE', 'SECONDS_ELAPSED_ATTENTION'], 'integer'],            
            [['CODE_008', 'MARITAL_STATUS', 'ID_USR_ADMISION','IESS_AFFILIATE', 'ISSFA_AFFILIATE', 'ISSPOL_AFFILIATE', 'SOAT', 'ADDRESS', 'PHONE', 'NOTIFIY_CONTACT', 'RELATIONSHIP_CONTACT', 'ADDRESS_CONTACT', 'ESCORT', 'ESCORT_ID', 'ESCORT_ADDRESS', 'ESCORT_PHONE', 'TRANSPORTATION_METHOD', 'TROUBLE', 'TROUBLE_DATE', 'TROUBLE_TIME', 'TROUBLE_PLACE', 'TROUBLE_ADDRESS', 'EMERGENCY_TYPE', 'VITAL_STATUS', 'REFERENCE', 'OBSERVATION_ADMISION', 'STATUS_ATTENTION', 'DATE_CREATED', 'TIME_CREATED', 'MODIFIED'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ErLog::find()->where([ 
               '=', 'STATUS_ATTENTION','ACTIVO'
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith('employee');

        $query->andFilterWhere([
            'ID_ERL' => $this->ID_ERL,
            'ID_ERP' => $this->ID_ERP,
            'ID_TRG' => $this->ID_TRG,          
            'AGE' => $this->AGE,
            'TROUBLE_DATE' => $this->TROUBLE_DATE,
            'TROUBLE_TIME' => $this->TROUBLE_TIME,
            'SECONDS_ELAPSED_ATTENTION' => $this->SECONDS_ELAPSED_ATTENTION,
            'DATE_CREATED' => $this->DATE_CREATED,
            'TIME_CREATED' => $this->TIME_CREATED,
            'MODIFIED' => $this->MODIFIED,
        ]);

        $query->andFilterWhere(['like', 'CODE_008', $this->CODE_008])
            ->andFilterWhere(['like', 'MARITAL_STATUS', $this->MARITAL_STATUS])
            ->andFilterWhere(['like', 'IESS_AFFILIATE', $this->IESS_AFFILIATE])
            ->andFilterWhere(['like', 'ISSFA_AFFILIATE', $this->ISSFA_AFFILIATE])
            ->andFilterWhere(['like', 'ISSPOL_AFFILIATE', $this->ISSPOL_AFFILIATE])
            ->andFilterWhere(['like', 'SOAT', $this->SOAT])
            ->andFilterWhere(['like', 'ADDRESS', $this->ADDRESS])
            ->andFilterWhere(['like', 'PHONE', $this->PHONE])
            ->andFilterWhere(['like', 'NOTIFIY_CONTACT', $this->NOTIFIY_CONTACT])
            ->andFilterWhere(['like', 'RELATIONSHIP_CONTACT', $this->RELATIONSHIP_CONTACT])
            ->andFilterWhere(['like', 'ADDRESS_CONTACT', $this->ADDRESS_CONTACT])
            ->andFilterWhere(['like', 'ESCORT', $this->ESCORT])
            ->andFilterWhere(['like', 'ESCORT_ID', $this->ESCORT_ID])
            ->andFilterWhere(['like', 'ESCORT_ADDRESS', $this->ESCORT_ADDRESS])
            ->andFilterWhere(['like', 'ESCORT_PHONE', $this->ESCORT_PHONE])
            ->andFilterWhere(['like', 'TRANSPORTATION_METHOD', $this->TRANSPORTATION_METHOD])
            ->andFilterWhere(['like', 'TROUBLE', $this->TROUBLE])
            ->andFilterWhere(['like', 'TROUBLE_PLACE', $this->TROUBLE_PLACE])
            ->andFilterWhere(['like', 'TROUBLE_ADDRESS', $this->TROUBLE_ADDRESS])
            ->andFilterWhere(['like', 'EMERGENCY_TYPE', $this->EMERGENCY_TYPE])
            ->andFilterWhere(['like', 'VITAL_STATUS', $this->VITAL_STATUS])
            ->andFilterWhere(['like', 'REFERENCE', $this->REFERENCE])
            ->andFilterWhere(['like', 'OBSERVATION_ADMISION', $this->OBSERVATION_ADMISION])
            ->andFilterWhere([
                'or',
                ['like', 'employee.LAST_NAME', $this->ID_USR_ADMISION],
                ['like', 'employee.FIRST_NAME', $this->ID_USR_ADMISION],
            ])
            ->andFilterWhere(['like', 'STATUS_ATTENTION', $this->STATUS_ATTENTION])
            ->addOrderBy("DATE_CREATED DESC, TIME_CREATED DESC");

        return $dataProvider;
    }
    
    
    /**
     * Creates data provider instance with search query applied
     * Searches ErLogs with no outflow related. This is to create in the Outflow page
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchForOutflow($params)
    {
        $query = ErLog::find()->where([ 
               '=', 'STATUS_ATTENTION','ACTIVO'
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith('employee');

        $query->andFilterWhere([
            'ID_ERL' => $this->ID_ERL,
            'ID_ERP' => $this->ID_ERP,
            'ID_TRG' => $this->ID_TRG,          
            'AGE' => $this->AGE,
            'TROUBLE_DATE' => $this->TROUBLE_DATE,
            'TROUBLE_TIME' => $this->TROUBLE_TIME,
            'SECONDS_ELAPSED_ATTENTION' => $this->SECONDS_ELAPSED_ATTENTION,
            'DATE_CREATED' => $this->DATE_CREATED,
            'TIME_CREATED' => $this->TIME_CREATED,
            'MODIFIED' => $this->MODIFIED,
        ]);

        $query->andFilterWhere(['like', 'CODE_008', $this->CODE_008])
            ->andFilterWhere(['like', 'MARITAL_STATUS', $this->MARITAL_STATUS])
            ->andFilterWhere(['like', 'IESS_AFFILIATE', $this->IESS_AFFILIATE])
            ->andFilterWhere(['like', 'ISSFA_AFFILIATE', $this->ISSFA_AFFILIATE])
            ->andFilterWhere(['like', 'ISSPOL_AFFILIATE', $this->ISSPOL_AFFILIATE])
            ->andFilterWhere(['like', 'SOAT', $this->SOAT])
            ->andFilterWhere(['like', 'ADDRESS', $this->ADDRESS])
            ->andFilterWhere(['like', 'PHONE', $this->PHONE])
            ->andFilterWhere(['like', 'NOTIFIY_CONTACT', $this->NOTIFIY_CONTACT])
            ->andFilterWhere(['like', 'RELATIONSHIP_CONTACT', $this->RELATIONSHIP_CONTACT])
            ->andFilterWhere(['like', 'ADDRESS_CONTACT', $this->ADDRESS_CONTACT])
            ->andFilterWhere(['like', 'ESCORT', $this->ESCORT])
            ->andFilterWhere(['like', 'ESCORT_ID', $this->ESCORT_ID])
            ->andFilterWhere(['like', 'ESCORT_ADDRESS', $this->ESCORT_ADDRESS])
            ->andFilterWhere(['like', 'ESCORT_PHONE', $this->ESCORT_PHONE])
            ->andFilterWhere(['like', 'TRANSPORTATION_METHOD', $this->TRANSPORTATION_METHOD])
            ->andFilterWhere(['like', 'TROUBLE', $this->TROUBLE])
            ->andFilterWhere(['like', 'TROUBLE_PLACE', $this->TROUBLE_PLACE])
            ->andFilterWhere(['like', 'TROUBLE_ADDRESS', $this->TROUBLE_ADDRESS])
            ->andFilterWhere(['like', 'EMERGENCY_TYPE', $this->EMERGENCY_TYPE])
            ->andFilterWhere(['like', 'VITAL_STATUS', $this->VITAL_STATUS])
            ->andFilterWhere(['like', 'REFERENCE', $this->REFERENCE])
            ->andFilterWhere(['like', 'OBSERVATION_ADMISION', $this->OBSERVATION_ADMISION])
            ->andFilterWhere([
                'or',
                ['like', 'employee.LAST_NAME', $this->ID_USR_ADMISION],
                ['like', 'employee.FIRST_NAME', $this->ID_USR_ADMISION],
            ])
            ->andFilterWhere(['like', 'STATUS_ATTENTION', $this->STATUS_ATTENTION])
            ->addOrderBy("DATE_CREATED DESC, TIME_CREATED DESC");

        return $dataProvider;
    }
    
}
