<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "er_patient".
 *
 * @property integer $ID_ERP
 * @property integer $ID_NT
 * @property integer $ID
 * @property string $FIRST_NAME
 * @property string $LAST_NAME
 * @property string $GENDER
 * @property string $BIRTHDAY
 * @property integer $CLINIC_NUMBER
 * @property string $CREATED
 * @property string $MODIFIED
 * @property string $STATUS
 *
 * @property CheckIn[] $checkIns
 * @property Nationality $iDNT
 */
class ErPatient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'er_patient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['STATUS', 'FIRST_NAME', 'LAST_NAME','ID_NT','CREATED','MODIFIED','GENDER','BIRTHDAY'], 'required'],
            [['ID_NT', 'ID', 'CLINIC_NUMBER'], 'integer'],
            [['GENDER', 'STATUS'], 'string'],  
            //REGLA PERSONALIZADA, abajo se crea la función para validarlo, y toca agregar al form al inicio enableAjaxValidation
            //además hay que poner el Controlador al moemtno de guardar que indique si hubo errores en la validación Ajax
            [['BIRTHDAY'],'revisarFechaNoMayorAHoy'],
            [['CREATED', 'MODIFIED'], 'safe'],
            [['FIRST_NAME', 'LAST_NAME'], 'string', 'max' => 45]
        ];
    }
    
    public function revisarFechaNoMayorAHoy($attribute, $params){
        $today = date ('Y-m-d');
        $selectedDate = date($this->BIRTHDAY);
        if($selectedDate > $today){
            $this->addError($attribute,'La Fecha de Nacimiento debe ser menor o igual a la fecha actual');
        }
    }
    
    public function getFull_Name() {
        return $this->LAST_NAME . ' ' . $this->FIRST_NAME;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_ERP' => 'Id  Erp',
            'ID_NT' => 'Nacionalidad',
            'ID' => 'Identificación',
            'FIRST_NAME' => 'Nombres',
            'LAST_NAME' => 'Apellidos',
            'GENDER' => 'Género',
            'BIRTHDAY' => 'Fecha de Nacimiento',
            'CLINIC_NUMBER' => 'Historia Clínica',
            'CREATED' => 'Creado',
            'MODIFIED' => 'Modificado',
            'STATUS' => 'Estado',
            'Full_Name'=>'Paciente'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckIns()
    {
        return $this->hasMany(CheckIn::className(), ['ID_ERP' => 'ID_ERP']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDNT()
    {
        return $this->hasOne(Nationality::className(), ['ID_NT' => 'ID_NT']);
    }
}
