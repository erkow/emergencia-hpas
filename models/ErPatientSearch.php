<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ErPatient;

/**
 * ErPatientSearch represents the model behind the search form about `app\models\ErPatient`.
 */
class ErPatientSearch extends ErPatient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_ERP', 'ID', 'CLINIC_NUMBER'], 'integer'],
            [['FIRST_NAME', 'LAST_NAME', 'GENDER', 'BIRTHDAY', 'CREATED', 'MODIFIED', 'STATUS','ID_NT'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ErPatient::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith('iDNT');

        $query->andFilterWhere([
            'ID_ERP' => $this->ID_ERP,            
            'ID' => $this->ID,
            'BIRTHDAY' => $this->BIRTHDAY,
            'CLINIC_NUMBER' => $this->CLINIC_NUMBER,
            'CREATED' => $this->CREATED,
            'MODIFIED' => $this->MODIFIED,
        ]);

        $query->andFilterWhere(['like', 'FIRST_NAME', $this->FIRST_NAME])
            ->andFilterWhere(['like', 'LAST_NAME', $this->LAST_NAME])
            ->andFilterWhere(['like', 'GENDER', $this->GENDER])
            ->andFilterWhere(['like', 'STATUS', $this->STATUS])
            ->andFilterWhere(['like', 'nationality.NATIONALITY', $this->ID_NT]);
//                          ABOVE IS TABLE NAME AND FIELD,        HERE IS THE NAME OF THE FIELD FORM
        return $dataProvider;
    }
}
