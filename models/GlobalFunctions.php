<?php
namespace app\models;

use Yii;

abstract class GlobalFunctions{
    
    public function dd (){
        
        return 1;
    }
    
    
    public static function getSecondsElapsedByDateTime ($oldDate,$oldTime){
        $oldTimeAndDate = $oldDate.' '.$oldTime;
        $oldTimeAndDate  = strtotime(str_replace('/','-',$oldTimeAndDate));
        $timeActual = strtotime('now');
        $differenceInSeconds = $timeActual - $oldTimeAndDate;
        
        return $differenceInSeconds;
    }
    
    public static function CalculaEdad($fecha) {
        list($Y, $m, $d) = explode("-", $fecha);
        return( date("md") < $m . $d ? date("Y") - $Y - 1 : date("Y") - $Y );
    }

    
    
}
