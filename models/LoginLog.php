<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "login_log".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $payment_holder
 * @property string $user_name
 * @property string $access_date
 */
class LoginLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'login_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'payment_holder', 'user_name'], 'required'],
            [['user_id', 'payment_holder'], 'integer'],
            [['access_date'], 'safe'],
            [['user_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'payment_holder' => 'Payment Holder',
            'user_name' => 'User Name',
            'access_date' => 'Access Date',
        ];
    }
}
