<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nationality".
 *
 * @property integer $ID_NT
 * @property string $NATIONALITY
 * @property string $STATUS
 *
 * @property ErPatient[] $erPatients
 */
class Nationality extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nationality';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['STATUS','NATIONALITY'], 'required'],            
            [['STATUS'], 'string'],
            [['NATIONALITY'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_NT' => 'Id  Nt',
            'NATIONALITY' => 'Nacionalidad',
            'STATUS' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErPatients()
    {
        return $this->hasMany(ErPatient::className(), ['ID_NT' => 'ID_NT']);
    }
}
