<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "outflow".
 *
 * @property integer $ID_OF
 * @property integer $ID_ERL
 * @property integer $ID_USR_ADMISION
 * @property string $ID_CIE10 
 * @property string $CONTUTION
 * @property string $CAUSE_EFFECT
 * @property string $DESTINATION
 * @property string $TRANSFERED
 * @property string $SPECIALITY
 * @property string $DOCTOR_OUTFLOW
 * @property string $DATE_OUTFLOW
 * @property string $TIME_OUTFLOW
 * @property string $OBSERVATION_ATTENTION
 * @property integer $SECONDS_ELAPSED_ATTENTION
 * @property string $MODIFIED 
 * @property string $STATUS_ATTENTION
 *
 * @property Cie10 $iDCIE10 
 * @property ErLog $iDERL
 * @property Users $iDUSRADMISION
 */
class Outflow extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'outflow';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_USR_ADMISION', 'DESTINATION', 'SPECIALITY', 'DOCTOR_OUTFLOW', 'DATE_OUTFLOW', 'TIME_OUTFLOW', 'SECONDS_ELAPSED_ATTENTION','ID_CIE10'], 'required'],
            [['ID_ERL', 'ID_USR_ADMISION', 'SECONDS_ELAPSED_ATTENTION'], 'integer'],
            [['DESTINATION', 'SPECIALITY', 'STATUS_ATTENTION','ID_CIE10'], 'string'],
            [['DATE_OUTFLOW', 'TIME_OUTFLOW','ID_ERL'], 'safe'],
            [['CONTUTION', 'CAUSE_EFFECT', 'TRANSFERED', 'DOCTOR_OUTFLOW'], 'string', 'max' => 100],
            [['OBSERVATION_ATTENTION'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_OF' => 'Id  Of',
            'ID_ERL' => 'Id  Erl',
            'ID_USR_ADMISION' => 'ID Admisonista',
            'ID_CIE10' => 'Diagnóstico de Egreso',
            'CONTUTION' => 'Contusión',
            'CAUSE_EFFECT' => 'Causa/Efecto',
            'DESTINATION' => 'Destino',
            'TRANSFERED' => 'Transferido',
            'SPECIALITY' => 'Especialidad',
            'DOCTOR_OUTFLOW' => 'Médico',
            'DATE_OUTFLOW' => 'Fecha Salida',
            'TIME_OUTFLOW' => 'Hora Salida',
            'OBSERVATION_ATTENTION' => 'Observación',
            'SECONDS_ELAPSED_ATTENTION' => 'Tiempo Transcurrido',
            'MODIFIED' =>'Última Modificación',
            'STATUS_ATTENTION' => 'Estado',
        ];
    }

    /** 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getIDCIE10() 
   { 
       return $this->hasOne(Cie10::className(), ['ID_CIE10' => 'ID_CIE10']); 
   }    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDERL()
    {
        return $this->hasOne(ErLog::className(), ['ID_ERL' => 'ID_ERL']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDUSRADMISION()
    {
        return $this->hasOne(Users::className(), ['ID_USR' => 'ID_USR_ADMISION']);
    }
    
        //Relation Many-Many N:M mediante bridge table    
    public function getEmployee(){
        return $this->hasOne(Employee::className(), ['ID_EMP'=>'ID_EMP'])
            ->viaTable(Users::tableName(),['ID_USR'=>'ID_USR_ADMISION']);
    }   
    
}
