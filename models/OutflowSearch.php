<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Outflow;

/**
 * OutflowSearch represents the model behind the search form about `app\models\Outflow`.
 */
class OutflowSearch extends Outflow
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_OF', 'ID_ERL', 'ID_USR_ADMISION', 'SECONDS_ELAPSED_ATTENTION'], 'integer'],
            [['CONTUTION', 'CAUSE_EFFECT', 'DESTINATION', 'TRANSFERED', 'SPECIALITY', 'DOCTOR_OUTFLOW', 'DATE_OUTFLOW', 'TIME_OUTFLOW', 'OBSERVATION_ATTENTION', 'MODIFIED', 'STATUS_ATTENTION'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Outflow::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID_OF' => $this->ID_OF,
            'ID_ERL' => $this->ID_ERL,
            'ID_USR_ADMISION' => $this->ID_USR_ADMISION,
            'DATE_OUTFLOW' => $this->DATE_OUTFLOW,
            'TIME_OUTFLOW' => $this->TIME_OUTFLOW,
            'SECONDS_ELAPSED_ATTENTION' => $this->SECONDS_ELAPSED_ATTENTION,
            'MODIFIED' => $this->MODIFIED,
        ]);

        $query->andFilterWhere(['like', 'CONTUTION', $this->CONTUTION])
            ->andFilterWhere(['like', 'CAUSE_EFFECT', $this->CAUSE_EFFECT])
            ->andFilterWhere(['like', 'DESTINATION', $this->DESTINATION])
            ->andFilterWhere(['like', 'TRANSFERED', $this->TRANSFERED])
            ->andFilterWhere(['like', 'SPECIALITY', $this->SPECIALITY])
            ->andFilterWhere(['like', 'DOCTOR_OUTFLOW', $this->DOCTOR_OUTFLOW])
            ->andFilterWhere(['like', 'OBSERVATION_ATTENTION', $this->OBSERVATION_ATTENTION])
            ->andFilterWhere(['like', 'STATUS_ATTENTION', $this->STATUS_ATTENTION]);

        return $dataProvider;
    }
}
