<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property integer $ID_PG
 * @property string $URL
 * @property string $STATUS
 *
 * @property PagesByProfile[] $pagesByProfiles
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['STATUS', 'URL'], 'required'],            
            [['STATUS'], 'string'],
            [['URL'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_PG' => 'Id Pg',
            'URL' => 'Url',
            'STATUS' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagesByProfiles()
    {
        return $this->hasMany(PagesByProfile::className(), ['ID_PG' => 'ID_PG']);
    }
}
