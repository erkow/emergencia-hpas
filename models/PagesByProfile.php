<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pages_by_profile".
 *
 * @property integer $ID_PP
 * @property integer $ID_PG
 * @property integer $ID_PRF
 * @property string $STATUS
 *
 * @property Profiles $iDPRF
 * @property Pages $iDPG
 */
class PagesByProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages_by_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_PG', 'ID_PRF'], 'required'],
            [['ID_PG', 'ID_PRF'], 'integer'],
            [['STATUS'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_PP' => 'Id  Página x Perfil',
            'ID_PG' => 'Id  Página',
            'ID_PRF' => 'Id  Perfil',
            'STATUS' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDPRF()
    {
        return $this->hasOne(Profiles::className(), ['ID_PRF' => 'ID_PRF']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDPG()
    {
        return $this->hasOne(Pages::className(), ['ID_PG' => 'ID_PG']);
    }
}
