<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PagesByProfile;

/**
 * PagesByProfileSearch represents the model behind the search form about `app\models\PagesByProfile`.
 */
class PagesByProfileSearch extends PagesByProfile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_PP'], 'integer'],
            [['STATUS','ID_PG','ID_PRF'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PagesByProfile::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith('iDPRF');
        $query->joinWith('iDPG');

        $query->andFilterWhere([
            'ID_PP' => $this->ID_PP           
        ]);

        $query->andFilterWhere(['like', 'STATUS', $this->STATUS])
                 ->andFilterWhere(['like', 'pages.URL', $this->ID_PG])
                ->andFilterWhere(['like', 'profiles.PROFILE', $this->ID_PRF]);

        return $dataProvider;
    }
}
