<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "priority".
 *
 * @property integer $ID_PRT
 * @property string $PRIORITY
 * @property string $STATUS
 *
 * @property Triage[] $triages
 */
class Priority extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'priority';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['STATUS', 'PRIORITY'], 'required'],            
            [['STATUS'], 'string'],
            [['PRIORITY'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_PRT' => 'Id  Prt',
            'PRIORITY' => 'Prioridad',
            'STATUS' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTriages()
    {
        return $this->hasMany(Triage::className(), ['ID_PRT' => 'ID_PRT']);
    }
}
