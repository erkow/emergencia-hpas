<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profiles".
 *
 * @property integer $ID_PRF
 * @property string $PROFILE
 * @property string $STATUS
 *
 * @property PagesByProfile[] $pagesByProfiles
 * @property UserProfiles[] $userProfiles
 */
class Profiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['STATUS', 'PROFILE'], 'required'],            
            [['STATUS'], 'string'],
            [['PROFILE'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_PRF' => 'Id  Prf',
            'PROFILE' => 'Perfil',
            'STATUS' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagesByProfiles()
    {
        return $this->hasMany(PagesByProfile::className(), ['ID_PRF' => 'ID_PRF']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfiles()
    {
        return $this->hasMany(UserProfiles::className(), ['ID_PRF' => 'ID_PRF']);
    }
}
