<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quick_care".
 *
 * @property integer $ID_QC
 * @property integer $ID_ERL
 * @property integer $ID_DST
 * @property integer $ID_DG
 * @property integer $ID_USR_DOCTOR
 * @property string $DIAGNOSIS_ATTENTION
 * @property string $DATE_IN_ATTENTION
 * @property string $HOUR_IN_ATTENTION
 * @property string $DATE_OUT_ATTENTION
 * @property string $TIME_OUT_ATTENTION
 * @property string $OBSERVATION_ATTENTION
 * @property integer $SECONDS_ELAPSED_ATTENTION
 * @property string $STATUS_ATTENTION
 *
 * @property Users $iDUSRDOCTOR
 * @property Destination $iDDST
 * @property Diagnosis $iDDG
 * @property ErLog $iDERL
 */
class QuickCare extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quick_care';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_QC', 'ID_ERL', 'ID_USR_DOCTOR', 'DIAGNOSIS_ATTENTION', 'DATE_IN_ATTENTION', 'HOUR_IN_ATTENTION', 'SECONDS_ELAPSED_ATTENTION'], 'required'],
            [['ID_QC', 'ID_ERL', 'ID_DST', 'ID_DG', 'ID_USR_DOCTOR', 'SECONDS_ELAPSED_ATTENTION'], 'integer'],
            [['DATE_IN_ATTENTION', 'HOUR_IN_ATTENTION', 'DATE_OUT_ATTENTION', 'TIME_OUT_ATTENTION'], 'safe'],
            [['STATUS_ATTENTION'], 'string'],
            [['DIAGNOSIS_ATTENTION', 'OBSERVATION_ATTENTION'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_QC' => 'Id  Qc',
            'ID_ERL' => 'Id  Erl',
            'ID_DST' => 'Id  Dst',
            'ID_DG' => 'Id  Dg',
            'ID_USR_DOCTOR' => 'Id  Usr  Doctor',
            'DIAGNOSIS_ATTENTION' => 'Diagnosis  Attention',
            'DATE_IN_ATTENTION' => 'Date  In  Attention',
            'HOUR_IN_ATTENTION' => 'Hour  In  Attention',
            'DATE_OUT_ATTENTION' => 'Date  Out  Attention',
            'TIME_OUT_ATTENTION' => 'Time  Out  Attention',
            'OBSERVATION_ATTENTION' => 'Observation  Attention',
            'SECONDS_ELAPSED_ATTENTION' => 'Seconds  Elapsed  Attention',
            'STATUS_ATTENTION' => 'Status  Attention',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDUSRDOCTOR()
    {
        return $this->hasOne(Users::className(), ['ID_USR' => 'ID_USR_DOCTOR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDDST()
    {
        return $this->hasOne(Destination::className(), ['ID_DST' => 'ID_DST']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDDG()
    {
        return $this->hasOne(Diagnosis::className(), ['ID_DG' => 'ID_DG']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDERL()
    {
        return $this->hasOne(ErLog::className(), ['ID_ERL' => 'ID_ERL']);
    }
}
