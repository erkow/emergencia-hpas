<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "triage".
 *
 * @property integer $ID_TRG
 * @property integer $ID_CI
 * @property integer $ID_USR
 * @property integer $ID_PRT
 * @property integer $ID_CIE10
 * @property string $DATE_IN
 * @property string $TIME_IN
 * @property string $DATE_OUT
 * @property string $TIME_OUT
 * @property integer $SECONDS_ELAPSED
 * @property string $OBSERVATION
 * @property string $STATUS
 *
 * @property ErLog[] $erLogs
 * @property Users $iDUSR
 * @property Cie10 $iDCIE10
 * @property CheckIn $iDCI
 * @property Priority $iDPRT
 */

class Triage extends \yii\db\ActiveRecord
{
//    public $ID_ERP;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'triage';
    }
    
    
    /**
     * @return array
     */
    public static function getTypesList() {        
            return $droptions = ['WAITING' => "EN ESPERA", 'ABSENT' => 'NO ACUDE','ATTENDED'=>'ATENDIDO'];
    }    

    /**
     * @return string
     */
    public function getStatusLabel() {
        return self::getTypesList()[$this->STATUS];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_CI', 'DATE_IN', 'TIME_IN','STATUS'], 'required','on'=>'create'],
            [['DATE_IN', 'TIME_IN', 'DATE_OUT', 'TIME_OUT'], 'safe','on'=>'create'],
            [['ID_PRT','ID_CIE10'], 'required','on'=>'update'],
            [['DATE_IN', 'TIME_IN', 'DATE_OUT', 'TIME_OUT','ID_CI','ID_TRG','ID_ERP','STATUS'], 'safe','on'=>'update'],
            [['ID_TRG', 'ID_CI', 'ID_USR', 'ID_PRT', 'SECONDS_ELAPSED'], 'integer'],            
            [['STATUS'], 'string'],
            [['ID_CIE10'], 'string', 'max' => 10],
            [['OBSERVATION'], 'string', 'max' => 200]            
        ];
    }
    
    //PERMITE DEFINIR REGLAS PARA ESCENARIOS, POR EJEMPLO AL CREAR QUE SE OBLIGATORIO O NO UN CAMPO, DIFERENTE DE UPDATE
    //PRIMERO SE DEFINEN LOS ESCENARIOS LUEGO EN REGLAS SE PONE CUALES SON
    //SEGUNDO EN EL CONTROLADOR SE DEFINE CUAL ESCENARIO SE APLICA
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['ID_PRT','STATUS','ID_CIE10'];//FIELDS REQUIRED FOR UPDATE
        $scenarios['create'] = ['ID_CI','DATE_IN','TIME_IN','STATUS'];//FIELDS REQUIRED FOR CREATE
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_TRG' => 'Id  Trg',
            'ID_CI' => 'Problema',
            'ID_ERP' => 'Paciente',
            'ID_USR' => 'Usuario',
            'ID_PRT' => 'Prioridad',
            'ID_CIE10' => 'Diagnóstico Preliminar',
            'DATE_IN' => 'Fecha de Ingreso',
            'TIME_IN' => 'Hora de Ingreso',
            'DATE_OUT' => 'Fecha de Salida',
            'TIME_OUT' => 'Hora de Salida',
            'SECONDS_ELAPSED' => 'Tiempo Transcurrido',
            'OBSERVATION' => 'Observación',
            'STATUS' => 'Estado',            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErLogs()
    {
        return $this->hasMany(ErLog::className(), ['ID_TRG' => 'ID_TRG']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDUSR()
    {
        return $this->hasOne(Users::className(), ['ID_USR' => 'ID_USR']);
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDCIE10()
       {           
           return $this->hasOne(Cie10::className(), ['ID_CIE10' => 'ID_CIE10']);
       }    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDCI()
    {
        return $this->hasOne(CheckIn::className(), ['ID_CI' => 'ID_CI']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDPRT()
    {
        return $this->hasOne(Priority::className(), ['ID_PRT' => 'ID_PRT']);
    }
    
    //Relation Many-Many N:M mediante bridge table    
    public function getErPatient(){
        return $this->hasOne(ErPatient::className(), ['ID_ERP'=>'ID_ERP'])
            ->viaTable(CheckIn::tableName(),['ID_CI'=>'ID_CI']);
    }   
    
}
