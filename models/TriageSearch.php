<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Triage;

/**
 * TriageSearch represents the model behind the search form about `app\models\Triage`.
 */
class TriageSearch extends Triage
{
    //ESTO ES PARA PODER BUSCAR CUANDO HAY UNA TERCERA TABLA EXTERNA, AL FINAL HAY QUE AGREGAR AL QUERY
    public $paciente;
    public $idErPatient;
    public $ageErPatient;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_TRG', 'ID_USR', 'SECONDS_ELAPSED','idErPatient','ageErPatient'], 'integer'],
            [['ID_CIE10','DATE_IN', 'TIME_IN', 'DATE_OUT', 'ID_PRT','TIME_OUT', 'OBSERVATION', 'STATUS','ID_CI','paciente'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $minAdultAge = 15;//FROM THIS AGE ON IS CONSIDERED ADULT, LESS AS CHILD
        
        if (Yii::$app->user->can('Perfil Triage Adultos') && Yii::$app->user->can('Perfil Triage Niños')) {//IN CASE IT HAS TWO PROFILES
            $vald = ">=";
            $minAdultAge = 0;//DISPLAY ALL AGES
        }elseif (Yii::$app->user->can('Perfil Triage Adultos')){
            $vald = ">="; //VALIDATION  CASE
        }elseif (Yii::$app->user->can('Perfil Triage Niños')) {
            $vald = "<"; //VALIDATION  CASE
        }else{
            $vald = ">=";
            $minAdultAge = 0;//DISPLAY ALL AGES
        }
        $dateTimeForAttended = date ('Y-m-d H:i:s',time() - 7400);//HORA ACTUAL MENOS 2 HORAS
        $dateTimeForAbsent = date ('Y-m-d H:i:s',time() - 14800);//HORA ACTUAL MENOS 4 HORAS
        $query = Triage::find()->where([
            
                'and',
                [
                    'or',
                    ['triage.STATUS' => 'WAITING'],
                    [
                        'and',
                        ['triage.STATUS' => 'ATTENDED'],
                        ['>=', "CONCAT(triage.DATE_OUT,' ',triage.TIME_OUT)", $dateTimeForAttended],
                    ],
                    [
                        'and',
                        ['triage.STATUS' => 'ABSENT'],
                        ['>=', "CONCAT(triage.DATE_OUT,' ',triage.TIME_OUT)", $dateTimeForAbsent],
                    ],
                ],
                [$vald, 'check_in.AGE',$minAdultAge],
            
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith('iDPRT');
//        $query->joinWith('iDCI');
        $query->joinWith('erPatient');

        $query->andFilterWhere([
            'ID_TRG' => $this->ID_TRG,            
            'ID_USR' => $this->ID_USR,            
            'DATE_IN' => $this->DATE_IN,
            'TIME_IN' => $this->TIME_IN,
            'DATE_OUT' => $this->DATE_OUT,
            'TIME_OUT' => $this->TIME_OUT,
            'SECONDS_ELAPSED' => $this->SECONDS_ELAPSED,
            'IDCI' => $this->iDCI,
        ]);

        $query->andFilterWhere(['like', 'OBSERVATION', $this->OBSERVATION])
           ->andFilterWhere(['like', 'triage.STATUS', $this->STATUS])
           ->andFilterWhere(['like','check_in.TROUBLE',$this->ID_CI])  
           ->andFilterWhere(['like', 'ID_CIE10', $this->ID_CIE10])
           ->andFilterWhere(['like', 'priority.PRIORITY', $this->ID_PRT])            
            ->andFilterWhere([
                'or',
                ['like', 'er_patient.LAST_NAME', $this->paciente],
                ['like', 'er_patient.FIRST_NAME', $this->paciente],
            ])

        ->addOrderBy("STATUS,DATE DESC, TIME DESC");

        return $dataProvider;       
    }
    
    
    /**
     * Creates data provider instance with search query applied
     * 
     * Returns only triages (not priority 5 this goes home) with no erlog related
     * The results are used to display and create a 008 
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchForErLog($params)
    {       
        $dateTimeForAttended = date ('Y-m-d H:i:s',time() - 7400);//HORA ACTUAL MENOS 2 HORAS        
        $query = Triage::find()->where([            
                'and',
                [                    
                    'and',
                    ['triage.STATUS' => 'ATTENDED'],
                    ['>=', "CONCAT(triage.DATE_OUT,' ',triage.TIME_OUT)", $dateTimeForAttended],                                       
                ],
                ['!=', 'triage.ID_PRT','5'],
                ['IS', 'er_log.ID_TRG',NULL],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith('iDPRT');
//        $query->joinWith('iDCI');
        $query->joinWith('erPatient');
        $query->joinWith('erLogs');

        $query->andFilterWhere([
            'ID_TRG' => $this->ID_TRG,            
            'ID_USR' => $this->ID_USR,            
            'DATE_IN' => $this->DATE_IN,
            'TIME_IN' => $this->TIME_IN,            
            'SECONDS_ELAPSED' => $this->SECONDS_ELAPSED,
            'IDCI' => $this->iDCI,   
            'check_in.AGE' => $this->ageErPatient,
        ]);

        $query->andFilterWhere(['like', 'OBSERVATION', $this->OBSERVATION])
            ->andFilterWhere(['like', 'triage.STATUS', $this->STATUS])
           ->andFilterWhere(['like','check_in.TROUBLE',$this->ID_CI])                
            ->andFilterWhere(['like', 'priority.PRIORITY', $this->ID_PRT])            
             ->andFilterWhere([
                'or',
                ['like', 'er_patient.LAST_NAME', $this->paciente],
                ['like', 'er_patient.FIRST_NAME', $this->paciente],
            ])
           ->andFilterWhere(['like','er_patient.ID',$this->idErPatient])
           ->andFilterWhere(['like','DATE_OUT',$this->DATE_OUT])
           ->andFilterWhere(['like','TIME_OUT',$this->TIME_OUT])

        ->addOrderBy("STATUS,DATE_OUT DESC, TIME_OUT DESC");

        return $dataProvider;       
    }
    
    
}
