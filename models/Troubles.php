<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "troubles".
 *
 * @property integer $ID_TRB
 * @property string $TROUBLE
 * @property string $STATUS
 *
 * @property CheckIn[] $checkIns
 */
class Troubles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'troubles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_TRB', 'TROUBLE'], 'required'],
            [['ID_TRB'], 'integer'],
            [['STATUS'], 'string'],
            [['TROUBLE'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_TRB' => 'Id  Trb',
            'TROUBLE' => 'Trouble',
            'STATUS' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckIns()
    {
        return $this->hasMany(CheckIn::className(), ['ID_TRB' => 'ID_TRB']);
    }
}
