<?php

namespace app\models;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $ID_USR;
    public $ID_EMP;
    public $USERNAME;
    public $PASSWORD;
    public $DATE_MODIFIED;
    public $STATUS;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'USERNAME' => 'Usuario',
            'PASSWORD' => 'Contraseña',
            'STATUS' => 'Estado',
        ];
    }
    
    
    /**
     * @inheritdoc
     */
    /* Regresa el id del usuario */
    public function getId()
    {
        return $this->ID_USR;
    }

    /**
     * @inheritdoc
     */
    
    /* busca la identidad del usuario a través de su $id */
    
    public static function findIdentity($id)
    {
        $users = Users::find()
                ->where("STATUS=:STATUS", [":STATUS" => "ACTIVE"])
                ->andWhere("ID_USR=:ID_USR", ["ID_USR" => $id])
                ->one();

        return isset($users) ? new static($users) : null;        
    }

    

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    /* Busca la identidad del usuario a través del username */
    public static function findByUsername($username)
    {
        $users = Users::find()
                ->where("STATUS=:STATUS", ["STATUS" => "ACTIVE"])
                ->andWhere("USERNAME=:USERNAME", [":USERNAME" => $username])
                ->all();
        
        foreach ($users as $user) {
            if (strcasecmp($user->USERNAME, $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }
    
     /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->PASSWORD === $password;
    }

///ESTO ACONTINUACION SE USA SI SE TIENE ESE CAMPO EN BASE Y SI SE MARCA REMEMBER ME AL HACER LOGIN
    /**
     * @inheritdoc
     */
    /* Regresa la clave de autenticación */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    /* Valida la clave de autenticación */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
    
    /**
     * @inheritdoc
     */
    /* Busca la identidad del usuario a través de su token de acceso */
    public static function findIdentityByAccessToken($token, $type = null)
   {
        
        $users = Users::find()
                ->where("STATUS=:STATUS", [":STATUS" => "ACTIVE"])
                ->andWhere("accessToken=:accessToken", [":accessToken" => $token])
                ->all();
        
        foreach ($users as $user) {
            if ($user->accessToken === $token) {
                return new static($user);
            }
        }

        return null;
    }
   
}
