<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_profiles".
 *
 * @property integer $ID_UP
 * @property integer $ID_PRF
 * @property integer $ID_USR
 * @property string $STATUS
 *
 * @property Users $iDUSR
 * @property Profiles $iDPRF
 */
class UserProfiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_PRF', 'ID_USR'], 'required'],
            [['ID_PRF', 'ID_USR'], 'integer'],
            [['STATUS'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_UP' => 'Id  Up',
            'ID_PRF' => 'Perfil',
            'ID_USR' => 'Id  Usr',
            'STATUS' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDUSR()
    {
        return $this->hasOne(Users::className(), ['ID_USR' => 'ID_USR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDPRF()
    {
        return $this->hasOne(Profiles::className(), ['ID_PRF' => 'ID_PRF']);
    }
}
