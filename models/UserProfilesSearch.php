<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserProfiles;

/**
 * UserProfilesSearch represents the model behind the search form about `app\models\UserProfiles`.
 */
class UserProfilesSearch extends UserProfiles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_UP'], 'integer'],
            [['STATUS','ID_PRF','ID_USR'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserProfiles::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith('iDUSR');
        $query->joinWith('iDPRF');

        $query->andFilterWhere([
            'ID_UP' => $this->ID_UP            
        ]);

        $query->andFilterWhere(['like', 'STATUS', $this->STATUS])
              ->andFilterWhere(['like', 'users.USERNAME', $this->ID_USR])
              ->andFilterWhere(['like', 'profiles.PROFILE', $this->ID_PRF]);

        return $dataProvider;
    }
}
