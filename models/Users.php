<?php

namespace app\models;

use Yii;

//AGREGUÉ LOS SIGUIENTES NAMESPACES PARA HACER EL LOGIN, EL ULTIMO ES EL MAS IMPORTANTE
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;


/**
 * This is the model class for table "users".
 *
 * @property integer $ID_USR
 * @property integer $ID_EMP
 * @property string $USERNAME
 * @property string $PASSWORD
 * @property string $DATE_MODIFIED
 * @property string $STATUS
 *
 * @property AccessLog[] $accessLogs
 * @property CheckIn[] $checkIns
 * @property ErLog[] $erLogs
 * @property Outflow[] $outflows
 * @property QuickCare[] $quickCares
 * @property Triage[] $triages
 * @property UserProfiles[] $userProfiles
 * @property Employee $iDEMP
 */

//**AQUI ES NECESARIO AÑADIR EL IMPLEMENT PARA HACER USO DEL LOGIN
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{
    

    public static function getDb() {

        return Yii::$app->db;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['USERNAME', 'PASSWORD','STATUS'], 'required'],
            [['ID_USR', 'ID_EMP'], 'integer'],
            [['DATE_MODIFIED'], 'safe'],
            [['STATUS'], 'string'],
            [['USERNAME'], 'string', 'max' => 30],
            [['PASSWORD'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_USR' => 'Id  Usr',
            'ID_EMP' => 'Empleado',
            'USERNAME' => 'Usuario',
            'PASSWORD' => 'Contraseña',
            'DATE_MODIFIED' => 'Fecha de modificación',
            'STATUS' => 'Status',
        ];
    }

        
     /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function getLastLoginByUser($id_usr)
    {   
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            SELECT concat(DATE_LOG,' ',TIME_LOG ) as last_log  
            FROM `access_log` 
            WHERE `ID_USR` = :user_id order by date_log desc, time_log desc
            limit 1,1", [':user_id' => $id_usr]);

        $result = $command->queryScalar();
            
        if($result){//PONER AQUI EN PALABRAS EL DIA y los meses
            list($year, $month, $day,$hour,$minute) = array_values(date_parse($result));
            
                $dayNumericPdf = date("j",strtotime($result)); //ejm 31
            $timePdf = date('H:i',strtotime($result));
            $monthPdf = \Yii::$app->params['months'][date('F',strtotime($result))]; //January
            $yearPdf = date('Y',strtotime($result));
            $dayPdf = \Yii::$app->params['days'][date('l',strtotime($result))]; //ejm lunes
            
            $result = $dayPdf.', '.$dayNumericPdf.' de '.$monthPdf.' del '.$yearPdf.' a las '.$timePdf;
            
        }else{
            $result = 'Nunca';
        }
        
        return $result;
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessLogs()
    {
        return $this->hasMany(AccessLog::className(), ['ID_USR' => 'ID_USR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckIns()
    {
        return $this->hasMany(CheckIn::className(), ['ID_USR' => 'ID_USR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErLogs()
    {
        return $this->hasMany(ErLog::className(), ['ID_USR_ADMISION' => 'ID_USR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutflows()
    {
        return $this->hasMany(Outflow::className(), ['ID_USR_ADMISION' => 'ID_USR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuickCares()
    {
        return $this->hasMany(QuickCare::className(), ['ID_USR_DOCTOR' => 'ID_USR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTriages()
    {
        return $this->hasMany(Triage::className(), ['ID_USR' => 'ID_USR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfiles()
    {
        return $this->hasMany(UserProfiles::className(), ['ID_USR' => 'ID_USR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDEMP()
    {
        return $this->hasOne(Employee::className(), ['ID_EMP' => 'ID_EMP']);
    }
    
    
    /////////////*******************LOS SIGUIENTES METODOS SE USAN PARA AUTENTICAR, SI NO SE PONEN DA ERROR POR QUE SON STATIC, Y EL
    ////////////IMPLEMENT ANTERIOR LOS NECESITA
    /** INCLUDE USER LOGIN VALIDATION FUNCTIONS* */

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    /* modified */
    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(['access_token' => $token]);
    }


    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['USERNAME' => $username]);
    }

    /**
     * Finds user by password reset token
     *
     * @param  string      $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return $this->PASSWORD === sha1($password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Security::generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Security::generateRandomKey();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Security::generateRandomKey() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    /** EXTENSION MOVIE * */
}
