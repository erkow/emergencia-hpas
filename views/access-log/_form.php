<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AccessLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="access-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID_USR')->textInput() ?>

    <?= $form->field($model, 'DATE_LOG')->textInput() ?>

    <?= $form->field($model, 'TIME_LOG')->textInput() ?>

    <?= $form->field($model, 'DATE_LOG_OUT')->textInput() ?>

    <?= $form->field($model, 'TIME_LOG_OUT')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
