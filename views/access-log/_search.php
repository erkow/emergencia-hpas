<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AccessLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="access-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_AL') ?>

    <?= $form->field($model, 'ID_USR') ?>

    <?= $form->field($model, 'DATE_LOG') ?>

    <?= $form->field($model, 'TIME_LOG') ?>

    <?= $form->field($model, 'DATE_LOG_OUT') ?>

    <?php // echo $form->field($model, 'TIME_LOG_OUT') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Resetear', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
