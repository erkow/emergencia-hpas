<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AccessLog */

$this->title = 'Update Access Log: ' . ' ' . $model->ID_AL;
$this->params['breadcrumbs'][] = ['label' => 'Access Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_AL, 'url' => ['view', 'id' => $model->ID_AL]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="access-log-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
