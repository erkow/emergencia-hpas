<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\ErPatient;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Nationality;
use dosamigos\datepicker\DatePicker;
use yii\widgets\MaskedInput;
//use \kartik\datecontrol\Module;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\CheckIn */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="check-in-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>        
    
    <div class="form-group">
        <label class="control-label">Es Extranjero?</label> <input type="checkbox" id="checkForeign" name="checkForeign" />  &nbsp;      
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label >Viene sólo?</label> <input type="checkbox" id="checkIsAlone" name="checkIsAlone" />  
    </div>
    <hr style="height:1px;border:none;color:black;background-color:black;">
    
    <!--  CREAR PACIENTE INICIO  -->
    <?= $form->field($erPatient, 'ID')->textInput() ?>
    
    <input id="auxId" name="auxId" val="" hidden="true"/> 
    
    
    <?= $form->field($erPatient, 'FIRST_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($erPatient, 'LAST_NAME')->textInput(['maxlength' => true]) ?>   


    

    <?php 
    $erPatient->ID_NT=34;//DEFAULT ECUATORIANA
    ?>
    
    <?= $form->field($erPatient, 'ID_NT')->dropDownList(
            ArrayHelper::map(Nationality::find()->all(), 'ID_NT', 'NATIONALITY'), ['prompt' => '-Seleccione-']
    )
    ?>
    
    <?php //echo $form->field($erPatient, 'ID_NT')->widget(Select2::classname(), [
//        'data' => ArrayHelper::map(Nationality::find()->all(), 'ID_NT', 'NATIONALITY'),         
//        'language' => 'es',              
//        'options' => ['placeholder' => '-Seleccione-'],        
//        'pluginOptions' => [
//            'allowClear' => true
//        ],
//    ]);
    ?>

    <?= $form->field($erPatient, 'GENDER')->dropDownList([ 'MALE' => 'Masculino', 'FEMALE' => 'Femenino',], ['prompt' => '-Seleccione-']) ?>

    <?=
    $form->field($erPatient, 'BIRTHDAY')->widget(DateControl::classname(), [
    'displayFormat' => 'yyyy/MM/dd',
    'autoWidget' => false,
    'widgetClass' => 'yii\widgets\MaskedInput',
    'options' => [
        'mask' => '9999/99/99'
    ],
]);
    ?>
    <!--  FIN PACIENTE -->
    
    <hr style="height:1px;border:none;color:black;background-color:black;">
    
   
       
    <?= $form->field($model, 'ESCORT')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TROUBLE')->textInput(['maxlength' => true]) ?>    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id' => $model->isNewRecord ? 'createButton' : 'updateButton']) ?>
        <?= Html::a('Cancelar', ['/check-in/index'], ['class'=>'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>


<?php
$script = <<< JS
        
//CHECKS ON LOAD STATUS OF FOREIGN CHECKED
    if(!$(this).is(":checked")) {
        $('#erpatient-id').attr('required',true);      
        $('#erpatient-id_nt').hide();
//        $('span[aria-labelledby="select2-erpatient-id_nt-container"]').hide();
        $('[for="erpatient-id_nt"]').hide();
   }

//ON LOAD ADD REQUIRED RULES FOR ESCORT
        if(!$('#checkIsAlone').is(":checked")) {
            $('#checkin-escort').attr('required',true);                        
        }   
        
     $('#checkForeign').change(function() {
        if($(this).is(":checked")) {
        //IS FOREIGN
        //SHOW NATIONALITY DEFAULT NONE, HIDE ID AND NO REQUIRED ID
            $("#erpatient-id_nt").val('');
            $('#erpatient-id_nt').show();
//        $('span[aria-labelledby="select2-erpatient-id_nt-container"]').show();
            $('[for="erpatient-id_nt"]').show();
            $("#erpatient-id").val('');
        
        
            $('#erpatient-id').hide();
            $('#erpatient-id').attr('required',false);   
            
            //HIDE LABEL, FIND BY CUSTOM ATTRIBUTE
            $('[for="erpatient-id"]').hide();           
        }else{
        //IS ECUADORIAN
        //HIDE NATIONALITY, DEFAULT ECUADOR AND MAKE REQUIRED ID
            $('#erpatient-id_nt').hide();  
            $('[for="erpatient-id_nt"]').hide();
            $("#erpatient-id_nt").val(34);
        
            $('#erpatient-id').show();
            $('#erpatient-id').attr('required',true);                    
            
            $('[for="erpatient-id"]').show();        
        }
     });
        
$('#checkIsAlone').change(function() {
        if($(this).is(":checked")) {
            $('#checkin-escort').hide();
            $('[for="checkin-escort"]').hide();
                //NO REQUIRED RULES
                $('#checkin-escort').attr('required',false);                         
        
        }else{
        $('#checkin-escort').show();
        $('[for="checkin-escort"]').show();
        //REQUIRED RULES
                $('#checkin-escort').attr('required',true);          
            }
  });
                
        
        
//PARA CARGAR DATOS SEGUN ID SI EXISTE
$('#erpatient-id').change(function(){
   var idCode = $(this).val();
    
   $.get('../er-patient/get-er-patient-by-id',{id : idCode},function(data){
        if(data!='null'){//EXISTE REGISTRO
        var data = $.parseJSON(data);
        $('#auxId').attr('value',data.ID_ERP);
        $('#erpatient-first_name').attr('value',data.FIRST_NAME);
        $('#erpatient-last_name').attr('value',data.LAST_NAME);
        $("#erpatient-id_nt").val(data.ID_NT);
        $("#erpatient-gender").val(data.GENDER);        
        $('#erpatient-birthday').attr('value',data.BIRTHDAY);
//        $("#erpatient-birthday-disp").kvDatepicker("setDate", "01/09/2015");
//        $("#erpatient-birthday-disp-kvdate").kvDatepicker("update", "DATE");
        $('#erpatient-birthday').attr('type', 'text');
        $('#erpatient-birthday-disp').hide();
        
        
        desactivarCampos(true);
        }else{
        $('#auxId').attr('value','');
        $('#erpatient-first_name').attr('value','');
        $('#erpatient-last_name').attr('value','');
        $('#erpatient-birthday').attr('value','');
//        $("#erpatient-id_nt").val('');
        $("#erpatient-gender").val('');
        $('#erpatient-birthday').attr('type', 'hidden');
        $('#erpatient-birthday-disp').show();
        desactivarCampos(false);
            }
    });
});
    
    //DESACTIVA EL BOTON AL HACER SUBMIT
//    $('form#{$model->formName()}').on('submit', function (e)
//    {
//	  $('#createButton').attr('disabled',true);
//    });
        
//PARA ENVIAR POR AJAX POST        
        
  $('form#{$model->formName()}').on('beforeSubmit', function (e)
    {//LOS \\ sirven para escapar el siguiente simbolo y que no tome como variable de php
        var \$form = $(this);
  $('#createButton').attr('disabled',true);
        $.post(
                \$form.attr('action'), //serialize Yii2 form
                \$form.serialize()
                )

                .done(function (result) {

                    if (result == 1)
                    {                                 
                          $.pjax.reload({container:'#checkInGridId', timeout: 2000});
                          $(document).find('#modalId').modal('hide');
//                        $(\$form).trigger("reset"); //Esto resetea este modal.reload({container: '#checkInGridId'});
                    } else {
                        $('#message').html(result);
                    }
                }).fail(function () {
            console.log('server error');
        });
        return false;
    });  
   
  
//FUNCION PARA SETEAR EN BLANCO DATOS PACIENTE (NO EVENTUALIDAD)
  
  
//FUNCION PARA DESHABILITAR/HABILITAR CAMPOS PACIENTE
function desactivarCampos(status){
  $('#erpatient-first_name').attr('disabled',status);
  $('#erpatient-last_name').attr('disabled',status);
  $("#erpatient-id_nt").attr('disabled',status);
  $("#erpatient-gender").attr('disabled',status);
  $('#erpatient-birthday').attr('readonly',status);//ESTE CAMPO VA EN READONLY PORQUE SI NO NO SE ENVIA POR POST
  //TODO LO QUE ESTA EN DISABLED NO SE ENVIA, LA FECHA SI NECESITO PARA CALCULAR LA EDAD
}  
  
//DESABILITAR EL BOTON DE GUARDADO UNA VEZ SE ENVIE LAS VALIDACIONES OK
//  $("#createButton").submit(function() {
//	$(this).disable();
//    });​
//  \$form.on('Submit', function () {
//    $(this).disable();
//    return false;
//});
  
   
JS;
$this->registerJs($script);
?>