<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CheckInSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="check-in-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_CI') ?>

    <?= $form->field($model, 'ID_ERP') ?>

    <?= $form->field($model, 'ID_USR') ?>

    <?= $form->field($model, 'ID_TRB') ?>

    <?= $form->field($model, 'DATE') ?>

    <?php // echo $form->field($model, 'TIME') ?>

    <?php // echo $form->field($model, 'ESCORT') ?>

    <?php // echo $form->field($model, 'TROUBLE') ?>

    <?php // echo $form->field($model, 'MODIFIED') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Resetear', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
