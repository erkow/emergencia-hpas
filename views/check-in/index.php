<?php 
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CheckInSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Registro';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-in-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>
        <?= Html::a('Crear '.$this->title, ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->
    
    <!--BOTON PARA MODAL-->
     <p>
        <?= Html::button('Crear '.$this->title.'', ['value'=>  Url::to('create'), 'class' => 'btn btn-success','id'=>'modalButtonCreate']) ?>
    </p>
    
    <?php 
    //ESTO ES PARA ABRIR UN DIALOG (MODAL) EN VEZ DE OTRA PAGINA
    //TOCA CAPTURAR EL EVENTO CLICK EN JS EN EL ARCHIVO APPAsset en la carpeta assets
    //MAS INFO DE ESTO Y MAS SE PUEDE VER EN PÁG de BOOTSTRAP
        Modal::begin([
           'header' =>'<h4>CREAR REGISTRO</h4>',
            'id'=>'modalId',
            'size'=>'modal-lg', //lg=large,sm=small 
            //ESTO HACE Q EL MODAL NO SE CIERRE POR CLICK, PERO SI POR ESC
//            'clientOptions' => ['backdrop' => 'static', 'keyboard' => TRUE]
        ]);
        echo "<div id='modalContent'></div>";
        Modal::end();
        //AL MOSTRAR EL MODAL CARGA CON MENU, PARA QUITAR ESTO TOCA IR AL CONTROLADOR DE LO QUE SE CARGA, ak actionCreate y donde dice
        //render cambiar por renderAjax

    ?>

    <?php Pjax::begin(['id'=>'checkInGridId']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'ID_CI',   
            [
              'attribute'  => 'ID_ERP',
                'value' => 'iDERP.Full_Name',
            ], 
//            'ID_USR',
//            'ID_TRB',
            'DATE',
             'TIME',
             'ESCORT',
             'TROUBLE',
             'MODIFIED',

            ['class' => 'yii\grid\ActionColumn','template'=>'{view}' ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
