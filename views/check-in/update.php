<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CheckIn */

$this->title = 'Actualizar Registro: ' . ' ' . $erPatient->Full_Name;
$this->params['breadcrumbs'][] = ['label' => 'Registro', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $erPatient->Full_Name, 'url' => ['view', 'id' => $model->ID_CI]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="check-in-update">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'erPatient'=>$erPatient,
    ]) ?>

</div>
