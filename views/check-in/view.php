<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CheckIn */

$this->title = $erPatient->Full_Name;
$this->params['breadcrumbs'][] = ['label' => 'Registros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-in-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->ID_CI], ['class' => 'btn btn-primary']) ?>
       <?= Html::a('Cancelar', ['/check-in/index'], ['class'=>'btn btn-warning']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'ID_CI',
            'iDERP.Full_Name',
//            'ID_USR',
//            'ID_TRB',
            'DATE',
            'TIME',
            'ESCORT',
            'TROUBLE',
            'MODIFIED',
        ],
    ]) ?>

</div>
