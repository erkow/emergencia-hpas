<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Destination */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="destination-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>
    
<!--Aqui comenté esto porque al crear no necesito que se pueda ingresar este campo -->
    <!--<?= $form->field($model, 'ID_DST')->textInput() ?>-->

    <?= $form->field($model, 'DESTINATION')->textInput(['maxlength' => true]) ?>

<!--Aqui comenté esto porque al crear no necesito que se pueda ingresar este campo -->    
    <!--<?= $form->field($model, 'STATUS')->dropDownList([ 'ACTIVE' => 'Activo', 'INACTIVE' => 'Inactivo', ], ['prompt' => '']) ?>-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

  $('form#{$model->formName()}').on('beforeSubmit', function (e)
    {
        //LOS \\ sirven para escapar el siguiente simbolo y que no tome como variable de php
        var \$form = $(this);
        $.post(
                \$form.attr('action'), //serialize Yii2 form
                \$form.serialize()
                )

                .done(function (result) {

                    if (result == 1)
                    {
                          $.pjax.reload({container:'#destinationGrid', timeout: 2000});
                          $(document).find('#modalIdD').modal('hide');
//                        $(\$form).trigger("reset"); //Esto resetea este modal
                    } else {
                        $('#message').html(result);
                    }
                }).fail(function () {
            console.log('server error');
        });

        return false;

    });
   
JS;
$this->registerJs($script);
?>
