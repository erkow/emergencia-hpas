<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Destination */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="destination-form">

    <?php $form = ActiveForm::begin(); ?>
    
<!--Aqui comenté esto porque al crear no necesito que se pueda ingresar este campo -->
    <!--<?= $form->field($model, 'ID_DST')->textInput() ?>-->

    <?= $form->field($model, 'DESTINATION')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'STATUS')->dropDownList([ 'ACTIVE' => 'Activo', 'INACTIVE' => 'Inactivo', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
