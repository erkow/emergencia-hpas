<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DestinationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Destinations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="destination-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Create Destination',['value'=>Url::to('destination/create'), 'class' => 'btn btn-success','id'=>'modalButtonCreateD']) ?>
    </p>
    
    <?php
    Modal::begin([
           'header' =>'<h4>Crear Destinos</h4>',
            'id'=>'modalIdD',
            'size'=>'modal-lg', //lg=large,sm=small
        ]);
        echo "<div id='modalContentD'></div>";
        Modal::end();
    ?>
<?php Pjax::begin(['id'=>'destinationGrid','timeout' => 2000]); ?>   
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'ID_DST',
            'DESTINATION',
            'STATUS',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?>    

</div>
