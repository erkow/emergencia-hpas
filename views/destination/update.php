<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Destination */

$this->title = 'Update Destination: ' . ' ' . $model->DESTINATION;
$this->params['breadcrumbs'][] = ['label' => 'Destinations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_DST, 'url' => ['view', 'id' => $model->ID_DST]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="destination-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>
