<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveField;
use dosamigos\datepicker\DatePicker;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-form">

 
            
    <?php $form = ActiveForm::begin(); ?>    
            
   <?= $form->field($model, 'ID')->textInput() ?>            

    <?= $form->field($model, 'FIRST_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'LAST_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'BIRTHDAY')->widget(DateControl::classname(), [
    'displayFormat' => 'yyyy/MM/dd',
    'autoWidget' => false,
    'widgetClass' => 'yii\widgets\MaskedInput',
    'options' => [
        'mask' => '9999/99/99'
    ],
]);
    ?>    
            
            
    <?php
    if (!$model->isNewRecord) {
        echo $form->field($model, 'STATUS')->dropDownList([ 'ACTIVE' => 'Activo', 'INACTIVE' => 'Inactivo', ], ['prompt' => '']) ;             
    }
    ?>  
    
    <?php 
      $authItems = ArrayHelper::map($authItems, 'name', 'name');
      //SOBREESCRIBO EL ANTERIOR ARREGLO PARA DEFINIR SOLO LOS SIGUIENTES PERFILES
      $authItems = array(
          'Registro-Admisión'=>' Admisión',
          'Perfil Triage Adultos'=>'Triage Adultos',
          'Perfil Triage Niños'=>' Triage Niños',
          );
      
    ?>
    <div class="row">
        <div class="col-lg-2">
            <?= $form->field($model, 'perfiles')->checkboxList($authItems, ['unselect' => NULL]); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>        
        <?= Html::a('Cancelar', ['/employee/index'], ['class'=>'btn btn-warning']) ?>
    </div>
    

    <?php ActiveForm::end(); ?>
            
      
    

</div>
