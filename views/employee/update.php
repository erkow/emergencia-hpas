<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */

$this->title = 'Actualizar Empleado: ' . ' ' . $model->LAST_NAME.' '.$model->FIRST_NAME;
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->LAST_NAME.' '.$model->FIRST_NAME, 'url' => ['view', 'id' => $model->ID_EMP]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="employee-update">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'authItems'=> $authItems,
    ]) ?>

</div>
