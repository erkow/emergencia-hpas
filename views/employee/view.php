<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */

$this->title = $model->LAST_NAME.' '.$model->FIRST_NAME;
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->ID_EMP], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Cancelar', ['/employee/index'], ['class'=>'btn btn-warning']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'ID_EMP',
            'ID',
            'FIRST_NAME',
            'LAST_NAME',
            'BIRTHDAY',            
            'DATE_MODIFIED',
            'STATUS',
        ],
    ]) ?>

</div>
