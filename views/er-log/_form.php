<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use app\models\ErPatient;
use yii\helpers\ArrayHelper;
use app\models\Nationality;
use kartik\datecontrol\DateControl;
use kartik\select2\Select2;
use yii\widgets\MaskedInput;
use kartik\time\TimePicker;
use kartik\form\ActiveForm;

$form = ActiveForm::begin(['formConfig' => ['labelSpan' => 3,
                'deviceSize' => ActiveForm::SIZE_SMALL],
            'options' => ['id' => $model->formName()],
            'enableAjaxValidation' => true,]);
/* @var $this yii\web\View */
/* @var $model app\models\ErLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="er-log-form">

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <i><b>Nota:</b> Recuerde que esta pantalla es para crear 008s que no pasaron por Triage.</i>
    <!--SEPARADOR INICIAL PACIENTE-->
<!--    <table width="100%">        
        <td style="width:1px; padding: 0 10px; white-space: nowrap;"><h4><b>Datos de Identificación del Paciente</b></h4></td>
        <td><hr style="height:1px;border:none;color:black;background-color:black;" ></td>        
    </table>
    <br>-->

    <br>
    <fieldset>     
        <legend>Datos Paciente</legend>
    </fieldset>    

    <div class="form-group">
        <label class="control-label">Es Extranjero?</label> <input type="checkbox" id="checkForeign" name="checkForeign" />        
    </div>
    <hr style="height:1px;border:none;color:black;background-color:black;">

    <!--  CREAR PACIENTE INICIO  -->
    <div class="form-group ">
        <div class="col-sm-2">
            <?= $form->field($erPatient, 'ID')->textInput() ?>
        </div>

        <input id="auxId" name="auxId" val="" hidden="true"/> 

        <div class="col-sm-5">
            <?= $form->field($erPatient, 'FIRST_NAME')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-5">
            <?= $form->field($erPatient, 'LAST_NAME')->textInput(['maxlength' => true]) ?>   
        </div>
    </div>    



    <?php
    $erPatient->ID_NT = 34; //DEFAULT ECUATORIANA
    ?>

    <div class="form-group">       

        <div class="col-sm-5">
            <?= $form->field($erPatient, 'GENDER')->dropDownList([ 'MALE' => 'Masculino', 'FEMALE' => 'Femenino',], ['prompt' => '-Seleccione-']) ?>
        </div>

        <div class="col-sm-4">
            <?=
            $form->field($erPatient, 'BIRTHDAY')->widget(DateControl::classname(), [
                'displayFormat' => 'yyyy/MM/dd',
                'autoWidget' => false,
                'widgetClass' => 'yii\widgets\MaskedInput',
                'options' => [
                    'mask' => '9999/99/99'
                ],
            ]);
            ?>
        </div>
         <div class="col-sm-3">
            <?=
            $form->field($erPatient, 'ID_NT')->dropDownList(
                    ArrayHelper::map(Nationality::find()->all(), 'ID_NT', 'NATIONALITY'), ['prompt' => '-Seleccione-']
            )
            ?>
        </div>
    </div>            
    <!--  FIN PACIENTE -->
    <div class="form-group">
        <div class="col-sm-12"></div>
    </div>

    <div class="form-group">
        <div class="col-sm-3">
            <?= $form->field($model, 'IESS_AFFILIATE')->checkbox([ 'SI' => 'SI', 'NO' => 'NO',], ['prompt' => '']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'ISSFA_AFFILIATE')->checkbox([ 'SI' => 'SI', 'NO' => 'NO',], ['prompt' => '']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'ISSPOL_AFFILIATE')->checkbox([ 'SI' => 'SI', 'NO' => 'NO',], ['prompt' => '']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'SOAT')->checkbox([ 'SI' => 'SI', 'NO' => 'NO',], ['prompt' => '']) ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <?= $form->field($model, 'MARITAL_STATUS')->dropDownList([ 'SOLTER@' => 'SOLTER@', 'CASADO@' => 'CASADO@', 'DIVORCIAD@' => 'DIVORCIAD@', 'UNION LIBRE' => 'UNION LIBRE', 'VIUD@' => 'VIUD@',], ['prompt' => '-Seleccione-']) ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-8">
            <?= $form->field($model, 'ADDRESS')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?=
            $form->field($model, 'PHONE')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '999-999-999',
            ])
            ?>
        </div>
    </div>



    <!--SEPARADOR CONTACTO ACOMPAÑANTE-->
<!--    <table width="100%">        
        <td style="width:1px; padding: 0 10px; white-space: nowrap;"><h4><b>Datos de Contacto y Acompañante</b></h4></td>
        <td><hr style="height:1px;border:none;color:black;background-color:black;" ></td>        
    </table>
    <br>-->


    <fieldset> 
        <legend>Contacto</legend>
        <div class="form-group">
            <div class="col-sm-4">
                <?= $form->field($model, 'NOTIFIY_CONTACT')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'RELATIONSHIP_CONTACT')->dropDownList([ 'ABUEL@' => 'ABUEL@', 'AMIG@' => 'AMIG@', 'COMADRE' => 'COMADRE', 'COMPADRE' => 'COMPADRE', 'CONOCID@' => 'CONOCID@', 'CONVIVIENTE' => 'CONVIVIENTE', 'CUÑAD@' => 'CUÑAD@', 'ESPOS@' => 'ESPOS@', 'HERMAN@' => 'HERMAN@', 'HIJ@' => 'HIJ@', 'MADRASTRA' => 'MADRASTRA', 'MADRE' => 'MADRE', 'NIET@' => 'NIET@', 'NOVI@' => 'NOVI@', 'PADRASTRO' => 'PADRASTRO', 'PADRE' => 'PADRE', 'PARIENTE' => 'PARIENTE', 'PRIM@' => 'PRIM@', 'SOBRIN@' => 'SOBRIN@', 'TI@' => 'TI@', 'VECIN@' => 'VECIN@',], ['prompt' => '-Seleccione-']) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'ADDRESS_CONTACT')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </fieldset>


    <label >Viene sólo?</label> <input type="checkbox" id="checkIsAlone" name="checkIsAlone" />     
    <fieldset id="escort_group"> 
        <legend>Acompañante</legend>    
        <div class="form-group">
            <div class="col-sm-4">
                <?= $form->field($model, 'ESCORT')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'ESCORT_ID')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'ESCORT_ADDRESS')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?=
                $form->field($model, 'ESCORT_PHONE')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '999-999-999',
                ])
                ?>           
            </div>
        </div>
    </fieldset>   

    <!--SEPARADOR DATOS EMERGENCIA-->
    <table width="100%">        
        <td style="width:1px; padding: 0 10px; white-space: nowrap;"><h4><b>Datos de la Emergencia</b></h4></td>
        <td><hr style="height:1px;border:none;color:black;background-color:black;" ></td>        
    </table>
    <br>

    <div class="form-group">
        <div class="col-sm-5">
            <?= $form->field($model, 'TROUBLE')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'TRANSPORTATION_METHOD')->dropDownList([ 'AMBULANCIA' => 'AMBULANCIA', 'BICICLETA' => 'BICICLETA', 'BUS' => 'BUS', 'CAMINANDO' => 'CAMINANDO', 'CAMION' => 'CAMION', 'CAMIONETA' => 'CAMIONETA', 'FURGON' => 'FURGON', 'MOTO' => 'MOTO', 'OTRO' => 'OTRO', 'PARTICULAR' => 'PARTICULAR', 'PATRULLERO' => 'PATRULLERO', 'TAXI' => 'TAXI',], ['prompt' => '-Seleccione-']) ?>
        </div>

        <?php /* DEFAULT */ $model->TROUBLE_DATE = date('Y-m-d'); ?>
        <div class="col-sm-2">
            <?=
            $form->field($model, 'TROUBLE_DATE')->widget(DateControl::classname(), [
                'displayFormat' => 'yyyy/MM/dd',
                'autoWidget' => false,
                'widgetClass' => 'yii\widgets\MaskedInput',
                'options' => [
                    'mask' => '9999/99/99'
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-2">
            <?=
            $form->field($model, 'TROUBLE_TIME')->widget(TimePicker::classname(), ['options' => [
//        'readonly' => true,
                ],
                'pluginOptions' => [
                    'minuteStep' => 5,
                    'defaultTime' => false,
                    'showInputs' => true,
                ],
            ])
            ?>
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-2">
            <?= $form->field($model, 'TROUBLE_PLACE')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'TROUBLE_ADDRESS')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'EMERGENCY_TYPE')->dropDownList([ 'ACCIDENTE' => 'ACCIDENTE', 'ACCIDENTE LABORAL' => 'ACCIDENTE LABORAL', 'ACCIDENTE DE TRANSITO' => 'ACCIDENTE DE TRANSITO', 'ACCIDENTE DOMESTICO' => 'ACCIDENTE DOMESTICO', 'ENVENENAMIENTO' => 'ENVENENAMIENTO', 'VIOLENCIA' => 'VIOLENCIA', 'VIOLACION' => 'VIOLACION', 'GINECO-OBSTETRICO' => 'GINECO-OBSTETRICO', 'EPIDEMIA' => 'EPIDEMIA', 'INFECTOCONTAGIOSO' => 'INFECTOCONTAGIOSO', 'INTOXICACION' => 'INTOXICACION', 'QUEMADURA' => 'QUEMADURA', 'OTRO' => 'OTRO', 'CERTIFICADO MEDICO' => 'CERTIFICADO MEDICO',], ['prompt' => '-Seleccione-']) ?>
        </div>

        <?php /* DEFAULT VIVO */ $model->VITAL_STATUS = 'VIVO'; ?>
        <div class="col-sm-2">
            <?= $form->field($model, 'VITAL_STATUS')->dropDownList([ 'COMA' => 'COMA', 'ESTUPOR' => 'ESTUPOR', 'MUERTO' => 'MUERTO', 'PARO CARDIACO' => 'PARO CARDIACO', 'SHOCK' => 'SHOCK', 'VIVO' => 'VIVO',], ['prompt' => '-Seleccione-']) ?>
        </div>

        <?php /* DEFAULT AUTOREFERENCIA */ $model->REFERENCE = 'AUTOREFERENCIA'; ?>
        <div class="col-sm-2">
            <?= $form->field($model, 'REFERENCE')->dropDownList([ 'AUTOREFERENCIA' => 'AUTOREFERENCIA', 'REFERENCIA' => 'REFERENCIA', 'CONTRAREFERENCIA' => 'CONTRAREFERENCIA', 'REFERENCIA INVERSA' => 'REFERENCIA INVERSA',], ['prompt' => '']) ?>
        </div>
    </div>            

    <div class="form-group">
        <div class="col-sm-12">
            <?= $form->field($model, 'OBSERVATION_ADMISION')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <!--<?= $form->field($model, 'SECONDS_ELAPSED_ATTENTION')->textInput() ?>-->

    <!--<?= $form->field($model, 'STATUS_ATTENTION')->dropDownList([ 'ACTIVO' => 'ACTIVO', 'INACTIVO' => 'INACTIVO',], ['prompt' => '']) ?>-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => $model->isNewRecord ? 'createButton' : 'updateButton']) ?>
        <?= Html::button('Cancelar', ['class' => 'btn btn-warning', 'id' => 'btnCancel']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>



<?php
$script = <<< JS
        
//CHECKS ON LOAD STATUS OF FOREIGN CHECKED
    if(!$('#checkForeign').is(":checked")) {
        $('#erpatient-id').attr('required',true);      
        $('#erpatient-id_nt').hide();
//        $('span[aria-labelledby="select2-erpatient-id_nt-container"]').hide();
        $('[for="erpatient-id_nt"]').hide();
   }
        
//ON LOAD ADD REQUIRED RULES FOR ESCORT
        if(!$('#checkIsAlone').is(":checked")) {
            $('#erlog-escort').attr('required',true);                    
            $('#erlog-escort_phone-disp').attr('required',true);
   }
        
     $('#checkForeign').change(function() {
        if($(this).is(":checked")) {
        //IS FOREIGN
        //SHOW NATIONALITY DEFAULT NONE, HIDE ID AND NO REQUIRED ID
            $("#erpatient-id_nt").val('');
            $('#erpatient-id_nt').show();
//        $('span[aria-labelledby="select2-erpatient-id_nt-container"]').show();
            $('[for="erpatient-id_nt"]').show();
            $("#erpatient-id").val('');
        
        
            $('#erpatient-id').hide();
            $('#erpatient-id').attr('required',false);   
            
            //HIDE LABEL, FIND BY CUSTOM ATTRIBUTE
            $('[for="erpatient-id"]').hide();           
        }else{
        //IS ECUADORIAN
        //HIDE NATIONALITY, DEFAULT ECUADOR AND MAKE REQUIRED ID
            $('#erpatient-id_nt').hide();  
            $('[for="erpatient-id_nt"]').hide();
            $("#erpatient-id_nt").val(34);
        
            $('#erpatient-id').show();
            $('#erpatient-id').attr('required',true);                    
            
            $('[for="erpatient-id"]').show();        
        }
     });
        
$('#checkIsAlone').change(function() {
        if($(this).is(":checked")) {
            $('#escort_group').hide();
                //NO REQUIRED RULES
                $('#erlog-escort').attr('required',false);                    
                $('#erlog-escort_phone-disp').attr('required',false);                    
        
        }else{
        $('#escort_group').show();
        //REQUIRED RULES
                $('#erlog-escort').attr('required',true);                    
                $('#erlog-escort_phone-disp').attr('required',true);
            }
  });
        
        
        
//PARA CARGAR DATOS SEGUN ID SI EXISTE
$('#erpatient-id').change(function(){
   var idCode = $(this).val();
//IF ITS LOADING 3 TIMES, ITS BECAUSE THE RENDER HAS TO BE AJAX IN THE ACTION
   $.get('../er-patient/get-er-patient-by-id',{id : idCode},function(data){
        if(data!='null'){//EXISTE REGISTRO
        var data = $.parseJSON(data);
        $('#auxId').attr('value',data.ID_ERP);
        $('#erpatient-first_name').attr('value',data.FIRST_NAME);
        $('#erpatient-last_name').attr('value',data.LAST_NAME);
        $("#erpatient-id_nt").val(data.ID_NT);
        $("#erpatient-gender").val(data.GENDER);        
        $('#erpatient-birthday').attr('value',data.BIRTHDAY);
//        $("#erpatient-birthday-disp").kvDatepicker("setDate", "01/09/2015");
//        $("#erpatient-birthday-disp-kvdate").kvDatepicker("update", "DATE");
        $('#erpatient-birthday').attr('type', 'text');
        $('#erpatient-birthday-disp').hide();
        
        
        desactivarCampos(true);
        }else{
        $('#auxId').attr('value','');
        $('#erpatient-first_name').attr('value','');
        $('#erpatient-last_name').attr('value','');
        $('#erpatient-birthday').attr('value','');
//        $("#erpatient-id_nt").val('');
//        $("#erpatient-id_nt").val(34);
        $("#erpatient-gender").val('');
        $('#erpatient-birthday').attr('type', 'hidden');
        $('#erpatient-birthday-disp').show();
        desactivarCampos(false);
            }
    });
});
    
        
//PARA ENVIAR POR AJAX POST        
        
  $('form#{$model->formName()}').on('beforeSubmit', function (e)
    {//LOS \\ sirven para escapar el siguiente simbolo y que no tome como variable de php
        var \$form = $(this);
    //DESACTIVA EL BOTON AL HACER SUBMIT  
  $('#createButton').attr('disabled',true);
        $.post(
                \$form.attr('action'), //serialize Yii2 form
                \$form.serialize()
                )

                .done(function (result) {

                    if (result == 1)
                    {                                 
                          $.pjax.reload({container:'#erLogERGridId', timeout: 2000});
                          $(document).find('#modalId').modal('hide');
//                        $(\$form).trigger("reset"); //Esto resetea este modal.reload({container: '#erLogERGridId'});
                    } else {
                        $('#message').html(result);
                    }
                }).fail(function () {
            console.log('server error');
        });
        return false;
    });  
   
 
  
//FUNCION PARA DESHABILITAR/HABILITAR CAMPOS PACIENTE
function desactivarCampos(status){
  $('#erpatient-first_name').attr('disabled',status);
  $('#erpatient-last_name').attr('disabled',status);
  $("#erpatient-id_nt").attr('disabled',status);
  $("#erpatient-gender").attr('disabled',status);
  $('#erpatient-birthday').attr('readonly',status);
}  
  

//ACCION BOTON CANCELAR , cierra modal y recarga grid
$("#btnCancel").click(function(e){
    $.pjax.reload({container:'#erLogGridCreatedId', async:false});
  $.pjax.reload({container:'#erLogTriagePendingId', async:false});
    $(document).find('#modalId').modal('hide');
});

 
   
JS;
$this->registerJs($script);
?>