<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use app\models\ErPatient;
use yii\helpers\ArrayHelper;
use app\models\Nationality;
use kartik\datecontrol\DateControl;
use kartik\select2\Select2;
use yii\widgets\MaskedInput;
use kartik\time\TimePicker;
use kartik\form\ActiveForm;

$form = ActiveForm::begin(['formConfig' => ['labelSpan' => 3, 
    'deviceSize' => ActiveForm::SIZE_SMALL],
    'options' => ['id' => $model->formName()],
    'enableAjaxValidation' => true,]);
/* @var $this yii\web\View */
/* @var $model app\models\ErLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="er-log-form">

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>    

    <!--SEPARADOR INICIAL PACIENTE-->
<!--    <table width="100%">        
        <td style="width:1px; padding: 0 10px; white-space: nowrap;"><h4><b>Datos de Identificación del Paciente</b></h4></td>
        <td><hr style="height:1px;border:none;color:black;background-color:black;" ></td>        
    </table>
    <br>-->

    <br>
    <fieldset>     
        <legend>Datos Paciente</legend>
    </fieldset>    

    <hr style="height:1px;border:none;color:black;background-color:black;">

    <!--  CREAR PACIENTE INICIO  -->
    <div class="form-group ">
        <div class="col-sm-2">
            <?= $form->field($erPatient, 'ID')->textInput(['disabled' => true]) ?>
        </div>
        <input id="auxId" name="auxId" val="" hidden="true"/> 

        <div class="col-sm-5">
            <?= $form->field($erPatient, 'FIRST_NAME')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>
        <div class="col-sm-5">
            <?= $form->field($erPatient, 'LAST_NAME')->textInput(['maxlength' => true, 'disabled' => true]) ?>   
        </div>
    </div>

    <div class="form-group">  
        <div class="col-sm-4">
            <?=
            $form->field($erPatient, 'ID_NT')->dropDownList(
                    ArrayHelper::map(Nationality::find()->all(), 'ID_NT', 'NATIONALITY'), ['prompt' => '-Seleccione-', 'disabled' => true]
            )
            ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($erPatient, 'GENDER')->dropDownList([ 'MALE' => 'Masculino', 'FEMALE' => 'Femenino'], ['prompt' => '-Seleccione-', 'disabled' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($erPatient, 'BIRTHDAY')->textInput(['readonly' => true]) ?>
        </div>  
    </div>    
    <!--  FIN PACIENTE -->


    <div class="form-group">         

        <div class="col-sm-3">
            <?= $form->field($model, 'IESS_AFFILIATE')->checkbox([ 'SI' => 'SI', 'NO' => 'NO',], ['prompt' => '']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'ISSFA_AFFILIATE')->checkbox([ 'SI' => 'SI', 'NO' => 'NO',], ['prompt' => '']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'ISSPOL_AFFILIATE')->checkbox([ 'SI' => 'SI', 'NO' => 'NO',], ['prompt' => '']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'SOAT')->checkbox([ 'SI' => 'SI', 'NO' => 'NO',], ['prompt' => '']) ?>
        </div>
    </div>  

    <div class="form-group">  
        <div class="col-sm-12">
            <?= $form->field($model, 'MARITAL_STATUS')->dropDownList([ 'SOLTER@' => 'SOLTER@', 'CASADO@' => 'CASADO@', 'DIVORCIAD@' => 'DIVORCIAD@', 'UNION LIBRE' => 'UNION LIBRE', 'VIUD@' => 'VIUD@',], ['prompt' => '-Seleccione-']) ?>
        </div>
    </div>
    <!--<?= $form->field($model, 'AGE')->textInput() ?>-->


    <div class="form-group">   
        <div class="col-sm-8">
            <?= $form->field($model, 'ADDRESS')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?=
            $form->field($model, 'PHONE')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '999-999-999',
            ])
            ?>
        </div>
    </div>    

    <!--SEPARADOR CONTACTO ACOMPAÑANTE-->
<!--    <table width="100%">        
        <td style="width:1px; padding: 0 10px; white-space: nowrap;"><h4><b>Datos de Contacto y Acompañante</b></h4></td>
        <td><hr style="height:1px;border:none;color:black;background-color:black;" ></td>        
    </table>
    <br>-->


    <fieldset> 
        <legend>Contacto</legend>

        <div class="form-group">   
            <div class="col-sm-4">
                <?= $form->field($model, 'NOTIFIY_CONTACT')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'RELATIONSHIP_CONTACT')->dropDownList([ 'ABUEL@' => 'ABUEL@', 'AMIG@' => 'AMIG@', 'COMADRE' => 'COMADRE', 'COMPADRE' => 'COMPADRE', 'CONOCID@' => 'CONOCID@', 'CONVIVIENTE' => 'CONVIVIENTE', 'CUÑAD@' => 'CUÑAD@', 'ESPOS@' => 'ESPOS@', 'HERMAN@' => 'HERMAN@', 'HIJ@' => 'HIJ@', 'MADRASTRA' => 'MADRASTRA', 'MADRE' => 'MADRE', 'NIET@' => 'NIET@', 'NOVI@' => 'NOVI@', 'PADRASTRO' => 'PADRASTRO', 'PADRE' => 'PADRE', 'PARIENTE' => 'PARIENTE', 'PRIM@' => 'PRIM@', 'SOBRIN@' => 'SOBRIN@', 'TI@' => 'TI@', 'VECIN@' => 'VECIN@',], ['prompt' => '-Seleccione-']) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'ADDRESS_CONTACT')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </fieldset>


    <fieldset id="escort_group"> 
        <legend>Acompañante</legend>    
        <div class="form-group">
            <div class="col-sm-4">
                <?= $form->field($checkIn, 'ESCORT')->textInput(['maxlength' => true, 'readonly' => true]) ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'ESCORT_ID')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'ESCORT_ADDRESS')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-2">
                <?=
                $form->field($model, 'ESCORT_PHONE')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '999-999-999',
                ])
                ?>           
            </div>
        </div>
    </fieldset>    

    <!--SEPARADOR DATOS EMERGENCIA-->
    <table width="100%">        
        <td style="width:1px; padding: 0 10px; white-space: nowrap;"><h4><b>Datos de la Emergencia</b></h4></td>
        <td><hr style="height:1px;border:none;color:black;background-color:black;" ></td>        
    </table>
    <br>
    <div class="form-group">
        <div class="col-sm-4">
            <?= $form->field($model, 'TROUBLE')->textInput(['maxlength' => true, 'readonly' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'TRANSPORTATION_METHOD')->dropDownList([ 'AMBULANCIA' => 'AMBULANCIA', 'BICICLETA' => 'BICICLETA', 'BUS' => 'BUS', 'CAMINANDO' => 'CAMINANDO', 'CAMION' => 'CAMION', 'CAMIONETA' => 'CAMIONETA', 'FURGON' => 'FURGON', 'MOTO' => 'MOTO', 'OTRO' => 'OTRO', 'PARTICULAR' => 'PARTICULAR', 'PATRULLERO' => 'PATRULLERO', 'TAXI' => 'TAXI',], ['prompt' => '-Seleccione-']) ?>
        </div>

        <?php /* DEFAULT */ $model->TROUBLE_DATE = date('Y-m-d'); ?>
        <div class="col-sm-2">
            <?=
            $form->field($model, 'TROUBLE_DATE')->widget(DateControl::classname(), [
                'displayFormat' => 'yyyy/MM/dd',
                'autoWidget' => false,
                'widgetClass' => 'yii\widgets\MaskedInput',
                'options' => [
                    'mask' => '9999/99/99'
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <?=
            $form->field($model, 'TROUBLE_TIME')->widget(TimePicker::classname(), ['options' => [
//        'readonly' => true,
                ],
                'pluginOptions' => [
                    'minuteStep' => 5,
                    'defaultTime' => false,
                    'showInputs' => true,
                ],
            ])
            ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-2">
            <?= $form->field($model, 'TROUBLE_PLACE')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'TROUBLE_ADDRESS')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'EMERGENCY_TYPE')->dropDownList([ 'ACCIDENTE' => 'ACCIDENTE', 'ACCIDENTE LABORAL' => 'ACCIDENTE LABORAL', 'ACCIDENTE DE TRANSITO' => 'ACCIDENTE DE TRANSITO', 'ACCIDENTE DOMESTICO' => 'ACCIDENTE DOMESTICO', 'ENVENENAMIENTO' => 'ENVENENAMIENTO', 'VIOLENCIA' => 'VIOLENCIA', 'VIOLACION' => 'VIOLACION', 'GINECO-OBSTETRICO' => 'GINECO-OBSTETRICO', 'EPIDEMIA' => 'EPIDEMIA', 'INFECTOCONTAGIOSO' => 'INFECTOCONTAGIOSO', 'INTOXICACION' => 'INTOXICACION', 'QUEMADURA' => 'QUEMADURA', 'OTRO' => 'OTRO', 'CERTIFICADO MEDICO' => 'CERTIFICADO MEDICO',], ['prompt' => '-Seleccione-']) ?>
        </div>
        <?php /* DEFAULT VIVO */ $model->VITAL_STATUS = 'VIVO'; ?>
        <div class="col-sm-2">
            <?= $form->field($model, 'VITAL_STATUS')->dropDownList([ 'COMA' => 'COMA', 'ESTUPOR' => 'ESTUPOR', 'MUERTO' => 'MUERTO', 'PARO CARDIACO' => 'PARO CARDIACO', 'SHOCK' => 'SHOCK', 'VIVO' => 'VIVO',], ['prompt' => '-Seleccione-']) ?>
        </div>
        <?php /* DEFAULT AUTOREFERENCIA */ $model->REFERENCE = 'AUTOREFERENCIA'; ?>
        <div class="col-sm-2">
            <?= $form->field($model, 'REFERENCE')->dropDownList([ 'AUTOREFERENCIA' => 'AUTOREFERENCIA', 'REFERENCIA' => 'REFERENCIA', 'CONTRAREFERENCIA' => 'CONTRAREFERENCIA', 'REFERENCIA INVERSA' => 'REFERENCIA INVERSA',], ['prompt' => '']) ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <?= $form->field($model, 'OBSERVATION_ADMISION')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <!--<?= $form->field($model, 'SECONDS_ELAPSED_ATTENTION')->textInput() ?>-->

    <!--<?= $form->field($model, 'STATUS_ATTENTION')->dropDownList([ 'ACTIVO' => 'ACTIVO', 'INACTIVO' => 'INACTIVO',], ['prompt' => '']) ?>-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id' => $model->isNewRecord ? 'createButton' : 'updateButton']) ?>
        <?= Html::a('Cancelar', ['/er-log/index'], ['class'=>'btn btn-warning']) ?>
    </div>
  
    <?php ActiveForm::end(); ?>

</div>



<?php
$script = <<< JS
        
//ON LOAD ADD HIDE ESCORT FIELDS IF ESCORT IS NULL
        if($('#checkin-escort').val()=='') {
            $('#escort_group').hide();
   }
        
               
//PARA ENVIAR POR AJAX POST        
        
  $('form#{$model->formName()}').on('beforeSubmit', function (e)
    {//LOS \\ sirven para escapar el siguiente simbolo y que no tome como variable de php
        var \$form = $(this);
    //DESACTIVA EL BOTON AL HACER SUBMIT  
  $('#createButton').attr('disabled',true);
        $.post(
                \$form.attr('action'), //serialize Yii2 form
                \$form.serialize()
                )

                .done(function (result) {

                    if (result == 1)
                    {                                 
                          $.pjax.reload({container:'#erLogERGridId', timeout: 2000});
                          $(document).find('#modalId').modal('hide');
//                        $(\$form).trigger("reset"); //Esto resetea este modal.reload({container: '#erLogERGridId'});
                    } else {
                        $('#message').html(result);
                    }
                }).fail(function () {
            console.log('server error');
        });
        return false;
    });  
   
 
  
//FUNCION PARA DESHABILITAR/HABILITAR CAMPOS PACIENTE
function desactivarCampos(status){
  $('#erpatient-first_name').attr('disabled',status);
  $('#erpatient-last_name').attr('disabled',status);
  $("#erpatient-id_nt").attr('disabled',status);
  $("#erpatient-gender").attr('disabled',status);
  $('#erpatient-birthday').attr('readonly',status);
}   

   
JS;
$this->registerJs($script);
?>