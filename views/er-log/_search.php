<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ErLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="er-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_ERL') ?>

    <?= $form->field($model, 'ID_ERP') ?>

    <?= $form->field($model, 'CODE_008') ?>

    <?= $form->field($model, 'ID_TRG') ?>

    <?= $form->field($model, 'ID_USR_ADMISION') ?>

    <?php // echo $form->field($model, 'MARITAL_STATUS') ?>

    <?php // echo $form->field($model, 'AGE') ?>

    <?php // echo $form->field($model, 'IESS_AFFILIATE') ?>

    <?php // echo $form->field($model, 'ISSFA_AFFILIATE') ?>

    <?php // echo $form->field($model, 'ISSPOL_AFFILIATE') ?>

    <?php // echo $form->field($model, 'SOAT') ?>

    <?php // echo $form->field($model, 'ADDRESS') ?>

    <?php // echo $form->field($model, 'PHONE') ?>

    <?php // echo $form->field($model, 'NOTIFIY_CONTACT') ?>

    <?php // echo $form->field($model, 'RELATIONSHIP_CONTACT') ?>

    <?php // echo $form->field($model, 'ADDRESS_CONTACT') ?>

    <?php // echo $form->field($model, 'ESCORT') ?>

    <?php // echo $form->field($model, 'ESCORT_ID') ?>

    <?php // echo $form->field($model, 'ESCORT_ADDRESS') ?>

    <?php // echo $form->field($model, 'ESCORT_PHONE') ?>

    <?php // echo $form->field($model, 'TRANSPORTATION_METHOD') ?>

    <?php // echo $form->field($model, 'TROUBLE') ?>

    <?php // echo $form->field($model, 'TROUBLE_DATE') ?>

    <?php // echo $form->field($model, 'TROUBLE_TIME') ?>

    <?php // echo $form->field($model, 'TROUBLE_PLACE') ?>

    <?php // echo $form->field($model, 'TROUBLE_ADDRESS') ?>

    <?php // echo $form->field($model, 'EMERGENCY_TYPE') ?>

    <?php // echo $form->field($model, 'VITAL_STATUS') ?>

    <?php // echo $form->field($model, 'REFERENCE') ?>

    <?php // echo $form->field($model, 'OBSERVATION_ADMISION') ?>

    <?php // echo $form->field($model, 'SECONDS_ELAPSED_ATTENTION') ?>

    <?php // echo $form->field($model, 'STATUS_ATTENTION') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Resetear', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
