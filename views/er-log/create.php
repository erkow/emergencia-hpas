<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ErLog */

$this->title = 'Crear 008 de Emergencia';
$this->params['breadcrumbs'][] = ['label' => '008s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="er-log-create">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'erPatient'=>$erPatient,
    ]) ?>

</div>
