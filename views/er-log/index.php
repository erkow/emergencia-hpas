<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ErLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '008 Ingreso';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="er-log-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Crear 008 de Emergencia', ['value'=>  Url::to('create'), 'class' => 'btn btn-success','id'=>'modalButtonCreate']) ?>
    </p>
    
    <?php 
    //MODAL    
        Modal::begin([
           'header' =>'<h4>CREAR 008 DE EMERGENCIA</h4>',
            'id'=>'modalId',
            'size'=>'modal-lg', //lg=large,sm=small 
            //ESTO HACE Q EL MODAL NO SE CIERRE POR CLICK, PERO SI POR ESC
//            'clientOptions' => ['backdrop' => 'static', 'keyboard' => TRUE]
        ]);
        echo "<div id='modalContent'></div>";
        Modal::end();
    ?>    
    
    <?php Pjax::begin(['id'=>'erLogTriagePendingId']); ?>
    <fieldset>     
        <legend>Triages Pendientes</legend>
    </fieldset>
    
    <?= GridView::widget([
        'dataProvider' => $triageDataProvider,
        'filterModel' => $triageSearchModel,
        'rowOptions' => function($model) {
            if ($model->STATUS == 'WAITING') {
                return ['class' => 'warning'];
            }
            if ($model->STATUS == 'ATTENDED') {
                return ['class' => 'success'];
            }
            if ($model->STATUS == 'ABSENT') {
                return ['class' => 'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],            

//            'ID_TRG',
//            'ID_CI',
            [
                'label'=>'Identificación',
                'attribute' => 'idErPatient',
                'value' => 'erPatient.ID',
            ],
            [//EN EL CASO DE TABLAS EN 3er RELACION PARA QUE SE PUEDA BUSCAR EL ATRIBUTE
            //DEBEMOS CREAR COMO VARIABLE EN SEARCHMODEL 
                'attribute'  => 'paciente',
                'value' => 'erPatient.Full_Name',
            ],
            [//PARA QUE SE PUEDA BUSCAR EL ATRIBUTE DEBE SER EL PARAMETRO QUE ESTA EN EL FORM, Q ESTE EN RULES
              'attribute'  => 'ID_CI',
                'value' => 'iDCI.TROUBLE',
            ],
//            [
//              'label'  => 'Edad',
//                'attribute' =>'ageErPatient',
//                'value' => 'iDCI.AGE',
//            ],     

//            'ID_USR',            
            [
              'attribute'  => 'ID_PRT',
                'value' => 'iDPRT.PRIORITY',
            ], 
//            'DATE_IN',
//             'TIME_IN',
//            [
//                'label' => 'Fecha Triaje',
//                'attribute' => 'DATE_OUT',
//                'value' => 'DATE_OUT',
//            ],
            [
                'label' => 'Hora Triaje',
                'attribute' => 'TIME_OUT',
                'value' => 'TIME_OUT',
            ],                   
             
//             'SECONDS_ELAPSED',
            // 'OBSERVATION',
//             'STATUS',
//            [
//              'attribute'=>'STATUS',
//               'value'=>'statusLabel'
//            ],            
            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view-triage}{create-er-log}',
                    'buttons' => [
                        //view button
                        'view-triage' => function ($url, $model) {//SHOW EDIT ONLY WHEN ITS DOESNT HAVE PRIORITY
                            return Html::a('<span  class="glyphicon glyphicon-eye-open">&nbsp</span>', $url, [ 'title' => Yii::t('app', 'Ver'), ]);
                        },
                        'create-er-log' => function ($url, $model) {//ONLY SHOW THIS WITH STATUS ATTENDED                       
                            return  ($model->STATUS =='ATTENDED') ? Html::a('<span class="fa fa-save"></span>', $url, ['title' => Yii::t('yii', 'Crear 008'),]) : '';                        
                        },
                    ],
            ],
            
            
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    
    <hr style="height:1px;border:none;color:black;background-color:black;">
    <fieldset>     
        <legend>008's Creadas</legend>
    </fieldset>
    <?php Pjax::begin(['id'=>'erLogGridCreatedId']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'ID_ERL',
            'CODE_008',
            [
              'attribute'  => 'ID_ERP',
                'value' => 'iDERP.Full_Name',
            ], 
            
            'ID_TRG',           
            [
              'attribute'  => 'ID_USR_ADMISION',
                'value' => 'employee.EmpleadoNombreCompleto',
            ], 
            // 'MARITAL_STATUS',
            // 'AGE',
            // 'IESS_AFFILIATE',
            // 'ISSFA_AFFILIATE',
            // 'ISSPOL_AFFILIATE',
            // 'SOAT',
            // 'ADDRESS',
            // 'PHONE',
            // 'NOTIFIY_CONTACT',
            // 'RELATIONSHIP_CONTACT',
            // 'ADDRESS_CONTACT',
            // 'ESCORT',
            // 'ESCORT_ID',
            // 'ESCORT_ADDRESS',
            // 'ESCORT_PHONE',
            // 'TRANSPORTATION_METHOD',
            // 'TROUBLE',
            // 'TROUBLE_DATE',
            // 'TROUBLE_TIME',
            // 'TROUBLE_PLACE',
            // 'TROUBLE_ADDRESS',
            // 'EMERGENCY_TYPE',
            // 'VITAL_STATUS',
            // 'REFERENCE',
            // 'OBSERVATION_ADMISION',
            // 'SECONDS_ELAPSED_ATTENTION',
            // 'STATUS_ATTENTION',

            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{generate-pdf}',
                    'buttons' => [
                        //view button
                        'view' => function ($url, $model) {//SHOW VIEW
                            return Html::a('<span  class="glyphicon glyphicon-eye-open">&nbsp</span>', $url, [ 'title' => Yii::t('app', 'Ver'), ]);
                        },

                        'generate-pdf' => function ($url, $model) {//ONLY SHOW THIS BUTTON WITH PRIORITY AND ATTENDED                        
                            return  ($model->ID_ERL) ? Html::a('<span class="fa fa-print">&nbsp</span>', $url, ['title' => Yii::t('yii', 'Imprimir 008'),]) : '';
                        },
                    ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?php
$script = <<< JS

//PARA CARGAR DATOS SEGUN ID SI EXISTE
function refresh() {
     $.pjax.reload({container:'#erLogTriagePendingId', async:false});//TENGO Q USAR async:false para que valga la doble recarga de los dos 
     $.pjax.reload({container:'#erLogGridCreatedId', async:false});
        
     setTimeout(refresh, 40000); // refresh  the function every 40 seconds
 }
        
 refresh();  
           
JS;
$this->registerJs($script);
?>        