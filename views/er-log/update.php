<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ErLog */

$this->title = 'Actualizar 008: ' . ' ' . $model->ID_ERL;
$this->params['breadcrumbs'][] = ['label' => '008s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_ERL, 'url' => ['view', 'id' => $model->ID_ERL]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="er-log-update">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
