<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Nationality;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
use app\models\ErPatient;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\ErPatient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="er-patient-form">

    <?php $form = ActiveForm::begin(['enableAjaxValidation'=>true]); ?>

    <?= $form->field($model, 'ID')->textInput() ?>
  
    <?= $form->field($model, 'FIRST_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'LAST_NAME')->textInput(['maxlength' => true]) ?>   


    <?=
    $form->field($model, 'ID_NT')->dropDownList(
            ArrayHelper::map(Nationality::find()->all(), 'ID_NT', 'NATIONALITY'), ['prompt' => '-Seleccione-']
    )
    ?>

    <?= $form->field($model, 'GENDER')->dropDownList([ 'MALE' => 'Masculino', 'FEMALE' => 'Femenino',], ['prompt' => '-Seleccione-']) ?>


    <?=
    $form->field($model, 'BIRTHDAY')->widget(
            DatePicker::className(), [
        // inline too, not bad
        'inline' => false,
        // modify template for custom rendering
//        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
            'language' => 'es',
            'todayBtn' => "linked",                
        ]
    ]);
    ?>


    <?= $form->field($model, 'CLINIC_NUMBER')->textInput() ?>

    <?= $form->field($model, 'STATUS')->dropDownList([ 'ACTIVE' => 'Activo', 'INACTIVE' => 'Inactivo',], ['prompt' => '-Seleccione-']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
//here is all JS        
$('#erpatient-id').change(function(){
   var idCode = $(this).val();
    
   $.get('../er-patient/get-er-patient-by-id',{id : idCode},function(data){
        if(data!='null'){
        var data = $.parseJSON(data);
        $('#erpatient-first_name').attr('value',data.FIRST_NAME);
        $('#erpatient-last_name').attr('value',data.LAST_NAME);
        $("#erpatient-id_nt").val(data.ID_NT);
        $("#erpatient-gender").val(data.GENDER);
        $('#erpatient-birthday').attr('value',data.BIRTHDAY);
        }else{
         $('#erpatient-first_name').attr('value','');
        $('#erpatient-last_name').attr('value','');
        $('#erpatient-birthday').attr('value','');
        $("#erpatient-id_nt").val('');
        $("#erpatient-gender").val('');
            }
    });
});
   
JS;
$this->registerJs($script);
?>        