<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ErPatientSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="er-patient-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_ERP') ?>

    <?= $form->field($model, 'ID_NT') ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'FIRST_NAME') ?>

    <?= $form->field($model, 'LAST_NAME') ?>

    <?php // echo $form->field($model, 'GENDER') ?>

    <?php // echo $form->field($model, 'BIRTHDAY') ?>

    <?php // echo $form->field($model, 'CLINIC_NUMBER') ?>

    <?php // echo $form->field($model, 'CREATED') ?>

    <?php // echo $form->field($model, 'MODIFIED') ?>

    <?php // echo $form->field($model, 'STATUS') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
