<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ErPatient */

$this->title = 'Create Er Patient';
$this->params['breadcrumbs'][] = ['label' => 'Er Patients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="er-patient-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
