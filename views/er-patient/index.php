<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ErPatientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Er Patients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="er-patient-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Er Patient', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(['timeout' => 2000]); ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //ESTO PERMITE DAR FORMATO A CIERTAS COLUMNAS EN ESPECIFICO, EN ESTE CASO PINTA ROJO LAS QUE TENGAN ESTADO INACTIVE
        'rowOptions' => function($model) {
            if ($model->STATUS == 'INACTIVE') {
                return ['class' => 'danger'];
            }
        },
            'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'ID_ERP',            
            'ID',
            [
                'label'=>'Paciente',
                'attribute'=>'Full_Name',
            ],
            'FIRST_NAME',
            'LAST_NAME',
            //LO DE ABAJO ES PARA QUE LOS CAMPOS RELACIONADOS MUESTREN OTRA INFORMACION Y SEAN BUSCABLES, 
            //aparte toca hacer cambios en el ModelSearch,ahi toca quitar regla tipo INT y poner en SAFE y al final en el query
            //JOIN y like table, field
            //iDNT es el nombre de la funcion de relacion que está en el COntrolador, la primera letra debe ir en minuscula
            [
                'attribute' => 'ID_NT',
                'value' => 'iDNT.NATIONALITY',
            ],
            // 'GENDER',            
            [
                'attribute' => 'BIRTHDAY',
                'value' => 'BIRTHDAY',
                'format' => 'raw',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'BIRTHDAY',
//                    'template' => '{addon}{input}',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
            ],
            // 'CLINIC_NUMBER',
            // 'CREATED',
            // 'MODIFIED',
            // 'STATUS',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
<?php    Pjax::end(); ?>
</div>
