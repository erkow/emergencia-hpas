<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ErPatient */

$this->title = 'Update Er Patient: ' . ' ' . $model->ID_ERP;
$this->params['breadcrumbs'][] = ['label' => 'Er Patients', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_ERP, 'url' => ['view', 'id' => $model->ID_ERP]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="er-patient-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
