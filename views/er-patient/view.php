<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ErPatient */

$this->title = $model->ID_ERP;
$this->params['breadcrumbs'][] = ['label' => 'Er Patients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="er-patient-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID_ERP], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID_ERP], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_ERP',
            'ID_NT',
            'ID',
            'FIRST_NAME',
            'LAST_NAME',
            'GENDER',
            'BIRTHDAY',
            'CLINIC_NUMBER',
            'CREATED',
            'MODIFIED',
            'STATUS',
        ],
    ]) ?>

</div>
