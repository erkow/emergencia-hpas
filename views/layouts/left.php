<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/msp.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info" style="text-align: center">
                <p>Hospital </p>
                <p>Pablo Arturo Suarez</p>
            </div>
        </div>

        <!-- search form -->
<!--        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>-->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Login', 'url' => ['site/login'],'icon' => 'fa  fa-map', 'visible' => Yii::$app->user->isGuest],
                    ['label' => 'Menú', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Configuración',
                        'icon' => 'fa  fa-cog',                        
                        'url' => '#',
                        'visible' => Yii::$app->user->can('admin'),
                        'items' => [
                            ['label' => 'Destinos', 'icon' => 'fa  fa-arrow-circle-o-right', 'url' => ['destination/index']],
                            ['label' => 'Nacionalidad', 'icon' => 'fa  fa-plane', 'url' => ['nationality/index']],
                            ['label' => 'Prioridad', 'icon' => 'fa  fa-sort-amount-asc', 'url' => ['priority/index']],
                        ],
                    ],
                    ['label' => 'Empleados', 'icon' => 'fa  fa-users', 'url' => ['employee/index'],'visible' => Yii::$app->user->can('/employee/index'),],
                    ['label' => 'Pacientes', 'icon' => 'fa  fa-wheelchair', 'url' => ['er-patient/index'],'visible' => Yii::$app->user->can('/er-patient/index'),],                    
                    ['label' => 'Registro', 'icon' => 'fa  fa-ambulance', 'url' => ['check-in/index'],'visible' => Yii::$app->user->can('/check-in/index'),],
                    [
                    'label' => 'Triages',
                    'icon' => 'fa  fa-stethoscope',
                    'url' => ['triage/index'],
                    'visible' => Yii::$app->user->can('Perfil Triage Adultos') || Yii::$app->user->can('admin') || Yii::$app->user->can('Perfil Triage Niños'),
                    ],
                    ['label' => 'Apertura 008', 'icon' => 'fa fa-file-text-o', 'url' => ['er-log/index'],'visible' => Yii::$app->user->can('/er-log/index'),],
                    ['label' => 'Cierre 008', 'icon' => 'fa fa-folder-o', 'url' => ['outflow/index'],'visible' => Yii::$app->user->can('/outflow/index'),],
//                    ['label' => 'Triage Niños', 'icon' => 'fa fa-child', 'url' => ['triage/index'],'visible' => Yii::$app->user->can('Perfil Triage Niños'),],
//                    ['label' => 'Triage Adultos', 'icon' => 'fa fa-male', 'url' => ['triage/index'],'visible' => Yii::$app->user->can('Perfil Triage Adultos'),],
//                    ['label' => 'Triage', 'icon' => 'fa fa-file-code-o', 'url' => ['triage/index']],
                    ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],'visible' => Yii::$app->user->can('admin'),],
                    ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],'visible' => Yii::$app->user->can('admin'),],                    
                    [
                        'label' => 'Same tools',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'visible' => false,
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'fa fa-circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'fa fa-circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
