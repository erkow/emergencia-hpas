<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LoginLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Login Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-log-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Login Log', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'payment_holder',
            'user_name',
            'access_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
