<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Nationality */

$this->title = 'Update Nationality: ' . ' ' . $model->ID_NT;
$this->params['breadcrumbs'][] = ['label' => 'Nationalities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_NT, 'url' => ['view', 'id' => $model->ID_NT]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nationality-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
