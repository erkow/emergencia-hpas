<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Cie10;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Outflow */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="outflow-form">

    <?php $form = ActiveForm::begin(); ?>       

     <?php echo $form->field($model, 'ID_CIE10')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Cie10::find()->all(), 'ID_CIE10', 'DESCRIPCION'),         
        'language' => 'es',              
        'options' => ['placeholder' => '-Seleccione-'],        
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>    
    
    <?= $form->field($model, 'CONTUTION')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CAUSE_EFFECT')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DESTINATION')->dropDownList([ 'TRANSFERIDO/DERIVADO' => 'TRANSFERIDO/DERIVADO', 'HOSPITALIZACIÓN' => 'HOSPITALIZACIÓN', 'DOMICILIO' => 'DOMICILIO', 'CONSULTA EXTERNA' => 'CONSULTA EXTERNA', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'TRANSFERED')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SPECIALITY')->dropDownList([ 'CIRUGÍA GENERAL' => 'CIRUGÍA GENERAL', 'GINECOLOGÍA' => 'GINECOLOGÍA', 'GINECOOBSTETRICIA' => 'GINECOOBSTETRICIA', 'INFECTOLOGÍA' => 'INFECTOLOGÍA', 'MAXILOFACIAL' => 'MAXILOFACIAL', 'MEDICINA INTERNA' => 'MEDICINA INTERNA', 'NEFROLOGÍA' => 'NEFROLOGÍA', 'NEONATOLOGÍA' => 'NEONATOLOGÍA', 'NEUMOLOGÍA' => 'NEUMOLOGÍA', 'NEUROLOGÍA' => 'NEUROLOGÍA', 'ODONTOLOGÍA' => 'ODONTOLOGÍA', 'OFTALMOLOGÍA' => 'OFTALMOLOGÍA', 'OTORRINOLARINGOLOGÍA' => 'OTORRINOLARINGOLOGÍA', 'PEDIATRÍA' => 'PEDIATRÍA', 'PSICOLOGÍA' => 'PSICOLOGÍA', 'PSIQUIATRÍA' => 'PSIQUIATRÍA', 'TRAUMATOLOGÍA' => 'TRAUMATOLOGÍA', 'UCI' => 'UCI', 'UROLOGÍA' => 'UROLOGÍA', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'DOCTOR_OUTFLOW')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'OBSERVATION_ATTENTION')->textInput(['maxlength' => true]) ?>

    <!--<?= $form->field($model, 'STATUS_ATTENTION')->dropDownList([ 'ACTIVO' => 'ACTIVO', 'INACTIVO' => 'INACTIVO', ], ['prompt' => '']) ?>-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancelar', ['/outflow/index'], ['class'=>'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$script = <<< JS
        
//CHECKS ON LOAD DESTINATION VALUE
    if($('#outflow-destination').val()!='TRANSFERIDO/DERIVADO') {
        $('#outflow-transfered').attr('required',false);   
        //HIDE LABEL, FIND BY CUSTOM ATTRIBUTE
        $('[for="outflow-transfered"]').hide();     
        $('#outflow-transfered').hide();
   }
        
     $('#outflow-destination').change(function() {
        if($(this).val()!='TRANSFERIDO/DERIVADO') {
            $('#outflow-transfered').attr('required',false);   
            //HIDE LABEL, FIND BY CUSTOM ATTRIBUTE
            $('[for="outflow-transfered"]').hide();     
            $('#outflow-transfered').hide();         
        }else{
            $('#outflow-transfered').attr('required',true);   
            //HIDE LABEL, FIND BY CUSTOM ATTRIBUTE
            $('[for="outflow-transfered"]').show();     
            $('#outflow-transfered').show();     
        }
     });
        
$('#checkIsAlone').change(function() {
        if($(this).is(":checked")) {
            $('#escort_group').hide();
                //NO REQUIRED RULES
                $('#erlog-escort').attr('required',false);                    
                $('#erlog-escort_phone-disp').attr('required',false);                    
        
        }else{
        $('#escort_group').show();
        //REQUIRED RULES
                $('#erlog-escort').attr('required',true);                    
                $('#erlog-escort_phone-disp').attr('required',true);
            }
  });
        
 
   
JS;
$this->registerJs($script);
?>