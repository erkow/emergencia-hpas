<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OutflowSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="outflow-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_OF') ?>

    <?= $form->field($model, 'ID_ERL') ?>

    <?= $form->field($model, 'ID_USR_ADMISION') ?>

    <?= $form->field($model, 'CONTUTION') ?>

    <?= $form->field($model, 'CAUSE_EFFECT') ?>

    <?php // echo $form->field($model, 'DESTINATION') ?>

    <?php // echo $form->field($model, 'TRANSFERED') ?>

    <?php // echo $form->field($model, 'SPECIALITY') ?>

    <?php // echo $form->field($model, 'DOCTOR_OUTFLOW') ?>

    <?php // echo $form->field($model, 'DATE_OUTFLOW') ?>

    <?php // echo $form->field($model, 'TIME_OUTFLOW') ?>

    <?php // echo $form->field($model, 'OBSERVATION_ATTENTION') ?>

    <?php // echo $form->field($model, 'SECONDS_ELAPSED_ATTENTION') ?>

    <?php // echo $form->field($model, 'MODIFIED') ?>

    <?php // echo $form->field($model, 'STATUS_ATTENTION') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Resetear', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
