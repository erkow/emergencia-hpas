<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Outflow */

$this->title = 'Crear Cierre '.$erLog->CODE_008.' :'.$erLog->iDERP->Full_Name;
$this->params['breadcrumbs'][] = ['label' => 'Cierre 008', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="outflow-create">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'erLog'=>$erLog,
    ]) ?>

</div>
