<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OutflowSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cierre 008';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="outflow-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>
        <?= Html::a('Crear ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->

    
        <fieldset>     
        <legend>008s Pendientes por Cerrar</legend>
    </fieldset>
    
    
 
    <?php Pjax::begin(['id'=>'pending008GridId']); ?>
    <?= GridView::widget([
        'dataProvider' => $erlogDataProvider,
        'filterModel' => $erlogSearchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'ID_ERL',
            'CODE_008',
            [
              'attribute'  => 'ID_ERP',
                'value' => 'iDERP.Full_Name',
            ], 
            
//            'ID_TRG',           
            [
              'attribute'  => 'ID_USR_ADMISION',
                'value' => 'employee.EmpleadoNombreCompleto',
            ], 
            'DATE_CREATED',
            // 'MARITAL_STATUS',
            // 'AGE',
            // 'IESS_AFFILIATE',
            // 'ISSFA_AFFILIATE',
            // 'ISSPOL_AFFILIATE',
            // 'SOAT',
            // 'ADDRESS',
            // 'PHONE',
            // 'NOTIFIY_CONTACT',
            // 'RELATIONSHIP_CONTACT',
            // 'ADDRESS_CONTACT',
            // 'ESCORT',
            // 'ESCORT_ID',
            // 'ESCORT_ADDRESS',
            // 'ESCORT_PHONE',
            // 'TRANSPORTATION_METHOD',
            // 'TROUBLE',
            // 'TROUBLE_DATE',
            // 'TROUBLE_TIME',
            // 'TROUBLE_PLACE',
            // 'TROUBLE_ADDRESS',
            // 'EMERGENCY_TYPE',
            // 'VITAL_STATUS',
            // 'REFERENCE',
            // 'OBSERVATION_ADMISION',
            // 'SECONDS_ELAPSED_ATTENTION',
            // 'STATUS_ATTENTION',

            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view-er-log}{create-outflow-by-er-log}',
                    'buttons' => [
                        //view button
                        'view-er-log' => function ($url, $model) {//
                            return Html::a('<span  class="glyphicon glyphicon-eye-open">&nbsp</span>', $url, [ 'title' => Yii::t('app', 'Ver'), ]);
                        },
                        'create-outflow-by-er-log' => function ($url, $model) {//                       
                            return  Html::a('<span class="fa fa-save"></span>', $url, ['title' => Yii::t('yii', 'Cerrar 008'),]) ;                        
                        },
                    ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
     
    
    <hr style="height:1px;border:none;color:black;background-color:black;">
    <fieldset>     
        <legend>Cierres Realizados</legend>
    </fieldset>
    
    <?php Pjax::begin(['id'=>'closed008GridId']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'ID_OF',
            'ID_ERL',            
            [
              'attribute'  => 'ID_USR_ADMISION',
                'value' => 'employee.EmpleadoNombreCompleto',
            ], 
//            'CONTUTION',
//            'CAUSE_EFFECT',
            // 'DESTINATION',
            // 'TRANSFERED',
            // 'SPECIALITY',
             'DOCTOR_OUTFLOW',
             'DATE_OUTFLOW',
             'TIME_OUTFLOW',
            // 'OBSERVATION_ATTENTION',
            // 'SECONDS_ELAPSED_ATTENTION',
            // 'MODIFIED',
            // 'STATUS_ATTENTION',
              [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                    
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?php
$script = <<< JS

//PARA CARGAR DATOS SEGUN ID SI EXISTE
function refresh() {
     $.pjax.reload({container:'#pending008GridId', async:false});//TENGO Q USAR async:false para que valga la doble recarga de los dos 
     $.pjax.reload({container:'#closed008GridId', async:false});
        
     setTimeout(refresh, 40000); // refresh  the function every 40 seconds
 }
        
 refresh();  
           
JS;
$this->registerJs($script);
?>        