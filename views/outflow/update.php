<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Outflow */

$this->title = 'Actualizar Cierre 008: ' . ' ' . $model->ID_OF;
$this->params['breadcrumbs'][] = ['label' => 'Outflows', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_OF, 'url' => ['view', 'id' => $model->ID_OF]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="outflow-update">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
