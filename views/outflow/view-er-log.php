<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ErLog */

$this->title = $model->iDERP->Full_Name;
$this->params['breadcrumbs'][] = ['label' => 'Cierre 008', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => '008 Detalle'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="er-log-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Cerrar 008', ['update', 'id' => $model->ID_ERL], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Cancelar', ['/outflow/index'], ['class'=>'btn btn-warning']) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'ID_ERL',
            'CODE_008',
//            [                      // Para sobreescribir el label
//            'label' => 'Admisionista Responsable',
//            'value' => $model->employee->EmpleadoNombreCompleto,
//            ],   
              [                      // Para sobreescribir el label
            'label' => 'Admisionista Responsable',
            'value'=>($model->employee) ? $model->employee->EmpleadoNombreCompleto : " Administrador ",
              ],
            
            

            
            'iDERP.Full_Name',            
//            'ID_TRG',         
            'MARITAL_STATUS',
            'AGE',
            'IESS_AFFILIATE',
            'ISSFA_AFFILIATE',
            'ISSPOL_AFFILIATE',
            'SOAT',
            'ADDRESS',
            'PHONE',
            'NOTIFIY_CONTACT',
            'RELATIONSHIP_CONTACT',
            'ADDRESS_CONTACT',
            'ESCORT',
            'ESCORT_ID',
            'ESCORT_ADDRESS',
            'ESCORT_PHONE',
            'TRANSPORTATION_METHOD',
            'TROUBLE',
            'TROUBLE_DATE',
            'TROUBLE_TIME',
            'TROUBLE_PLACE',
            'TROUBLE_ADDRESS',
            'EMERGENCY_TYPE',
            'VITAL_STATUS',
            'REFERENCE',
            'OBSERVATION_ADMISION',
            'SECONDS_ELAPSED_ATTENTION',
            'STATUS_ATTENTION',
        ],
    ]) ?>

</div>
