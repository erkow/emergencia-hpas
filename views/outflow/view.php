<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Outflow */

$this->title = $erLog->iDERP->Full_Name;
$this->params['breadcrumbs'][] = ['label' => 'Cierre 008', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="outflow-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <!--<?= Html::a('Actualizar', ['update', 'id' => $model->ID_OF], ['class' => 'btn btn-primary']) ?>-->
        <?= Html::a('Cancelar', ['/outflow/index'], ['class'=>'btn btn-warning']) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'ID_OF',
            'ID_ERL',            
              [                      // Para sobreescribir el label
            'label' => 'Admisionista Responsable',
            'value'=>($model->employee) ? $model->employee->EmpleadoNombreCompleto : " Administrador ",
              ],
                          [                      // Para sobreescribir el label
            'label' => 'Diagnóstico de Egreso',
            'value'=>$model->iDCIE10->DESCRIPCION,
              ],            
            'CONTUTION',
            'CAUSE_EFFECT',
            'DESTINATION',
            'TRANSFERED',
            'SPECIALITY',
            'DOCTOR_OUTFLOW',
            'DATE_OUTFLOW',
            'TIME_OUTFLOW',
            'OBSERVATION_ATTENTION',
            'SECONDS_ELAPSED_ATTENTION',
            'MODIFIED',
//            'STATUS_ATTENTION',
        ],
    ]) ?>

</div>
