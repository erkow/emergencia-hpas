<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Pages;
use app\models\Profiles;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\PagesByProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-by-profile-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID_PG')->dropDownList(
            ArrayHelper::map(Pages::find()->all(), 'ID_PG', 'URL'), ['prompt' => '-Seleccione-']
    )
    ?>    
    
    

    <?= $form->field($model, 'ID_PRF')->dropDownList(
            ArrayHelper::map(Profiles::find()->all(),'ID_PRF','PROFILE'),['prompt'=>'-Seleccione-']
            ) ?>

    <?= $form->field($model, 'STATUS')->dropDownList([ 'ACTIVE' => 'Activo', 'INACTIVE' => 'Inactivo', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
