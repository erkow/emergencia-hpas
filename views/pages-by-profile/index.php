<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PagesByProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages By Profiles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-by-profile-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Pages By Profile', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'ID_PP',            
            [
                'attribute' => 'ID_PRF',
                'value' => 'iDPRF.PROFILE',
            ],
            [
                'attribute' => 'ID_PG',
                'value' => 'iDPG.URL',
            ],            
            'STATUS',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
