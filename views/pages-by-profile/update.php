<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PagesByProfile */

$this->title = 'Update Pages By Profile: ' . ' ' . $model->ID_PP;
$this->params['breadcrumbs'][] = ['label' => 'Pages By Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_PP, 'url' => ['view', 'id' => $model->ID_PP]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pages-by-profile-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
