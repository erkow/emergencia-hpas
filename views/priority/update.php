<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Priority */

$this->title = 'Update Priority: ' . ' ' . $model->ID_PRT;
$this->params['breadcrumbs'][] = ['label' => 'Priorities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_PRT, 'url' => ['view', 'id' => $model->ID_PRT]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="priority-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
