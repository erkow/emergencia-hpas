<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\CheckIn;
use app\models\Priority;
use yii\helpers\ArrayHelper;
use app\models\Cie10;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Triage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="triage-form">

    <?php $form = ActiveForm::begin(); ?>       
    
    <?= $form->field($erPatient, 'Full_Name')->textInput(['maxlength' => true,'disabled'=>true]) ?>
    
     <?= $form->field($checkIn, 'TROUBLE')->textInput(['maxlength' => true,'disabled'=>true]) ?>    
    <?php 
    if($model->isNewRecord){
           echo $form->field($model, 'ID_CI')->dropDownList(
            ArrayHelper::map(CheckIn::find()->all(), 'ID_CI', 'ID_CI'), ['prompt' => '-Seleccione-']
    );
    }
    ?>
    
    <?= $form->field($model, 'ID_PRT')->dropDownList(
            ArrayHelper::map(Priority::find()->all(), 'ID_PRT', 'PRIORITY'), ['prompt' => '-Seleccione-']
    )
    ?>    
    
    
    <?php echo $form->field($model, 'ID_CIE10')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Cie10::find()->all(), 'ID_CIE10', 'DESCRIPCION'),         
        'language' => 'es',              
        'options' => ['placeholder' => '-Seleccione-'],        
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>    
    
    <?= $form->field($model, 'OBSERVATION')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'btnUpdate']) ?>
        <?= Html::a('Cancelar', ['/triage/index'], ['class'=>'btn btn-warning']) ?>     
    </div>
 
 
    

    <?php ActiveForm::end(); ?>

</div>
