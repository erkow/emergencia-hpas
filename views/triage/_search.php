<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TriageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="triage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_TRG') ?>

    <?= $form->field($model, 'ID_CI') ?>

    <?= $form->field($model, 'ID_USR') ?>

    <?= $form->field($model, 'ID_PRT') ?>

    <?= $form->field($model, 'DATE_IN') ?>

    <?php // echo $form->field($model, 'TIME_IN') ?>

    <?php // echo $form->field($model, 'DATE_OUT') ?>

    <?php // echo $form->field($model, 'TIME_OUT') ?>

    <?php // echo $form->field($model, 'SECONDS_ELAPSED') ?>

    <?php // echo $form->field($model, 'OBSERVATION') ?>

    <?php // echo $form->field($model, 'STATUS') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Resetear', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
