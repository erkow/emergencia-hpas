<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TriageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Triages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="triage-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<!--
    <p>
        <?= Html::a('Crear Triage', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->

    <?php ///PARA MOSTRAR LA OPCION DE EXPORTAR
//      $gridColumns = [
//          //id
////          'iDCI.ESCORT',
//          'DATE_IN',
//          'TIME_IN',
//          'DATE_OUT'
//      ];
//    
//    //Renders an export dropdown menu
//    echo ExportMenu::widget([
//       'dataProvider' => $dataProvider,
//        'columns' => $gridColumns,       
//    ]);     
    ?>

<?php Pjax::begin(['id'=>'triageGridId']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //ESTO PERMITE DAR FORMATO A CIERTAS COLUMNAS EN ESPECIFICO, EN ESTE CASO PINTA ROJO LAS QUE TENGAN ESTADO INACTIVE
        'rowOptions' => function($model) {
            if ($model->STATUS == 'WAITING') {
                return ['class' => 'warning'];
            }
            if ($model->STATUS == 'ATTENDED') {
                return ['class' => 'success'];
            }
            if ($model->STATUS == 'ABSENT') {
                return ['class' => 'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],            

//            'ID_TRG',
//            'ID_CI',
//            'iDCI.TROUBLE',
            [//EN EL CASO DE TABLAS EN 3er RELACION PARA QUE SE PUEDA BUSCAR EL ATRIBUTE
            //DEBEMOS CREAR COMO VARIABLE EN SEARCHMODEL 
                'attribute'  => 'paciente',
                'value' => 'erPatient.Full_Name',
            ],
            [//PARA QUE SE PUEDA BUSCAR EL ATRIBUTE DEBE SER EL PARAMETRO QUE ESTA EN EL FORM, Q ESTE EN RULES
              'attribute'  => 'ID_CI',
                'value' => 'iDCI.TROUBLE',
            ],
            [
              'attribute'  => 'Edad',
                'value' => 'iDCI.AGE',
            ],     

//            'ID_USR',            
//            [
//              'attribute'  => 'ID_PRT',
//                'value' => 'iDPRT.PRIORITY',
//            ], 
            'DATE_IN',
             'TIME_IN',
            // 'DATE_OUT',
            // 'TIME_OUT',
//             'SECONDS_ELAPSED',
            // 'OBSERVATION',
//             'STATUS',
            [
              'attribute'=>'STATUS',
               'value'=>'statusLabel'
            ],

            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}{generate-pdf}{absent}',
                    'buttons' => [
                        //view button
                        'view' => function ($url, $model) {//SHOW EDIT ONLY WHEN ITS DOESNT HAVE PRIORITY
                            return Html::a('<span  class="glyphicon glyphicon-eye-open">&nbsp</span>', $url, [ 'title' => Yii::t('app', 'Ver'), ]);
                        },

                        //update button
                        'update' => function ($url, $model) {//SHOW EDIT ONLY WHEN ITS DOESNT HAVE PRIORITY
                            return $model->ID_PRT == '' ? Html::a('<span  class="glyphicon glyphicon-pencil">&nbsp</span>', $url, [ 'title' => Yii::t('app', 'Actualizar'), ]) : '';
                        },
                        'generate-pdf' => function ($url, $model) {//ONLY SHOW THIS BUTTON WITH PRIORITY AND ATTENDED                        
                            return  ($model->ID_PRT && $model->STATUS =='ATTENDED') ? Html::a('<span class="fa fa-print">&nbsp</span>', $url, ['title' => Yii::t('yii', 'Imprimir'),]) : '';
                        },
                        'absent' => function ($url, $model) {//ONLY SHOW THIS WITH STATUS WAITING                       
                            return  ($model->STATUS =='WAITING') ? Html::a('<span class="fa fa-user-times"></span>', $url, ['title' => Yii::t('yii', 'Ausente'),]) : '';                        
                        },
                    ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div>
<i><b>Nota 1:</b> Los pacientes "ATENDIDOS" se muestran por 2 horas.</i>
<br>
<i><b>Nota 2:</b> Los pacientes que "NO ACUDEN" se muestran por 4 horas.</i>


<?php
$script = <<< JS

//PARA CARGAR DATOS SEGUN ID SI EXISTE
function refresh() {
     $.pjax.reload({container:'#triageGridId', timeout: 90000});//ESTE VALOR DEBE SER MAYOR QUE EL DE ABJO PARA QUE FUNCIONE LO DEL SETTIMETOUT
        
     setTimeout(refresh, 40000); // refresh  the function every 40 seconds
 }
        
 refresh();  
           
JS;
$this->registerJs($script);
?>        