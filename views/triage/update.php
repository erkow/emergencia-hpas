<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Triage */

$this->title = 'Actualizar Triage: ' . ' ' . $erPatient->Full_Name;
$this->params['breadcrumbs'][] = ['label' => 'Triages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $erPatient->Full_Name, 'url' => ['view', 'id' => $model->ID_TRG]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="triage-update">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'checkIn'=> $checkIn,
        'erPatient'=> $erPatient,
    ]) ?>

</div>
