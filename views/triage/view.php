<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Triage */

$this->title = $erPatient->Full_Name;
$this->params['breadcrumbs'][] = ['label' => 'Triages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="triage-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?php 
            if (!$model->ID_PRT && $model->STATUS != 'ATTENDED') {
                echo Html::a('Actualizar', ['update', 'id' => $model->ID_TRG], ['class' => 'btn btn-primary']);
            }
            if ($model->ID_PRT && $model->STATUS == 'ATTENDED') {
                   echo     Html::a('<i class="fa glyphicon glyphicon-hand-up"></i>Generar PDF', ['generate-pdf', 'id' => $model->ID_TRG], 
                    ['class'=>'btn btn-danger', 'target'=> '_blank','data-toggle'=>'tooltip']);                            
            }
        ?>   
        <?= Html::a('Cancelar', ['/triage/index'], ['class'=>'btn btn-warning']) ?>
    

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'ID_TRG',
//            'ID_CI',
//            'ID_USR',
            'erPatient.Full_Name',
            'ID_PRT',
            'iDCIE10.DESCRIPCION',      
            'DATE_IN',
            'TIME_IN',
            'DATE_OUT',
            'TIME_OUT',
            'SECONDS_ELAPSED',
            'OBSERVATION',
            [ // Para sobreescribir el label
            'label' => 'Estado',
            'value' => $model->statusLabel,
            ],                        
        ],
    ]) ?>

</div>
