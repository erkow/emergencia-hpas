<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Profiles;

/* @var $this yii\web\View */
/* @var $model app\models\UserProfiles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-profiles-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <?= $form->field($model, 'ID_PRF')->dropDownList(
            ArrayHelper::map(Profiles::find()->all(),'ID_PRF','PROFILE'),
            ['prompt'=>'Seleccione Perfil']
            )?>

    <?= $form->field($model, 'ID_USR')->textInput() ?>

    <?= $form->field($model, 'STATUS')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
