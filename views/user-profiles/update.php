<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserProfiles */

$this->title = 'Update User Profiles: ' . ' ' . $model->ID_UP;
$this->params['breadcrumbs'][] = ['label' => 'User Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_UP, 'url' => ['view', 'id' => $model->ID_UP]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-profiles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
