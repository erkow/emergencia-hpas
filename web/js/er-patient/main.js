/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function () {
//get the click of the create Button of Check-in

//PARA CARGAR DATOS SEGUN ID SI EXISTE
$('#erpatient-id').change(function(){
   var idCode = $(this).val();
    
   $.get('../er-patient/get-er-patient-by-id',{id : idCode},function(data){
        if(data!='null'){//EXISTE REGISTRO
        var data = $.parseJSON(data);
        $('#auxId').attr('value',data.ID_ERP);
        $('#erpatient-first_name').attr('value',data.FIRST_NAME);
        $('#erpatient-last_name').attr('value',data.LAST_NAME);
        $("#erpatient-id_nt").val(data.ID_NT);
        $("#erpatient-gender").val(data.GENDER);
        $('#erpatient-birthday').attr('value',data.BIRTHDAY);
        desactivarCampos(true);
        }else{
        $('#auxId').attr('value','');
        $('#erpatient-first_name').attr('value','');
        $('#erpatient-last_name').attr('value','');
        $('#erpatient-birthday').attr('value','');
        $("#erpatient-id_nt").val('');
        $("#erpatient-gender").val('');
        desactivarCampos(false);
            }
    });
});
    
//FUNCION PARA SETEAR EN BLANCO DATOS PACIENTE (NO EVENTUALIDAD)
  
  
//FUNCION PARA DESHABILITAR/HABILITAR CAMPOS PACIENTE
function desactivarCampos(status){
  $('#erpatient-first_name').attr('disabled',status);
  $('#erpatient-last_name').attr('disabled',status);
  $("#erpatient-id_nt").attr('disabled',status);
  $("#erpatient-gender").attr('disabled',status);
  $('#erpatient-birthday').attr('disabled',status);
   
}  
  
//FUNCION PARA HABILITAR CAMPOS PACIENTE  
  
   
   
});

